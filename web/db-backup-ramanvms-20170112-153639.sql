CREATE DATABASE IF NOT EXISTS ramanvms;

USE ramanvms;

DROP TABLE IF EXISTS auth_assignment;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO auth_assignment VALUES("commit","4490239068","");
INSERT INTO auth_assignment VALUES("commit","4490271891","");
INSERT INTO auth_assignment VALUES("driver","4490239068","");
INSERT INTO auth_assignment VALUES("driver","4490271891","");
INSERT INTO auth_assignment VALUES("karpardaz","4490239068","");
INSERT INTO auth_assignment VALUES("karpardaz","4490271891","");
INSERT INTO auth_assignment VALUES("modirmain","4490271891","");
INSERT INTO auth_assignment VALUES("modirvahed","4490271891","");



DROP TABLE IF EXISTS auth_item;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO auth_item VALUES("Ajans","2","","","","","");
INSERT INTO auth_item VALUES("AllCar","2","","","","","");
INSERT INTO auth_item VALUES("Amanat","2","","","","","");
INSERT INTO auth_item VALUES("Attach","2","","","","","");
INSERT INTO auth_item VALUES("AttachType","2","","","","","");
INSERT INTO auth_item VALUES("Bazdid","2","","","","","");
INSERT INTO auth_item VALUES("BazdidType","2","","","","","");
INSERT INTO auth_item VALUES("Bazresi","2","","","","","");
INSERT INTO auth_item VALUES("Bimenameh","2","","","","","");
INSERT INTO auth_item VALUES("BimeUse","2","","","","","");
INSERT INTO auth_item VALUES("CarType","2","","","","","");
INSERT INTO auth_item VALUES("ChangeBimenameh","2","","","","","");
INSERT INTO auth_item VALUES("ChangeFani","2","","","","","");
INSERT INTO auth_item VALUES("ChangeInsuUse","2","","","","","");
INSERT INTO auth_item VALUES("ChangeJarime","2","","","","","");
INSERT INTO auth_item VALUES("ChangeMaliat","2","","","","","");
INSERT INTO auth_item VALUES("ChangePassword","2","","","","","");
INSERT INTO auth_item VALUES("ChangeSookht","2","","","","","");
INSERT INTO auth_item VALUES("ChangeTamirReq","2","","","","","");
INSERT INTO auth_item VALUES("ChangeTamirStatus","2","","","","","");
INSERT INTO auth_item VALUES("ChangeTrafic","2","","","","","");
INSERT INTO auth_item VALUES("City","2","","","","","");
INSERT INTO auth_item VALUES("Color","2","","","","","");
INSERT INTO auth_item VALUES("commit","1","","","","","");
INSERT INTO auth_item VALUES("Company","2","","","","","");
INSERT INTO auth_item VALUES("CreateCar","2","","","","","");
INSERT INTO auth_item VALUES("CreateReminders","2","","","","","");
INSERT INTO auth_item VALUES("CreateTamirReq","2","","","","","");
INSERT INTO auth_item VALUES("DeleteCar","2","","","","","");
INSERT INTO auth_item VALUES("DeleteTamirReq","2","","","","","");
INSERT INTO auth_item VALUES("Dept","2","","","","","");
INSERT INTO auth_item VALUES("driver","1","","","","","");
INSERT INTO auth_item VALUES("DriverCar","2","","","","","");
INSERT INTO auth_item VALUES("DriverPanel","2","","","","","");
INSERT INTO auth_item VALUES("EditCar","2","","","","","");
INSERT INTO auth_item VALUES("EditEmployee","2","","","","","");
INSERT INTO auth_item VALUES("EditNecessary","2","","","","","");
INSERT INTO auth_item VALUES("EditTamir","2","","","","","");
INSERT INTO auth_item VALUES("EhdaTo","2","","","","","");
INSERT INTO auth_item VALUES("EjareCar","2","","","","","");
INSERT INTO auth_item VALUES("Employee","2","","","","","");
INSERT INTO auth_item VALUES("Enteghal","2","","","","","");
INSERT INTO auth_item VALUES("Esghat","2","","","","","");
INSERT INTO auth_item VALUES("FactorAttach","2","","","","","");
INSERT INTO auth_item VALUES("Fani","2","","","","","");
INSERT INTO auth_item VALUES("Forosh","2","","","","","");
INSERT INTO auth_item VALUES("Foroshgah","2","","","","","");
INSERT INTO auth_item VALUES("HadeseType","2","","","","","");
INSERT INTO auth_item VALUES("Havades","2","","","","","");
INSERT INTO auth_item VALUES("Insu","2","","","","","");
INSERT INTO auth_item VALUES("Jarime","2","","","","","");
INSERT INTO auth_item VALUES("Karkerd","2","","","","","");
INSERT INTO auth_item VALUES("karpardaz","1","","","","","");
INSERT INTO auth_item VALUES("KarpardazSystem","2","","","","","");
INSERT INTO auth_item VALUES("Maliat","2","","","","","");
INSERT INTO auth_item VALUES("Mamoriat","2","","","","","");
INSERT INTO auth_item VALUES("modirmain","1","","","","","");
INSERT INTO auth_item VALUES("modirvahed","1","","","","","");
INSERT INTO auth_item VALUES("Necessary","2","","","","","");
INSERT INTO auth_item VALUES("NecessaryDriver","2","","","","","");
INSERT INTO auth_item VALUES("NecessaryType","2","","","","","");
INSERT INTO auth_item VALUES("RahbarSazman","2","","","","","");
INSERT INTO auth_item VALUES("RahbarSystem","2","","","","","");
INSERT INTO auth_item VALUES("Reminders","2","","","","","");
INSERT INTO auth_item VALUES("RemindersType","2","","","","","");
INSERT INTO auth_item VALUES("Report","2","","","","","");
INSERT INTO auth_item VALUES("Segment","2","","","","","");
INSERT INTO auth_item VALUES("Serghat","2","","","","","");
INSERT INTO auth_item VALUES("ServiceAjans","2","","","","","");
INSERT INTO auth_item VALUES("Setting","2","","","","","");
INSERT INTO auth_item VALUES("ShiftKari","2","","","","","");
INSERT INTO auth_item VALUES("Sookht","2","","","","","");
INSERT INTO auth_item VALUES("TahvilCar","2","","","","","");
INSERT INTO auth_item VALUES("TamdidCar","2","","","","","");
INSERT INTO auth_item VALUES("Tamir","2","","","","","");
INSERT INTO auth_item VALUES("Tamirgah","2","","","","","");
INSERT INTO auth_item VALUES("TamirReq","2","","","","","");
INSERT INTO auth_item VALUES("TamirSeg","2","","","","","");
INSERT INTO auth_item VALUES("TamirType","2","","","","","");
INSERT INTO auth_item VALUES("Trafic","2","","","","","");
INSERT INTO auth_item VALUES("Vahed","2","","","","","");
INSERT INTO auth_item VALUES("ViewCar","2","","","","","");
INSERT INTO auth_item VALUES("ViewEmployee","2","","","","","");
INSERT INTO auth_item VALUES("ViewNecessary","1","","","","","");
INSERT INTO auth_item VALUES("ViewTamir","2","","","","","");
INSERT INTO auth_item VALUES("ViewTamirReq","2","","","","","");



DROP TABLE IF EXISTS auth_item_child;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO auth_item_child VALUES("modirmain","Ajans");
INSERT INTO auth_item_child VALUES("modirvahed","AllCar");
INSERT INTO auth_item_child VALUES("modirvahed","Amanat");
INSERT INTO auth_item_child VALUES("karpardaz","Attach");
INSERT INTO auth_item_child VALUES("modirvahed","Attach");
INSERT INTO auth_item_child VALUES("modirmain","AttachType");
INSERT INTO auth_item_child VALUES("driver","Bazdid");
INSERT INTO auth_item_child VALUES("modirvahed","Bazdid");
INSERT INTO auth_item_child VALUES("modirmain","BazdidType");
INSERT INTO auth_item_child VALUES("modirvahed","Bazresi");
INSERT INTO auth_item_child VALUES("karpardaz","Bimenameh");
INSERT INTO auth_item_child VALUES("modirvahed","Bimenameh");
INSERT INTO auth_item_child VALUES("modirvahed","BimeUse");
INSERT INTO auth_item_child VALUES("modirmain","CarType");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeBimenameh");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeFani");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeInsuUse");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeJarime");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeMaliat");
INSERT INTO auth_item_child VALUES("driver","ChangePassword");
INSERT INTO auth_item_child VALUES("karpardaz","ChangePassword");
INSERT INTO auth_item_child VALUES("modirmain","ChangePassword");
INSERT INTO auth_item_child VALUES("modirvahed","ChangePassword");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeSookht");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeTamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeTamirStatus");
INSERT INTO auth_item_child VALUES("modirvahed","ChangeTrafic");
INSERT INTO auth_item_child VALUES("modirmain","City");
INSERT INTO auth_item_child VALUES("modirmain","Color");
INSERT INTO auth_item_child VALUES("modirmain","Company");
INSERT INTO auth_item_child VALUES("modirvahed","CreateCar");
INSERT INTO auth_item_child VALUES("modirvahed","CreateReminders");
INSERT INTO auth_item_child VALUES("driver","CreateTamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","CreateTamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","DeleteCar");
INSERT INTO auth_item_child VALUES("modirvahed","DeleteTamirReq");
INSERT INTO auth_item_child VALUES("modirmain","Dept");
INSERT INTO auth_item_child VALUES("driver","DriverCar");
INSERT INTO auth_item_child VALUES("driver","DriverPanel");
INSERT INTO auth_item_child VALUES("modirvahed","EditCar");
INSERT INTO auth_item_child VALUES("modirmain","EditEmployee");
INSERT INTO auth_item_child VALUES("modirvahed","EditEmployee");
INSERT INTO auth_item_child VALUES("modirvahed","EditNecessary");
INSERT INTO auth_item_child VALUES("karpardaz","EditTamir");
INSERT INTO auth_item_child VALUES("modirvahed","EditTamir");
INSERT INTO auth_item_child VALUES("modirvahed","EhdaTo");
INSERT INTO auth_item_child VALUES("modirvahed","EjareCar");
INSERT INTO auth_item_child VALUES("modirmain","Employee");
INSERT INTO auth_item_child VALUES("modirvahed","Employee");
INSERT INTO auth_item_child VALUES("modirvahed","Enteghal");
INSERT INTO auth_item_child VALUES("modirvahed","Esghat");
INSERT INTO auth_item_child VALUES("modirvahed","FactorAttach");
INSERT INTO auth_item_child VALUES("karpardaz","Fani");
INSERT INTO auth_item_child VALUES("modirvahed","Fani");
INSERT INTO auth_item_child VALUES("modirvahed","Forosh");
INSERT INTO auth_item_child VALUES("modirmain","Foroshgah");
INSERT INTO auth_item_child VALUES("modirmain","HadeseType");
INSERT INTO auth_item_child VALUES("modirvahed","Havades");
INSERT INTO auth_item_child VALUES("modirmain","Insu");
INSERT INTO auth_item_child VALUES("karpardaz","Jarime");
INSERT INTO auth_item_child VALUES("modirvahed","Jarime");
INSERT INTO auth_item_child VALUES("driver","Karkerd");
INSERT INTO auth_item_child VALUES("modirvahed","Karkerd");
INSERT INTO auth_item_child VALUES("karpardaz","KarpardazSystem");
INSERT INTO auth_item_child VALUES("karpardaz","Maliat");
INSERT INTO auth_item_child VALUES("modirvahed","Maliat");
INSERT INTO auth_item_child VALUES("modirvahed","Mamoriat");
INSERT INTO auth_item_child VALUES("modirvahed","Necessary");
INSERT INTO auth_item_child VALUES("driver","NecessaryDriver");
INSERT INTO auth_item_child VALUES("modirmain","NecessaryType");
INSERT INTO auth_item_child VALUES("modirmain","RahbarSazman");
INSERT INTO auth_item_child VALUES("modirvahed","RahbarSystem");
INSERT INTO auth_item_child VALUES("driver","Reminders");
INSERT INTO auth_item_child VALUES("modirvahed","Reminders");
INSERT INTO auth_item_child VALUES("modirmain","RemindersType");
INSERT INTO auth_item_child VALUES("karpardaz","Report");
INSERT INTO auth_item_child VALUES("modirmain","Report");
INSERT INTO auth_item_child VALUES("modirvahed","Report");
INSERT INTO auth_item_child VALUES("modirmain","Segment");
INSERT INTO auth_item_child VALUES("modirvahed","Serghat");
INSERT INTO auth_item_child VALUES("modirvahed","ServiceAjans");
INSERT INTO auth_item_child VALUES("modirvahed","Setting");
INSERT INTO auth_item_child VALUES("modirmain","ShiftKari");
INSERT INTO auth_item_child VALUES("driver","Sookht");
INSERT INTO auth_item_child VALUES("modirvahed","Sookht");
INSERT INTO auth_item_child VALUES("modirvahed","TahvilCar");
INSERT INTO auth_item_child VALUES("modirvahed","TamdidCar");
INSERT INTO auth_item_child VALUES("driver","Tamir");
INSERT INTO auth_item_child VALUES("karpardaz","Tamir");
INSERT INTO auth_item_child VALUES("modirvahed","Tamir");
INSERT INTO auth_item_child VALUES("modirmain","Tamirgah");
INSERT INTO auth_item_child VALUES("driver","TamirReq");
INSERT INTO auth_item_child VALUES("karpardaz","TamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","TamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","TamirSeg");
INSERT INTO auth_item_child VALUES("modirmain","TamirType");
INSERT INTO auth_item_child VALUES("modirvahed","Trafic");
INSERT INTO auth_item_child VALUES("modirmain","Vahed");
INSERT INTO auth_item_child VALUES("driver","ViewCar");
INSERT INTO auth_item_child VALUES("modirvahed","ViewCar");
INSERT INTO auth_item_child VALUES("modirmain","ViewEmployee");
INSERT INTO auth_item_child VALUES("modirvahed","ViewEmployee");
INSERT INTO auth_item_child VALUES("modirvahed","ViewNecessary");
INSERT INTO auth_item_child VALUES("driver","ViewTamir");
INSERT INTO auth_item_child VALUES("karpardaz","ViewTamir");
INSERT INTO auth_item_child VALUES("modirvahed","ViewTamir");
INSERT INTO auth_item_child VALUES("driver","ViewTamirReq");
INSERT INTO auth_item_child VALUES("karpardaz","ViewTamirReq");
INSERT INTO auth_item_child VALUES("modirvahed","ViewTamirReq");



DROP TABLE IF EXISTS auth_rule;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE IF EXISTS migration;

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_persian_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO migration VALUES("m000000_000000_base","1475250231");
INSERT INTO migration VALUES("m140506_102106_rbac_init","1475250710");



DROP TABLE IF EXISTS tahadis;

CREATE TABLE `tahadis` (
  `TahadisID` int(11) NOT NULL AUTO_INCREMENT,
  `Body` longtext COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`TahadisID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tajans;

CREATE TABLE `tajans` (
  `ACode` int(11) NOT NULL AUTO_INCREMENT,
  `AName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `Tell` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Address` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  PRIMARY KEY (`ACode`),
  KEY `FK_ajans_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_ajans_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tajans VALUES("1","آژانس بلوار","08433333333","ایلام","1");
INSERT INTO tajans VALUES("2","آژانس دانشگاه","08433333333","ایلام","1");
INSERT INTO tajans VALUES("3","آژانس سپهر","08433333333","ایلام","1");



DROP TABLE IF EXISTS tamanatfrom;

CREATE TABLE `tamanatfrom` (
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VcodeAmanat` int(11) NOT NULL,
  `Sh_mojavez` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Date_mojavez` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vcode` int(11) NOT NULL,
  `AmanatDate_s` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `AmanatDate_f` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `Tozihat` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`AmanatDate_s`),
  KEY `FK_amanat_vahedAmanat_idx` (`FK_VcodeAmanat`),
  KEY `FK_amanat_vahed_idx` (`FK_Vcode`),
  CONSTRAINT `FK_amanat_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_amanat_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_amanat_vahedAmanat` FOREIGN KEY (`FK_VcodeAmanat`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tamanatfrom VALUES("9812ج637","2","12354","1471853410","1","1472631010","1504253410","12000","ندارد","4490271891","1482069224");



DROP TABLE IF EXISTS tamanatto;

CREATE TABLE `tamanatto` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode_S` int(11) NOT NULL,
  `FK_VCode_D` int(11) NOT NULL,
  `AmanatDate_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `AmanatDate_F` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mojavez_sh` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `MojavezDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`AmanatDate_S`),
  KEY `FK_enteghal_vahedS_idx` (`FK_VCode_S`),
  KEY `FK_enteghal_vahedD_idx` (`FK_VCode_D`),
  CONSTRAINT `FK_amanatto_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_amanatto_vahedD` FOREIGN KEY (`FK_VCode_D`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_amanatto_vahedS` FOREIGN KEY (`FK_VCode_S`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tatttype;

CREATE TABLE `tatttype` (
  `AttTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `AttName` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`AttTypeID`),
  KEY `FK_atttype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_atttype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tatttype VALUES("1","سند خودرو","");
INSERT INTO tatttype VALUES("2","کارت ماشین","");
INSERT INTO tatttype VALUES("3","کارت سوخت","");
INSERT INTO tatttype VALUES("4","بیمه نامه شخص ثالث","");
INSERT INTO tatttype VALUES("5","بیمه نامه بدنه","");
INSERT INTO tatttype VALUES("6","فاکتور خرید","1");
INSERT INTO tatttype VALUES("7","قرارداد خرید","1");
INSERT INTO tatttype VALUES("8","استعلام خلافی","1");
INSERT INTO tatttype VALUES("9","صورتجلسه تحویل","1");
INSERT INTO tatttype VALUES("10","صورتجلسه انتقال","1");
INSERT INTO tatttype VALUES("11","صورتجلسه اهدا","1");
INSERT INTO tatttype VALUES("12","صورتجلسه امانت","1");
INSERT INTO tatttype VALUES("13","کروکی تصادف","1");
INSERT INTO tatttype VALUES("14","صورتجلسه حادثه","1");
INSERT INTO tatttype VALUES("15","صورتجلسه اسقاط","1");
INSERT INTO tatttype VALUES("16","صورتجلسه فروش","1");
INSERT INTO tatttype VALUES("17","فرم پرداخت عوارض شهرداری","1");
INSERT INTO tatttype VALUES("18","فرم پرداخت مالیات","1");
INSERT INTO tatttype VALUES("19","فرم معاینه فنی","1");
INSERT INTO tatttype VALUES("20","فرم طرح ترافیک","1");
INSERT INTO tatttype VALUES("21","صورتجلسه بازدید ","1");



DROP TABLE IF EXISTS tbazdid;

CREATE TABLE `tbazdid` (
  `BazID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `DateS` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_BazTypeId` int(11) NOT NULL,
  `FK_CodeT` int(11) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `ReqRem` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vahed` int(11) NOT NULL,
  PRIMARY KEY (`BazID`),
  KEY `FK_bazdid_car_idx` (`FK_Pelak`),
  KEY `FK_bazdid_bazdidtype_idx` (`FK_BazTypeId`),
  KEY `FK_bazdid_tamirgah_idx` (`FK_CodeT`),
  KEY `FK_bazdid_vahed_idx` (`FK_Vahed`),
  CONSTRAINT `FK_bazdid_bazdidtype` FOREIGN KEY (`FK_BazTypeId`) REFERENCES `tbazdidtype` (`BazTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazdid_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazdid_tamirgah` FOREIGN KEY (`FK_CodeT`) REFERENCES `ttamirgah` (`CodeT`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazdid_vahed` FOREIGN KEY (`FK_Vahed`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tbazdidtype;

CREATE TABLE `tbazdidtype` (
  `BazTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BazTypeName` varchar(150) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`BazTypeID`),
  KEY `FK_bazdidtype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_bazdidtype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbazdidtype VALUES("8","بازدید اتصالات، لوله ها و شیلنگهای ترمز","");
INSERT INTO tbazdidtype VALUES("9","بازدید پدال ترمز","");
INSERT INTO tbazdidtype VALUES("10","بازدید پدال و کلاچ و رگلاژ سیم کلاچ","");
INSERT INTO tbazdidtype VALUES("11","بازدید دیسک ترمز و کاسه چرخ و لنت های جلو و کفشکهای عقب","");
INSERT INTO tbazdidtype VALUES("12","بازدید روغن ترمز","");
INSERT INTO tbazdidtype VALUES("13","بازدید روغن گیربکس اتوماتیک و در صورت نیاز تنظیم مقدار و یا تعویض آن","");
INSERT INTO tbazdidtype VALUES("14","بازدید زیر اتومبیل","");
INSERT INTO tbazdidtype VALUES("15","بازدید سفتی مهره های چرخ","");
INSERT INTO tbazdidtype VALUES("16","بازدید سیستم خنک کننده شامل تنظیم مقدار مایع خنک کننده","");
INSERT INTO tbazdidtype VALUES("17","بازدید سیستم فرمان و تعلیق جلو","");
INSERT INTO tbazdidtype VALUES("18","بازدید عملکرد کمربندهای ایمنی","");
INSERT INTO tbazdidtype VALUES("19","بازدید فشار اویل پمپ","");
INSERT INTO tbazdidtype VALUES("20","بازدید کلیه گردگیر پلوسها","");
INSERT INTO tbazdidtype VALUES("21","بازدید لاستیکها شامل زاپاس و تنظیم میزان فشار باد","");
INSERT INTO tbazdidtype VALUES("22","بازدید لوله ها و شیلنگهای سوخت","");
INSERT INTO tbazdidtype VALUES("23","بازدید مدارات الکتریکی شامل چراغها، برف پاک کن ها، بوق و ...","");
INSERT INTO tbazdidtype VALUES("24","بازدید و اسکازین گیربکس و در صورت نیاز تنظیم مقدار آن","");
INSERT INTO tbazdidtype VALUES("25","بازدید و تنظیم شمع ها","");
INSERT INTO tbazdidtype VALUES("26","بازدید وضعیت بوستر ترمز و شیلنگ ها","");
INSERT INTO tbazdidtype VALUES("27","تست جاده","");
INSERT INTO tbazdidtype VALUES("28","تعویض روغن ترمز","");
INSERT INTO tbazdidtype VALUES("29","تعویض روغن موتور","");
INSERT INTO tbazdidtype VALUES("30","تعویض فیلتر روغن","");
INSERT INTO tbazdidtype VALUES("31","تعویض فیلتر سوخت","");
INSERT INTO tbazdidtype VALUES("32","تعویض و اسکازین گیربکس","");
INSERT INTO tbazdidtype VALUES("33","تنظمی میزان آب باطری و غلظت آن","");
INSERT INTO tbazdidtype VALUES("34","تنظيم موتور مناسب با شرایط محیطی","");
INSERT INTO tbazdidtype VALUES("35","تنظیم تایمینگ دلکو","");
INSERT INTO tbazdidtype VALUES("36","تنظیم ترمز دستی","");
INSERT INTO tbazdidtype VALUES("37","تنظیم تسمه ها","");
INSERT INTO tbazdidtype VALUES("38","تنظیم سوپاپها (فیلر گیری)","");
INSERT INTO tbazdidtype VALUES("39","تنظیم نور و چراغها جلو","");
INSERT INTO tbazdidtype VALUES("40","رگلاژ درها","");
INSERT INTO tbazdidtype VALUES("41","سفت کردن پیچ و مهره های منیفولد","");
INSERT INTO tbazdidtype VALUES("42","شستشوی خودرو","");
INSERT INTO tbazdidtype VALUES("43","سرویس اولیه","");
INSERT INTO tbazdidtype VALUES("44","سرویس 3000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("45","سرویس 5000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("46","سرویس 10000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("47","سرویس 15000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("48","سرویس 20000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("49","سرویس 30000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("50","سرویس 40000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("51","سرویس 50000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("52","سرویس 60000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("53","سرویس 70000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("54","سرویس 80000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("55","سرویس 90000 کیلومتر","");
INSERT INTO tbazdidtype VALUES("56","سرویس 100000کیلومتر","");
INSERT INTO tbazdidtype VALUES("57","بازدید باد چرخها","");
INSERT INTO tbazdidtype VALUES("58","آچارکشی پیچ و و مهره های بدنه و شاسی اتومبیل","1");



DROP TABLE IF EXISTS tbazresi;

CREATE TABLE `tbazresi` (
  `BazresiID` int(11) NOT NULL AUTO_INCREMENT,
  `BazresiDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_Codemeli` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Comment` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_VCode` int(11) NOT NULL,
  `FK_Bazdidkonande` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`BazresiID`),
  KEY `FK_bazresi_pelak_idx` (`FK_Pelak`),
  KEY `FK_bazresi_employee_idx` (`FK_Codemeli`),
  KEY `FK_bazresi_vahed_idx` (`FK_VCode`),
  KEY `FK_bazresikonande_employee_idx` (`FK_Bazdidkonande`),
  CONSTRAINT `FK_bazresikonande_employee` FOREIGN KEY (`FK_Bazdidkonande`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazresi_employee` FOREIGN KEY (`FK_Codemeli`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazresi_pelak` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bazresi_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tbimenameh;

CREATE TABLE `tbimenameh` (
  `ShomareBime` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `CodeInsu` int(11) NOT NULL,
  `BimenamehDateS` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `BimenamehDateF` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `InsuType` tinyint(4) NOT NULL,
  `MizanTahod` varchar(10) COLLATE utf8_persian_ci NOT NULL,
  `Hazine` varchar(10) COLLATE utf8_persian_ci NOT NULL,
  `TakhYear` int(11) NOT NULL,
  `TakhMablagh` varchar(10) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Tozihat` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`ShomareBime`),
  KEY `FK_bimenameh_car_idx` (`FK_Pelak`),
  KEY `FK_bimenameh_insu_idx` (`CodeInsu`),
  KEY `FK_bimenameh_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_bimenameh_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bimenameh_insu` FOREIGN KEY (`CodeInsu`) REFERENCES `tinsu` (`CodeI`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bimenameh_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tcar;

CREATE TABLE `tcar` (
  `Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `CarType` int(11) NOT NULL,
  `ShMotor` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `ShShasi` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `VIN` bigint(20) NOT NULL,
  `Sakht_t` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Kharid_t` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Color` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_MCode` int(11) NOT NULL,
  `Sookht_type` tinyint(4) NOT NULL,
  `Mosafer_cap` tinyint(4) NOT NULL,
  `Bar_cap` float NOT NULL,
  `Use_type` tinyint(4) NOT NULL,
  `TamalokStatus` int(11) NOT NULL,
  `First_cost` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `Date_tahvil` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_Vcode` int(11) DEFAULT NULL,
  `FK_codemeli` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_Dcode` int(11) DEFAULT NULL,
  `Desp_darsad` tinyint(4) NOT NULL,
  `Garanty_t_f` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Garanty_kilo` int(11) NOT NULL,
  `EnterType` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `CountRing` int(11) DEFAULT NULL,
  `CountSilandr` int(11) DEFAULT NULL,
  `AverageInCity` float DEFAULT NULL,
  `AverageOutCity` float DEFAULT NULL,
  `ForoshandeName` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  `ForoshandePhone` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `ForoshandeAddress` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`Pelak`),
  KEY `FK_car_vahed_idx` (`FK_Vcode`),
  KEY `FK_car_dept_idx` (`FK_Dcode`),
  KEY `FK_car_CarModel_idx` (`FK_MCode`),
  KEY `FK_car_cartype_idx` (`CarType`),
  CONSTRAINT `FK_car_CarModel` FOREIGN KEY (`FK_MCode`) REFERENCES `tcarmodel` (`Mcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_car_cartype` FOREIGN KEY (`CarType`) REFERENCES `tcartype` (`CarTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_car_dept` FOREIGN KEY (`FK_Dcode`) REFERENCES `tdept` (`Dcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_car_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcar VALUES("9812ج637","5","6546848","468489","8746565465","1346573410","1347437410","1","2","1","4","1","1","2","100000000","2","1482052210","1","4490239068","2","10","1409731810","10200","2","4490271891","1483600499","4","4","10","8","","","");
INSERT INTO tcar VALUES("9822ب111","6","1","1","1","1474531810","1479287410","2","1","1","1","1","1","1","12","1","1479287410","1","4490271891","1","10","1479633010","1","1","4490271891","1484061889","4","4","1","1","","","");



DROP TABLE IF EXISTS tcaratt;

CREATE TABLE `tcaratt` (
  `AttID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `DateS` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_AttType` int(11) NOT NULL,
  `FilePath` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`AttID`),
  KEY `FK_carAtt_car_idx` (`FK_Pelak`),
  KEY `FK_carAtt_attType_idx` (`FK_AttType`),
  CONSTRAINT `FK_carAtt_attType` FOREIGN KEY (`FK_AttType`) REFERENCES `tatttype` (`AttTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_carAtt_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcaratt VALUES("1","9822ب111","پیوست 1","1484125810","2","CarAtt1.jpg","4490271891","1484112681");



DROP TABLE IF EXISTS tcarmodel;

CREATE TABLE `tcarmodel` (
  `Mcode` int(11) NOT NULL AUTO_INCREMENT,
  `Modelname` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_Comcode` int(11) NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Mcode`),
  KEY `FK_carmodel_company_idx` (`FK_Comcode`),
  KEY `FK_varmodel_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_carmodel_company` FOREIGN KEY (`FK_Comcode`) REFERENCES `tcompany` (`Comcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_varmodel_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcarmodel VALUES("1","پژو405","1","1");
INSERT INTO tcarmodel VALUES("2","پراید 131","2","1");
INSERT INTO tcarmodel VALUES("3","پژو پارس","1","1");
INSERT INTO tcarmodel VALUES("4","پژو 206","1","1");
INSERT INTO tcarmodel VALUES("5","پژو 207","1","1");
INSERT INTO tcarmodel VALUES("6","پراید 132","2","1");
INSERT INTO tcarmodel VALUES("7","تندر 90","1","1");
INSERT INTO tcarmodel VALUES("8","سمند","1","1");
INSERT INTO tcarmodel VALUES("9","تیبا","2","1");
INSERT INTO tcarmodel VALUES("10","وانت نیسان","2","1");
INSERT INTO tcarmodel VALUES("11","سمند ال ایکس","1","1");
INSERT INTO tcarmodel VALUES("12","سمند سورن","1","1");
INSERT INTO tcarmodel VALUES("13","رانا","1","1");
INSERT INTO tcarmodel VALUES("14","پراید صبا","2","1");
INSERT INTO tcarmodel VALUES("15","وانت مزدا تک کابین","4","1");
INSERT INTO tcarmodel VALUES("16","وانت مزدا دو کابین","4","1");
INSERT INTO tcarmodel VALUES("17","پژو 405","21","1");
INSERT INTO tcarmodel VALUES("18","پژو  206","21","1");
INSERT INTO tcarmodel VALUES("19","لیفان X50","22","1");
INSERT INTO tcarmodel VALUES("20","لیفان X60","22","1");
INSERT INTO tcarmodel VALUES("21","لیفان 520","22","1");
INSERT INTO tcarmodel VALUES("22","لیفان 620","22","1");
INSERT INTO tcarmodel VALUES("23","رنو L90","20","1");
INSERT INTO tcarmodel VALUES("24","رنو ساندرو","20","1");
INSERT INTO tcarmodel VALUES("25","رنو داستر","20","1");
INSERT INTO tcarmodel VALUES("26","سانتافه","23","1");
INSERT INTO tcarmodel VALUES("27","آمبولانس اسپرینتر","5","1");
INSERT INTO tcarmodel VALUES("28","سوزوکی گرندویتارا","42","1");
INSERT INTO tcarmodel VALUES("29","پیکان","1","1");



DROP TABLE IF EXISTS tcartype;

CREATE TABLE `tcartype` (
  `CarTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  `CarGroup` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `KarkerdType` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`CarTypeID`),
  KEY `FK_cartype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_cartype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcartype VALUES("5","سواری","1","1","1");
INSERT INTO tcartype VALUES("6","وانت دو کابین","1","1","1");
INSERT INTO tcartype VALUES("7","وانت تک کابین","1","1","1");
INSERT INTO tcartype VALUES("8","مینی بوس","1","3","1");
INSERT INTO tcartype VALUES("9","اتوبوس","1","3","1");
INSERT INTO tcartype VALUES("10","آمبولانس","1","1","1");
INSERT INTO tcartype VALUES("11","کامیونت","1","3","1");
INSERT INTO tcartype VALUES("12","کامیون کشنده","1","2","1");
INSERT INTO tcartype VALUES("14","کامیون باری","1","2","1");
INSERT INTO tcartype VALUES("15","تریلر","1","2","1");
INSERT INTO tcartype VALUES("16","بولدزر","1","2","2");
INSERT INTO tcartype VALUES("17","لیفتراک","1","2","2");
INSERT INTO tcartype VALUES("18","تراکتور","1","2","2");
INSERT INTO tcartype VALUES("19","کامیون تانکر","1","2","1");
INSERT INTO tcartype VALUES("20","لودر","1","2","2");
INSERT INTO tcartype VALUES("21","گریدر","1","2","2");
INSERT INTO tcartype VALUES("22","کامیون حمل زباله","1","3","1");
INSERT INTO tcartype VALUES("23","بیل مکانیکی","1","3","2");
INSERT INTO tcartype VALUES("24","کمپرسی 10 تن","1","2","1");
INSERT INTO tcartype VALUES("25","کمپرسی 5 تن","1","3","1");
INSERT INTO tcartype VALUES("26","استیشن","1","1","1");
INSERT INTO tcartype VALUES("27","ون","1","1","1");
INSERT INTO tcartype VALUES("28","آتشنشانی معمولی","1","3","1");
INSERT INTO tcartype VALUES("29","خودروی حامل نردبان","1","3","1");
INSERT INTO tcartype VALUES("30","آتشنشانی فرودگاهی","1","3","1");
INSERT INTO tcartype VALUES("31","آتشنشانی صنعتی","1","3","1");
INSERT INTO tcartype VALUES("32","آتشنشانی جنگل","1","3","1");
INSERT INTO tcartype VALUES("33","نجات و حوادث ویژه","1","3","1");
INSERT INTO tcartype VALUES("34","جرثقیل","1","2","2");
INSERT INTO tcartype VALUES("35","مینی لودر","1","2","2");
INSERT INTO tcartype VALUES("36","مینی غلتک","1","2","2");



DROP TABLE IF EXISTS tcity;

CREATE TABLE `tcity` (
  `Ccode` int(11) NOT NULL AUTO_INCREMENT,
  `Cname` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_Ocode` int(11) NOT NULL,
  PRIMARY KEY (`Ccode`),
  KEY `FK_city_ostan_idx` (`FK_Ocode`),
  CONSTRAINT `FK_city_ostan` FOREIGN KEY (`FK_Ocode`) REFERENCES `tostan` (`Ocode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcity VALUES("1","تبریز","1");
INSERT INTO tcity VALUES("2","مراغه","1");
INSERT INTO tcity VALUES("3","اهر","1");
INSERT INTO tcity VALUES("4","سراب","1");
INSERT INTO tcity VALUES("5","مرند","1");
INSERT INTO tcity VALUES("6","میانه","1");
INSERT INTO tcity VALUES("7","هشترود","1");
INSERT INTO tcity VALUES("8","بناب","1");
INSERT INTO tcity VALUES("9","هریس","1");
INSERT INTO tcity VALUES("10","کلیبر","1");
INSERT INTO tcity VALUES("11","بستان‌آباد","1");
INSERT INTO tcity VALUES("12","شبستر","1");
INSERT INTO tcity VALUES("13","جلفا","1");
INSERT INTO tcity VALUES("14","ملکان","1");
INSERT INTO tcity VALUES("15","آذرشهر","1");
INSERT INTO tcity VALUES("16","اسکو","1");
INSERT INTO tcity VALUES("17","چاراویماق","1");
INSERT INTO tcity VALUES("18","خداآفرین","1");
INSERT INTO tcity VALUES("19","عجب شیر","1");
INSERT INTO tcity VALUES("20","ورزقان","1");
INSERT INTO tcity VALUES("21","ارومیه","2");
INSERT INTO tcity VALUES("22","خوی","2");
INSERT INTO tcity VALUES("23","مهاباد","2");
INSERT INTO tcity VALUES("24","ماکو","2");
INSERT INTO tcity VALUES("25","سلماس","2");
INSERT INTO tcity VALUES("26","میاندوآب","2");
INSERT INTO tcity VALUES("27","نقده","2");
INSERT INTO tcity VALUES("28","پیرانشهر","2");
INSERT INTO tcity VALUES("29","سردشت","2");
INSERT INTO tcity VALUES("30","بوکان","2");
INSERT INTO tcity VALUES("31","تکاب","2");
INSERT INTO tcity VALUES("32","شاهین دژ","2");
INSERT INTO tcity VALUES("33","اشنویه","2");
INSERT INTO tcity VALUES("34","پلدشت","2");
INSERT INTO tcity VALUES("35","چالدران","2");
INSERT INTO tcity VALUES("36","چایپاره","2");
INSERT INTO tcity VALUES("37","شوط","2");
INSERT INTO tcity VALUES("38","اردبیل","3");
INSERT INTO tcity VALUES("39","خلخال","3");
INSERT INTO tcity VALUES("40","مشگین شهر","3");
INSERT INTO tcity VALUES("41","گرمی","3");
INSERT INTO tcity VALUES("42","بیله سوار","3");
INSERT INTO tcity VALUES("43","پارس‌آباد","3");
INSERT INTO tcity VALUES("44","نمین","3");
INSERT INTO tcity VALUES("45","کوثر","3");
INSERT INTO tcity VALUES("46","نیر","3");
INSERT INTO tcity VALUES("47","سرعین","3");
INSERT INTO tcity VALUES("48","اصفهان","4");
INSERT INTO tcity VALUES("49","کاشان","4");
INSERT INTO tcity VALUES("50","گلپایگان","4");
INSERT INTO tcity VALUES("51","شهرضا","4");
INSERT INTO tcity VALUES("52","فریدن","4");
INSERT INTO tcity VALUES("53","اردستان","4");
INSERT INTO tcity VALUES("54","نایین","4");
INSERT INTO tcity VALUES("55","نجف‌آباد","4");
INSERT INTO tcity VALUES("56","نطنز","4");
INSERT INTO tcity VALUES("57","سمیرم","4");
INSERT INTO tcity VALUES("58","لنجان","4");
INSERT INTO tcity VALUES("59","خوانسار","4");
INSERT INTO tcity VALUES("60","فریدون‌شهر","4");
INSERT INTO tcity VALUES("61","خمینی شهر","4");
INSERT INTO tcity VALUES("62","فلاورجان","4");
INSERT INTO tcity VALUES("63","شاهین شهر و میمه","4");
INSERT INTO tcity VALUES("64","مبارکه","4");
INSERT INTO tcity VALUES("65","آران و بیدگل","4");
INSERT INTO tcity VALUES("66","تیران و کرون","4");
INSERT INTO tcity VALUES("67","چادگان","4");
INSERT INTO tcity VALUES("68","دهاقان","4");
INSERT INTO tcity VALUES("69","برخوار","4");
INSERT INTO tcity VALUES("70","خور و بیابانک","4");
INSERT INTO tcity VALUES("71","بویین و میاندشت","4");
INSERT INTO tcity VALUES("72","کرج","5");
INSERT INTO tcity VALUES("73","ساوجبلاغ","5");
INSERT INTO tcity VALUES("74","نظرآباد","5");
INSERT INTO tcity VALUES("75","طالقان","5");
INSERT INTO tcity VALUES("76","اشتهارد","5");
INSERT INTO tcity VALUES("77","فردیس","5");
INSERT INTO tcity VALUES("78","ایلام","6");
INSERT INTO tcity VALUES("79","دره شهر","6");
INSERT INTO tcity VALUES("80","دهلران","6");
INSERT INTO tcity VALUES("81","مهران","6");
INSERT INTO tcity VALUES("82","شیروان و چرداول","6");
INSERT INTO tcity VALUES("83","آبدانان","6");
INSERT INTO tcity VALUES("84","ایوان","6");
INSERT INTO tcity VALUES("86","سیروان","6");
INSERT INTO tcity VALUES("87","بوشهر","7");
INSERT INTO tcity VALUES("88","دشتستان","7");
INSERT INTO tcity VALUES("89","تنگستان","7");
INSERT INTO tcity VALUES("90","دشتی","7");
INSERT INTO tcity VALUES("91","کنگان","7");
INSERT INTO tcity VALUES("92","گناوه","7");
INSERT INTO tcity VALUES("93","دیر","7");
INSERT INTO tcity VALUES("94","دیلم","7");
INSERT INTO tcity VALUES("95","جم","7");
INSERT INTO tcity VALUES("96","عسلویه","7");
INSERT INTO tcity VALUES("97","تهران","8");
INSERT INTO tcity VALUES("98","دماوند","8");
INSERT INTO tcity VALUES("99","ورامین","8");
INSERT INTO tcity VALUES("100","ری","8");
INSERT INTO tcity VALUES("101","شمیرانات","8");
INSERT INTO tcity VALUES("102","شهریار","8");
INSERT INTO tcity VALUES("103","اسلامشهر","8");
INSERT INTO tcity VALUES("104","رباط کریم","8");
INSERT INTO tcity VALUES("105","فیروزکوه","8");
INSERT INTO tcity VALUES("106","پاکدشت","8");
INSERT INTO tcity VALUES("107","قدس","8");
INSERT INTO tcity VALUES("108","ملارد","8");
INSERT INTO tcity VALUES("109","بهارستان","8");
INSERT INTO tcity VALUES("110","پیشوا","8");
INSERT INTO tcity VALUES("111","پردیس","8");
INSERT INTO tcity VALUES("112","قرچک","8");
INSERT INTO tcity VALUES("113","بیرجند","9");
INSERT INTO tcity VALUES("114","فردوس","9");
INSERT INTO tcity VALUES("115","طبس","9");
INSERT INTO tcity VALUES("116","قاینات","9");
INSERT INTO tcity VALUES("117","نهبندان","9");
INSERT INTO tcity VALUES("118","سربیشه","9");
INSERT INTO tcity VALUES("119","درمیان","9");
INSERT INTO tcity VALUES("120","سرایان","9");
INSERT INTO tcity VALUES("121","بشرویه","9");
INSERT INTO tcity VALUES("122","خوسف","9");
INSERT INTO tcity VALUES("123","زیرکوه","9");
INSERT INTO tcity VALUES("124","تربت حیدریه","10");
INSERT INTO tcity VALUES("125","سبزوار","10");
INSERT INTO tcity VALUES("126","قوچان","10");
INSERT INTO tcity VALUES("127","مشهد","10");
INSERT INTO tcity VALUES("128","گناباد","10");
INSERT INTO tcity VALUES("129","نیشابور","10");
INSERT INTO tcity VALUES("130","کاشمر","10");
INSERT INTO tcity VALUES("131","درگز","10");
INSERT INTO tcity VALUES("132","تربت جام","10");
INSERT INTO tcity VALUES("133","تایباد","10");
INSERT INTO tcity VALUES("134","خواف","10");
INSERT INTO tcity VALUES("135","سرخس","10");
INSERT INTO tcity VALUES("136","چناران","10");
INSERT INTO tcity VALUES("137","فریمان","10");
INSERT INTO tcity VALUES("138","بردسکن","10");
INSERT INTO tcity VALUES("139","رشتخوار","10");
INSERT INTO tcity VALUES("140","خلیل‌آباد","10");
INSERT INTO tcity VALUES("141","خوشاب","10");
INSERT INTO tcity VALUES("142","کلات","10");
INSERT INTO tcity VALUES("143","مه‌ولات","10");
INSERT INTO tcity VALUES("144","بجستان","10");
INSERT INTO tcity VALUES("145","بینالود","10");
INSERT INTO tcity VALUES("146","تخت جلگه","10");
INSERT INTO tcity VALUES("147","جغتای","10");
INSERT INTO tcity VALUES("148","جوین","10");
INSERT INTO tcity VALUES("149","زاوه","10");
INSERT INTO tcity VALUES("150","باخرز","10");
INSERT INTO tcity VALUES("151","داورزن","10");
INSERT INTO tcity VALUES("152","بجنورد","11");
INSERT INTO tcity VALUES("153","اسفراین","11");
INSERT INTO tcity VALUES("154","شیروان","11");
INSERT INTO tcity VALUES("155","جاجرم","11");
INSERT INTO tcity VALUES("156","مانه و سملقان","11");
INSERT INTO tcity VALUES("157","فاروج","11");
INSERT INTO tcity VALUES("158","گرمه","11");
INSERT INTO tcity VALUES("159","راز و جرگلان","11");
INSERT INTO tcity VALUES("160","اهواز","12");
INSERT INTO tcity VALUES("161","بهبهان","12");
INSERT INTO tcity VALUES("162","خرمشهر","12");
INSERT INTO tcity VALUES("163","آبادان","12");
INSERT INTO tcity VALUES("164","دشت آزادگان","12");
INSERT INTO tcity VALUES("165","دزفول","12");
INSERT INTO tcity VALUES("166","شوشتر","12");
INSERT INTO tcity VALUES("167","مسجدسلیمان","12");
INSERT INTO tcity VALUES("168","ایذه","12");
INSERT INTO tcity VALUES("169","رامهرمز","12");
INSERT INTO tcity VALUES("170","بندر ماهشهر","12");
INSERT INTO tcity VALUES("171","شادگان","12");
INSERT INTO tcity VALUES("172","اندیمشک","12");
INSERT INTO tcity VALUES("173","شوش","12");
INSERT INTO tcity VALUES("174","باغ ملک","12");
INSERT INTO tcity VALUES("175","امیدیه","12");
INSERT INTO tcity VALUES("176","لالی","12");
INSERT INTO tcity VALUES("177","هندیجان","12");
INSERT INTO tcity VALUES("178","اندیکا","12");
INSERT INTO tcity VALUES("179","رامشیر","12");
INSERT INTO tcity VALUES("180","گتوند","12");
INSERT INTO tcity VALUES("181","هفتگل","12");
INSERT INTO tcity VALUES("182","هویزه","12");
INSERT INTO tcity VALUES("183","باوی","12");
INSERT INTO tcity VALUES("184","آغاجاری","12");
INSERT INTO tcity VALUES("185","حمیدیه","12");
INSERT INTO tcity VALUES("186","کارون","12");
INSERT INTO tcity VALUES("187","زنجان","13");
INSERT INTO tcity VALUES("188","ابهر","13");
INSERT INTO tcity VALUES("189","خدابنده","13");
INSERT INTO tcity VALUES("190","ماهنشان","13");
INSERT INTO tcity VALUES("191","ایجرود","13");
INSERT INTO tcity VALUES("192","خرمدره","13");
INSERT INTO tcity VALUES("193","طارم","13");
INSERT INTO tcity VALUES("194","سلطانیه","13");
INSERT INTO tcity VALUES("195","سمنان","14");
INSERT INTO tcity VALUES("196","شاهرود","14");
INSERT INTO tcity VALUES("197","دامغان","14");
INSERT INTO tcity VALUES("198","گرمسار","14");
INSERT INTO tcity VALUES("199","مهدی‌شهر","14");
INSERT INTO tcity VALUES("200","میامی","14");
INSERT INTO tcity VALUES("201","آرادان","14");
INSERT INTO tcity VALUES("202","سرخه","14");
INSERT INTO tcity VALUES("203","زابل","15");
INSERT INTO tcity VALUES("204","زاهدان","15");
INSERT INTO tcity VALUES("205","ایرانشهر","15");
INSERT INTO tcity VALUES("206","سراوان","15");
INSERT INTO tcity VALUES("207","چاه‌بهار","15");
INSERT INTO tcity VALUES("208","خاش","15");
INSERT INTO tcity VALUES("209","نیک شهر","15");
INSERT INTO tcity VALUES("210","سرباز","15");
INSERT INTO tcity VALUES("211","زهک","15");
INSERT INTO tcity VALUES("212","کنارک","15");
INSERT INTO tcity VALUES("213","دلگان","15");
INSERT INTO tcity VALUES("214","سیب و سوران","15");
INSERT INTO tcity VALUES("215","مهرستان","15");
INSERT INTO tcity VALUES("216","هیرمند","15");
INSERT INTO tcity VALUES("217","قصرقند","15");
INSERT INTO tcity VALUES("218","میرجاوه","15");
INSERT INTO tcity VALUES("219","هامون","15");
INSERT INTO tcity VALUES("220","نیمروز","15");
INSERT INTO tcity VALUES("221","بمپور","15");
INSERT INTO tcity VALUES("222","آباده","16");
INSERT INTO tcity VALUES("223","شیراز","16");
INSERT INTO tcity VALUES("224","فسا","16");
INSERT INTO tcity VALUES("225","لارستان","16");
INSERT INTO tcity VALUES("226","فیروزآباد","16");
INSERT INTO tcity VALUES("227","جهرم","16");
INSERT INTO tcity VALUES("228","کازرون","16");
INSERT INTO tcity VALUES("229","استهبان","16");
INSERT INTO tcity VALUES("230","داراب","16");
INSERT INTO tcity VALUES("231","نی ریز","16");
INSERT INTO tcity VALUES("232","ممسنی","16");
INSERT INTO tcity VALUES("233","اقلید","16");
INSERT INTO tcity VALUES("234","سپیدان","16");
INSERT INTO tcity VALUES("235","مرودشت","16");
INSERT INTO tcity VALUES("236","لامرد","16");
INSERT INTO tcity VALUES("237","بوانات","16");
INSERT INTO tcity VALUES("238","رستم","16");
INSERT INTO tcity VALUES("239","گراش","16");
INSERT INTO tcity VALUES("240","ارسنجان","16");
INSERT INTO tcity VALUES("241","خرم بید","16");
INSERT INTO tcity VALUES("242","زرین دشت","16");
INSERT INTO tcity VALUES("243","قیروکارزین","16");
INSERT INTO tcity VALUES("244","مهر","16");
INSERT INTO tcity VALUES("245","فراشبند","16");
INSERT INTO tcity VALUES("246","خنج","16");
INSERT INTO tcity VALUES("247","سروستان","16");
INSERT INTO tcity VALUES("248","پاسارگاد","16");
INSERT INTO tcity VALUES("249","خرامه","16");
INSERT INTO tcity VALUES("250","کوار","16");
INSERT INTO tcity VALUES("251","قزوین","17");
INSERT INTO tcity VALUES("252","تاکستان","17");
INSERT INTO tcity VALUES("253","بویین‌زهرا","17");
INSERT INTO tcity VALUES("254","آبیک","17");
INSERT INTO tcity VALUES("255","البرز","17");
INSERT INTO tcity VALUES("256","آوج","17");
INSERT INTO tcity VALUES("257","قم","18");
INSERT INTO tcity VALUES("258","خرم‌آباد","19");
INSERT INTO tcity VALUES("259","بروجرد","19");
INSERT INTO tcity VALUES("260","الیگودرز","19");
INSERT INTO tcity VALUES("261","دلفان","19");
INSERT INTO tcity VALUES("262","دورود","19");
INSERT INTO tcity VALUES("263","کوهدشت","19");
INSERT INTO tcity VALUES("264","ازنا","19");
INSERT INTO tcity VALUES("265","سلسله","19");
INSERT INTO tcity VALUES("266","پلدختر","19");
INSERT INTO tcity VALUES("267","دوره","19");
INSERT INTO tcity VALUES("268","تنکابن","20");
INSERT INTO tcity VALUES("269","ساری","20");
INSERT INTO tcity VALUES("270","بابل","20");
INSERT INTO tcity VALUES("271","قائم‌شهر","20");
INSERT INTO tcity VALUES("272","آمل","20");
INSERT INTO tcity VALUES("273","نوشهر","20");
INSERT INTO tcity VALUES("274","بهشهر","20");
INSERT INTO tcity VALUES("275","نور","20");
INSERT INTO tcity VALUES("276","رامسر","20");
INSERT INTO tcity VALUES("277","سوادکوه","20");
INSERT INTO tcity VALUES("278","بابلسر","20");
INSERT INTO tcity VALUES("279","محمودآباد","20");
INSERT INTO tcity VALUES("280","نکا","20");
INSERT INTO tcity VALUES("281","چالوس","20");
INSERT INTO tcity VALUES("282","جویبار","20");
INSERT INTO tcity VALUES("283","گلوگاه","20");
INSERT INTO tcity VALUES("284","فریدونکنار","20");
INSERT INTO tcity VALUES("285","عباس آباد","20");
INSERT INTO tcity VALUES("286","میاندرود","20");
INSERT INTO tcity VALUES("287","سیمرغ","20");
INSERT INTO tcity VALUES("288","اراک","21");
INSERT INTO tcity VALUES("289","ساوه","21");
INSERT INTO tcity VALUES("290","محلات","21");
INSERT INTO tcity VALUES("291","تفرش","21");
INSERT INTO tcity VALUES("292","خمین","21");
INSERT INTO tcity VALUES("293","آشتیان","21");
INSERT INTO tcity VALUES("294","دلیجان","21");
INSERT INTO tcity VALUES("295","شازند","21");
INSERT INTO tcity VALUES("296","زرندیه","21");
INSERT INTO tcity VALUES("297","کمیجان","21");
INSERT INTO tcity VALUES("298","خنداب","21");
INSERT INTO tcity VALUES("299","فراهان","21");
INSERT INTO tcity VALUES("300","بندرعباس","22");
INSERT INTO tcity VALUES("301","بندر لنگه","22");
INSERT INTO tcity VALUES("302","میناب","22");
INSERT INTO tcity VALUES("303","قشم","22");
INSERT INTO tcity VALUES("304","ابوموسی","22");
INSERT INTO tcity VALUES("305","جاسک","22");
INSERT INTO tcity VALUES("306","رودان","22");
INSERT INTO tcity VALUES("307","حاجی‌آباد","22");
INSERT INTO tcity VALUES("308","بستک","22");
INSERT INTO tcity VALUES("309","بشاگرد","22");
INSERT INTO tcity VALUES("310","خمیر","22");
INSERT INTO tcity VALUES("311","پارسیان","22");
INSERT INTO tcity VALUES("312","سیریک","22");
INSERT INTO tcity VALUES("313","ملایر","23");
INSERT INTO tcity VALUES("314","همدان","23");
INSERT INTO tcity VALUES("315","نهاوند","23");
INSERT INTO tcity VALUES("316","تویسرکان","23");
INSERT INTO tcity VALUES("317","کبودرآهنگ","23");
INSERT INTO tcity VALUES("318","اسدآباد","23");
INSERT INTO tcity VALUES("319","بهار","23");
INSERT INTO tcity VALUES("320","رزن","23");
INSERT INTO tcity VALUES("321","فامنین","23");
INSERT INTO tcity VALUES("322","شهرکرد","24");
INSERT INTO tcity VALUES("323","بروجن","24");
INSERT INTO tcity VALUES("324","فارسان","24");
INSERT INTO tcity VALUES("325","لردگان","24");
INSERT INTO tcity VALUES("326","اردل","24");
INSERT INTO tcity VALUES("327","کوهرنگ","24");
INSERT INTO tcity VALUES("328","کیار","24");
INSERT INTO tcity VALUES("329","سامان","24");
INSERT INTO tcity VALUES("330","بن","24");
INSERT INTO tcity VALUES("331","لنده","24");
INSERT INTO tcity VALUES("332","بیجار","25");
INSERT INTO tcity VALUES("333","سنندج","25");
INSERT INTO tcity VALUES("334","سقز","25");
INSERT INTO tcity VALUES("335","بانه","25");
INSERT INTO tcity VALUES("336","مریوان","25");
INSERT INTO tcity VALUES("337","قروه","25");
INSERT INTO tcity VALUES("338","دیواندره","25");
INSERT INTO tcity VALUES("339","کامیاران","25");
INSERT INTO tcity VALUES("340","سروآباد","25");
INSERT INTO tcity VALUES("341","دهگلان","25");
INSERT INTO tcity VALUES("342","بم","26");
INSERT INTO tcity VALUES("343","کرمان","26");
INSERT INTO tcity VALUES("344","سیرجان","26");
INSERT INTO tcity VALUES("345","رفسنجان","26");
INSERT INTO tcity VALUES("346","جیرفت","26");
INSERT INTO tcity VALUES("347","بافت","26");
INSERT INTO tcity VALUES("348","زرند","26");
INSERT INTO tcity VALUES("349","شهربابک","26");
INSERT INTO tcity VALUES("350","بردسیر","26");
INSERT INTO tcity VALUES("351","کهنوج","26");
INSERT INTO tcity VALUES("352","راور","26");
INSERT INTO tcity VALUES("353","منوجان","26");
INSERT INTO tcity VALUES("354","عنبرآباد","26");
INSERT INTO tcity VALUES("355","کوهبنان","26");
INSERT INTO tcity VALUES("356","رودبار جنوب","26");
INSERT INTO tcity VALUES("357","قلعه گنج","26");
INSERT INTO tcity VALUES("358","ریگان","26");
INSERT INTO tcity VALUES("359","انار","26");
INSERT INTO tcity VALUES("360","رابر","26");
INSERT INTO tcity VALUES("361","فهرج","26");
INSERT INTO tcity VALUES("362","ارزوئیه","26");
INSERT INTO tcity VALUES("363","فاریاب","26");
INSERT INTO tcity VALUES("364","نرماشیر","26");
INSERT INTO tcity VALUES("365","اسلام‌آباد غرب","27");
INSERT INTO tcity VALUES("366","کرمانشاه","27");
INSERT INTO tcity VALUES("367","قصرشیرین","27");
INSERT INTO tcity VALUES("368","پاوه","27");
INSERT INTO tcity VALUES("369","سنقر","27");
INSERT INTO tcity VALUES("370","سرپل ذهاب","27");
INSERT INTO tcity VALUES("371","کنگاور","27");
INSERT INTO tcity VALUES("372","گیلانغرب","27");
INSERT INTO tcity VALUES("373","جوانرود","27");
INSERT INTO tcity VALUES("374","صحنه","27");
INSERT INTO tcity VALUES("375","هرسین","27");
INSERT INTO tcity VALUES("376","ثلاث باباجانی","27");
INSERT INTO tcity VALUES("377","روانسر","27");
INSERT INTO tcity VALUES("378","دالاهو","27");
INSERT INTO tcity VALUES("379","کهگیلویه","28");
INSERT INTO tcity VALUES("380","بویراحمد","28");
INSERT INTO tcity VALUES("381","گچساران","28");
INSERT INTO tcity VALUES("382","دنا","28");
INSERT INTO tcity VALUES("383","بهمئی","28");
INSERT INTO tcity VALUES("384","باشت","28");
INSERT INTO tcity VALUES("385","چرام","28");
INSERT INTO tcity VALUES("386","گرگان","29");
INSERT INTO tcity VALUES("387","گنبد کاووس","29");
INSERT INTO tcity VALUES("388","ترکمن","29");
INSERT INTO tcity VALUES("389","علی‌آباد","29");
INSERT INTO tcity VALUES("390","کردکوی","29");
INSERT INTO tcity VALUES("391","مینودشت","29");
INSERT INTO tcity VALUES("392","بندر گز","29");
INSERT INTO tcity VALUES("393","آق قلا","29");
INSERT INTO tcity VALUES("394","کلاله","29");
INSERT INTO tcity VALUES("395","آزادشهر","29");
INSERT INTO tcity VALUES("396","رامیان","29");
INSERT INTO tcity VALUES("397","مراوه تپه","29");
INSERT INTO tcity VALUES("398","گمیشان","29");
INSERT INTO tcity VALUES("399","گالیکش","29");
INSERT INTO tcity VALUES("400","رشت","30");
INSERT INTO tcity VALUES("401","بندر انزلی","30");
INSERT INTO tcity VALUES("402","تالش","30");
INSERT INTO tcity VALUES("403","فومن","30");
INSERT INTO tcity VALUES("404","لاهیجان","30");
INSERT INTO tcity VALUES("405","آستارا","30");
INSERT INTO tcity VALUES("406","رودسر","30");
INSERT INTO tcity VALUES("407","رودبار","30");
INSERT INTO tcity VALUES("408","صومعه سرا","30");
INSERT INTO tcity VALUES("409","لنگرود","30");
INSERT INTO tcity VALUES("410","آستانه اشرفیه","30");
INSERT INTO tcity VALUES("411","شفت","30");
INSERT INTO tcity VALUES("412","املش","30");
INSERT INTO tcity VALUES("413","رضوانشهر","30");
INSERT INTO tcity VALUES("414","سیاهکل","30");
INSERT INTO tcity VALUES("415","ماسال","30");
INSERT INTO tcity VALUES("416","یزد","31");
INSERT INTO tcity VALUES("417","اردکان","31");
INSERT INTO tcity VALUES("418","بافق","31");
INSERT INTO tcity VALUES("419","تفت","31");
INSERT INTO tcity VALUES("420","مهریز","31");
INSERT INTO tcity VALUES("421","میبد","31");
INSERT INTO tcity VALUES("422","ابرکوه","31");
INSERT INTO tcity VALUES("423","صدوق","31");
INSERT INTO tcity VALUES("424","خاتم","31");
INSERT INTO tcity VALUES("425","بهاباد","31");



DROP TABLE IF EXISTS tcolor;

CREATE TABLE `tcolor` (
  `ColorID` int(11) NOT NULL AUTO_INCREMENT,
  `ColorName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`ColorID`),
  KEY `FK_color_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_color_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcolor VALUES("1","سفید","1");
INSERT INTO tcolor VALUES("2","سیاه","1");
INSERT INTO tcolor VALUES("3","آبی","1");
INSERT INTO tcolor VALUES("4","قرمز","1");
INSERT INTO tcolor VALUES("5","زرد","1");
INSERT INTO tcolor VALUES("6","خاکستری","1");
INSERT INTO tcolor VALUES("7","سرمه ای","1");
INSERT INTO tcolor VALUES("8","ارغوانی","");
INSERT INTO tcolor VALUES("9","ارکیده","");
INSERT INTO tcolor VALUES("10","ارکیده بنفش","");
INSERT INTO tcolor VALUES("11","ارکیده سیر","");
INSERT INTO tcolor VALUES("12","استخوانی","");
INSERT INTO tcolor VALUES("13","آبی","");
INSERT INTO tcolor VALUES("14","آبی-بنفش سیر","");
INSERT INTO tcolor VALUES("15","آبی آسمانی","");
INSERT INTO tcolor VALUES("16","آبی آسمانی سیر","");
INSERT INTO tcolor VALUES("17","آبی بنفش","");
INSERT INTO tcolor VALUES("18","آبی دریایی","");
INSERT INTO tcolor VALUES("19","آبی دودی","");
INSERT INTO tcolor VALUES("20","آبی روشن","");
INSERT INTO tcolor VALUES("21","آبی سیر","");
INSERT INTO tcolor VALUES("22","آبی فولادی","");
INSERT INTO tcolor VALUES("23","آبی کبریتی","");
INSERT INTO tcolor VALUES("24","آبی کبریتی روشن","");
INSERT INTO tcolor VALUES("25","آبی کدر","");
INSERT INTO tcolor VALUES("26","آبی کمرنگ","");
INSERT INTO tcolor VALUES("27","آبی لجنی","");
INSERT INTO tcolor VALUES("28","آبی متالیک روشن","");
INSERT INTO tcolor VALUES("29","آبی محو","");
INSERT INTO tcolor VALUES("30","آبی نفتی","");
INSERT INTO tcolor VALUES("31","آلبالویی","");
INSERT INTO tcolor VALUES("32","بادامی سیر","");
INSERT INTO tcolor VALUES("33","بادمجانی","");
INSERT INTO tcolor VALUES("34","بادمجانی روشن","");
INSERT INTO tcolor VALUES("35","برنزه کدر","");
INSERT INTO tcolor VALUES("36","بژ","");
INSERT INTO tcolor VALUES("37","بژ باز","");
INSERT INTO tcolor VALUES("38","بژ تیره","");
INSERT INTO tcolor VALUES("39","بژ تیره","");
INSERT INTO tcolor VALUES("40","بژ روشن","");
INSERT INTO tcolor VALUES("41","بنفش","");
INSERT INTO tcolor VALUES("42","بنفش باز","");
INSERT INTO tcolor VALUES("43","بنفش روشن","");
INSERT INTO tcolor VALUES("44","بنفش کدر","");
INSERT INTO tcolor VALUES("45","بنفش مایل به آبی","");
INSERT INTO tcolor VALUES("46","پوست پیازی","");
INSERT INTO tcolor VALUES("47","توسی","");
INSERT INTO tcolor VALUES("48","حناییِ روشن","");
INSERT INTO tcolor VALUES("49","خاکستری","");
INSERT INTO tcolor VALUES("50","خاکستری سیر","");
INSERT INTO tcolor VALUES("51","خاکستری مات","");
INSERT INTO tcolor VALUES("52","خاکستری محو","");
INSERT INTO tcolor VALUES("53","خاکی","");
INSERT INTO tcolor VALUES("54","خاکی","");
INSERT INTO tcolor VALUES("55","خردلی","");
INSERT INTO tcolor VALUES("56","خزه‌ای","");
INSERT INTO tcolor VALUES("57","خزه‌ای پررنگ","");
INSERT INTO tcolor VALUES("58","دودی","");
INSERT INTO tcolor VALUES("59","زرد","");
INSERT INTO tcolor VALUES("60","زرشکی","");
INSERT INTO tcolor VALUES("61","زیتونی","");
INSERT INTO tcolor VALUES("62","زیتونی سیر","");
INSERT INTO tcolor VALUES("63","سبز","");
INSERT INTO tcolor VALUES("64","سبز ارتشی","");
INSERT INTO tcolor VALUES("65","سبز آووکادو","");
INSERT INTO tcolor VALUES("66","سبز چمنی","");
INSERT INTO tcolor VALUES("67","سبز دریایی","");
INSERT INTO tcolor VALUES("68","سبز دریایی تیره","");
INSERT INTO tcolor VALUES("69","سبز دریایی روشن","");
INSERT INTO tcolor VALUES("70","سبز دودی","");
INSERT INTO tcolor VALUES("71","سبز روشن","");
INSERT INTO tcolor VALUES("72","سبز کبریتی تیره","");
INSERT INTO tcolor VALUES("73","سبز کبریتی روشن","");
INSERT INTO tcolor VALUES("74","سبز کدر","");
INSERT INTO tcolor VALUES("75","سبز کمرنگ","");
INSERT INTO tcolor VALUES("76","سبز لجنی","");
INSERT INTO tcolor VALUES("77","سربی","");
INSERT INTO tcolor VALUES("78","سربی تیره","");
INSERT INTO tcolor VALUES("79","سرخابی","");
INSERT INTO tcolor VALUES("80","سرخابی","");
INSERT INTO tcolor VALUES("81","سرخابی روشن","");
INSERT INTO tcolor VALUES("82","سرمه‌ای","");
INSERT INTO tcolor VALUES("83","سفید","");
INSERT INTO tcolor VALUES("84","سفید بنفشه","");
INSERT INTO tcolor VALUES("85","سفید نعنائی","");
INSERT INTO tcolor VALUES("86","سیاه","");
INSERT INTO tcolor VALUES("87","شرابی","");
INSERT INTO tcolor VALUES("88","شرابی روشن","");
INSERT INTO tcolor VALUES("89","شفقی","");
INSERT INTO tcolor VALUES("90","شویدی","");
INSERT INTO tcolor VALUES("91","شیرشکری","");
INSERT INTO tcolor VALUES("92","شیری","");
INSERT INTO tcolor VALUES("93","صورتی","");
INSERT INTO tcolor VALUES("94","صورتی پررنگ","");
INSERT INTO tcolor VALUES("95","صورتی مات","");
INSERT INTO tcolor VALUES("96","صورتی محو","");
INSERT INTO tcolor VALUES("97","عسلی پررنگ","");
INSERT INTO tcolor VALUES("98","عنابی تند","");
INSERT INTO tcolor VALUES("99","فیروزه‌ای","");
INSERT INTO tcolor VALUES("100","فیروزه‌ای تیره","");
INSERT INTO tcolor VALUES("101","فیروزه‌ای سیر","");
INSERT INTO tcolor VALUES("102","فیروزه‌ای فسفری","");
INSERT INTO tcolor VALUES("103","فیروزه‌ای کدر","");
INSERT INTO tcolor VALUES("104","قرمز","");
INSERT INTO tcolor VALUES("105","قرمز-نارنجی","");
INSERT INTO tcolor VALUES("106","قرمز گوجه‌ای","");
INSERT INTO tcolor VALUES("107","قهوه‌ای","");
INSERT INTO tcolor VALUES("108","قهوه‌ایِ حنایی","");
INSERT INTO tcolor VALUES("109","قهوه‌ای متوسط","");
INSERT INTO tcolor VALUES("110","کاکائویی","");
INSERT INTO tcolor VALUES("111","کاهگلی","");
INSERT INTO tcolor VALUES("112","کاهی","");
INSERT INTO tcolor VALUES("113","کتانی","");
INSERT INTO tcolor VALUES("114","کرم","");
INSERT INTO tcolor VALUES("115","کرم سیر","");
INSERT INTO tcolor VALUES("116","کرم نارنجی","");
INSERT INTO tcolor VALUES("117","کهربایی باز","");
INSERT INTO tcolor VALUES("118","گندمی","");
INSERT INTO tcolor VALUES("119","لاجوردی","");
INSERT INTO tcolor VALUES("120","لجنی تیره","");
INSERT INTO tcolor VALUES("121","لیمویی روشن","");
INSERT INTO tcolor VALUES("122","ماشی","");
INSERT INTO tcolor VALUES("123","ماشی سیر","");
INSERT INTO tcolor VALUES("124","مخملی","");
INSERT INTO tcolor VALUES("125","مغزپسته‌ای","");
INSERT INTO tcolor VALUES("126","مغزپسته‌ای","");
INSERT INTO tcolor VALUES("127","مغزپسته‌ای پررنگ","");
INSERT INTO tcolor VALUES("128","نارنجی","");
INSERT INTO tcolor VALUES("129","نارنجی پررنگ","");
INSERT INTO tcolor VALUES("130","نارنجی سیر","");
INSERT INTO tcolor VALUES("131","نارنجی کرم","");
INSERT INTO tcolor VALUES("132","نخودی","");
INSERT INTO tcolor VALUES("133","نقره‌ای","");
INSERT INTO tcolor VALUES("134","نیلی","");
INSERT INTO tcolor VALUES("135","نیلی سیر","");
INSERT INTO tcolor VALUES("136","نیلی کمرنگ","");
INSERT INTO tcolor VALUES("137","نیلی متالیک","");
INSERT INTO tcolor VALUES("138","نیلی محو","");
INSERT INTO tcolor VALUES("139","هِلی","");
INSERT INTO tcolor VALUES("140","هلویی","");
INSERT INTO tcolor VALUES("141","هلویی پررنگ","");
INSERT INTO tcolor VALUES("142","هلویی روشن","");
INSERT INTO tcolor VALUES("143","هلویی سیر","");
INSERT INTO tcolor VALUES("144","یشمی","");
INSERT INTO tcolor VALUES("145","یشمی سیر","");
INSERT INTO tcolor VALUES("146","یشمی کمرنگ","");
INSERT INTO tcolor VALUES("147","یشمی محو","");



DROP TABLE IF EXISTS tcompany;

CREATE TABLE `tcompany` (
  `Comcode` int(11) NOT NULL AUTO_INCREMENT,
  `Comname` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Comcode`),
  KEY `FK_company_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_company_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tcompany VALUES("1","ایران خودرو","1");
INSERT INTO tcompany VALUES("2","سایپا","1");
INSERT INTO tcompany VALUES("3","نیسان","1");
INSERT INTO tcompany VALUES("4","مزدا","1");
INSERT INTO tcompany VALUES("5","بنز","1");
INSERT INTO tcompany VALUES("6","فوجیتسو","1");
INSERT INTO tcompany VALUES("7","اسکانیا","1");
INSERT INTO tcompany VALUES("8","JONYANG","1");
INSERT INTO tcompany VALUES("9","CHANGLIN","1");
INSERT INTO tcompany VALUES("10","کوماتسو","1");
INSERT INTO tcompany VALUES("11","ماک","1");
INSERT INTO tcompany VALUES("12","ولوو","1");
INSERT INTO tcompany VALUES("13","داف","1");
INSERT INTO tcompany VALUES("14","هپکو","1");
INSERT INTO tcompany VALUES("15","کاترپیلار","1");
INSERT INTO tcompany VALUES("16","تِرکس ","1");
INSERT INTO tcompany VALUES("17","جی سی بی","1");
INSERT INTO tcompany VALUES("18","جان دییر","1");
INSERT INTO tcompany VALUES("19","ایران فایر","1");
INSERT INTO tcompany VALUES("20","رنو","1");
INSERT INTO tcompany VALUES("21","پژو","1");
INSERT INTO tcompany VALUES("22","لیفان","1");
INSERT INTO tcompany VALUES("23","هیوندای","1");
INSERT INTO tcompany VALUES("24","MVM","1");
INSERT INTO tcompany VALUES("25","چانگان","1");
INSERT INTO tcompany VALUES("26","JAC","1");
INSERT INTO tcompany VALUES("27","هایما","1");
INSERT INTO tcompany VALUES("28","MG","1");
INSERT INTO tcompany VALUES("29","فولکس واگن","1");
INSERT INTO tcompany VALUES("30","بایک","1");
INSERT INTO tcompany VALUES("31","برلیانس","1");
INSERT INTO tcompany VALUES("32","بسترن","1");
INSERT INTO tcompany VALUES("33","فاو","1");
INSERT INTO tcompany VALUES("34","دانگ فنگ","1");
INSERT INTO tcompany VALUES("35","بی ام و","1");
INSERT INTO tcompany VALUES("36","تویوتا","1");
INSERT INTO tcompany VALUES("37","پورشه","1");
INSERT INTO tcompany VALUES("38","جیلی","1");
INSERT INTO tcompany VALUES("39","چری","1");
INSERT INTO tcompany VALUES("40","زوتی","1");
INSERT INTO tcompany VALUES("41","سانگ یانگ","1");
INSERT INTO tcompany VALUES("42","سوزوکی","1");
INSERT INTO tcompany VALUES("43","کیا","1");
INSERT INTO tcompany VALUES("44","سمند سریر","1");
INSERT INTO tcompany VALUES("45","پراید 111","1");
INSERT INTO tcompany VALUES("46","پراید 131","1");
INSERT INTO tcompany VALUES("47","پراید 141","1");
INSERT INTO tcompany VALUES("48","تیبا1","1");
INSERT INTO tcompany VALUES("49","تیبا 2","1");
INSERT INTO tcompany VALUES("50","هوندا","1");



DROP TABLE IF EXISTS tdept;

CREATE TABLE `tdept` (
  `Dcode` int(11) NOT NULL AUTO_INCREMENT,
  `Dname` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Dcode`),
  KEY `FK_dept_vahed_idx` (`FK_Vcode`),
  CONSTRAINT `FK_dept_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tdept VALUES("1","مدیریت","1");
INSERT INTO tdept VALUES("2","امور مالی","1");
INSERT INTO tdept VALUES("3","نقلیه","1");
INSERT INTO tdept VALUES("4","حراست","1");
INSERT INTO tdept VALUES("5","منابع انسانی","1");
INSERT INTO tdept VALUES("6","فناوری اطلاعات","1");



DROP TABLE IF EXISTS tehdafrom;

CREATE TABLE `tehdafrom` (
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VcodeEhda` int(11) NOT NULL,
  `Sh_mojavez` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Date_mojavez` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `EhdaDate_s` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`EhdaDate_s`),
  KEY `FK_ehda_vahedEhda_idx` (`FK_VcodeEhda`),
  KEY `FK_ehda_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_ehda_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ehda_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ehda_vahedEhda` FOREIGN KEY (`FK_VcodeEhda`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tehdato;

CREATE TABLE `tehdato` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode_S` int(11) NOT NULL,
  `FK_VCode_D` int(11) NOT NULL,
  `EhdaDate_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mojavez_sh` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `MojavezDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`EhdaDate_S`),
  KEY `FK_enteghal_vahedS_idx` (`FK_VCode_S`),
  KEY `FK_enteghal_vahedD_idx` (`FK_VCode_D`),
  CONSTRAINT `FK_ehdato_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ehdato_vahedD` FOREIGN KEY (`FK_VCode_D`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ehdato_vahedS` FOREIGN KEY (`FK_VCode_S`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tejarefrom;

CREATE TABLE `tejarefrom` (
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VcodeEjare` int(11) NOT NULL,
  `Sh_mojavez` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `date_mojavez` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vcode` int(11) NOT NULL,
  `EjarePrice` int(11) NOT NULL,
  `EjareDate_s` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `EjareDate_f` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Kilometer` int(11) DEFAULT NULL,
  `Tozihat` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`EjareDate_s`),
  KEY `FK_ejarefrom_vahed_idx` (`FK_VcodeEjare`),
  KEY `FK_ejarefrom_Vcode_idx` (`FK_Vcode`),
  CONSTRAINT `FK_ejarefrom_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ejarefrom_vahed` FOREIGN KEY (`FK_VcodeEjare`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ejarefrom_Vcode` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS temployee;

CREATE TABLE `temployee` (
  `Codemeli` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Ename` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Family` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `Birthday` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Empno` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Madrak` varchar(40) COLLATE utf8_persian_ci DEFAULT NULL,
  `Jensiat` tinyint(4) DEFAULT NULL,
  `FK_Vcode` int(11) NOT NULL,
  `FK_Dcode` int(11) NOT NULL,
  `mobile` varchar(11) COLLATE utf8_persian_ci NOT NULL,
  `Workphone` varchar(13) COLLATE utf8_persian_ci DEFAULT NULL,
  `Address` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  `Listype` tinyint(4) DEFAULT NULL,
  `Lisdate` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `LisAuthdate` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Password` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `LastLogin` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Salt` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Ranande` tinyint(4) DEFAULT NULL,
  `Karpardaz` tinyint(4) DEFAULT NULL,
  `Commit` tinyint(4) DEFAULT NULL,
  `ModirVahed` tinyint(4) DEFAULT NULL,
  `ModirMain` tinyint(4) DEFAULT NULL,
  `Email` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `CanLogin` tinyint(4) DEFAULT NULL,
  `Other` tinyint(4) DEFAULT NULL,
  `GcmID` text COLLATE utf8_persian_ci,
  PRIMARY KEY (`Codemeli`),
  KEY `FK_employee_vahed_idx` (`FK_Vcode`),
  KEY `FK_employee_dept_idx` (`FK_Dcode`),
  CONSTRAINT `FK_employee_dept` FOREIGN KEY (`FK_Dcode`) REFERENCES `tdept` (`Dcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_employee_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO temployee VALUES("4490239068","ساسان","ابراهیمی","734602210","12","8","1","1","2","09331031001","08433343743","ایلام","3","1353489010","1511341810","1","4490271891","1483536859","$2y$11$833a91d85de6ed71a3f11uedfx4DP7VBFhX6z.19wpED3NbzxsZoi","","$2y$11$833a91d85de6ed71a3f115","1","1","1","","0","mortezamoradifard6@gmail.com","1","1","");
INSERT INTO temployee VALUES("4490271891","محمد","رحیمی","765187810","1","8","1","1","1","09217150848","08433333333","ایلام","3","1337155810","1495008610","1","4490271891","1483614166","$2y$11$d9b3291ddc377b0196384OO51lt3nrTF6YUUZuDcK3F2Rq.iGGnRO","1484122700","$2y$11$d9b3291ddc377b0196384b","1","1","1","1","1","miladkazemi369@gmail.com","1","1","dxdk_hWXNH4:APA91bGm-S_1MmcAV3mwYG790H9_yTFcJaMlb3viqnGMf6bDqlJvwoJJoDGffMdxhOYuXE9kyLPnZaNQqg3HuGAB3Ge5rfSPULcrSqL00djW-B5NMO3W0LNt3apNtI1jHJS6fFSbXcmH");



DROP TABLE IF EXISTS tenteghal;

CREATE TABLE `tenteghal` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode_S` int(11) NOT NULL,
  `FK_VCode_D` int(11) NOT NULL,
  `EnteghalDate_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `EnteghalDate_F` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mojavez_sh` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `MojavezDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `EnteghalType` tinyint(4) NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `EnteghalPrice` int(11) NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`EnteghalDate_S`),
  KEY `FK_enteghal_vahedS_idx` (`FK_VCode_S`),
  KEY `FK_enteghal_vahedD_idx` (`FK_VCode_D`),
  CONSTRAINT `FK_enteghal_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_enteghal_vahedD` FOREIGN KEY (`FK_VCode_D`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_enteghal_vahedS` FOREIGN KEY (`FK_VCode_S`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tesghat;

CREATE TABLE `tesghat` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `EsghatDate_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mojavez_sh` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `MojavezDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`EsghatDate_S`),
  KEY `FK_enteghal_vahedS_idx` (`FK_VCode`),
  CONSTRAINT `FK_esghat_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_esghat_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tfactoratt;

CREATE TABLE `tfactoratt` (
  `FK_TamirID` int(11) NOT NULL,
  `DateS` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FilePath` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_TamirID`,`DateS`),
  CONSTRAINT `FK_factorAtt_tamir` FOREIGN KEY (`FK_TamirID`) REFERENCES `ttamir` (`TamirID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tfani;

CREATE TABLE `tfani` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Date_M` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `Date_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Date_F` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mab_Pay` int(11) NOT NULL,
  `Markaz` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `AddressMarkaz` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `Result` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`FK_Pelak`,`Date_M`),
  KEY `FK_fani_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_fani_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_fani_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tfani VALUES("9812ج637","1482311410","1","123","1482311410","1513933810","100000","1","1","1","4490271891","1483600051","1","4490271891","4490271891");



DROP TABLE IF EXISTS tforosh;

CREATE TABLE `tforosh` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `ForoshDate_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mojavez_sh` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `MojavezDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `ForoshType` tinyint(4) NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `ForoshPrice` int(11) NOT NULL,
  `Kharidar` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `TellKharidar` varchar(15) COLLATE utf8_persian_ci NOT NULL,
  `AddressKharidar` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`ForoshDate_S`),
  KEY `FK_enteghal_vahedS_idx` (`FK_VCode`),
  CONSTRAINT `FK_forosh_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_forosh_vahedS` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tforoshgah;

CREATE TABLE `tforoshgah` (
  `CodeF` int(11) NOT NULL AUTO_INCREMENT,
  `NameF` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_CityCode` int(11) NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Tell` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `Address` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`CodeF`),
  KEY `FK_foroshgah_city_idx` (`FK_CityCode`),
  KEY `FK_foroshgah_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_foroshgah_city` FOREIGN KEY (`FK_CityCode`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foroshgah_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tforoshgah VALUES("1","فروشگاه ایران خودرو","78","1","084333333333","ایلام");
INSERT INTO tforoshgah VALUES("2","فروشگاه سایپا","78","1","08433333333","ایلام");



DROP TABLE IF EXISTS thavades;

CREATE TABLE `thavades` (
  `HadeseID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Date_V` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Htype` int(11) NOT NULL,
  `NeedPay` tinyint(4) NOT NULL,
  `Date_P` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `NeedRepair` tinyint(4) NOT NULL,
  `FK_Ccity` int(11) NOT NULL,
  `CarStatus` tinyint(4) NOT NULL,
  `Date_RafHadese` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Comment` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`HadeseID`),
  KEY `FK_hadese_car_idx` (`FK_Pelak`),
  KEY `FK_hadese_vahed_idx` (`FK_VCode`),
  KEY `FK_hadese_havadesType_idx` (`FK_Htype`),
  KEY `FK_hadese_city_idx` (`FK_Ccity`),
  CONSTRAINT `FK_hadese_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hadese_city` FOREIGN KEY (`FK_Ccity`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hadese_havadesType` FOREIGN KEY (`FK_Htype`) REFERENCES `thavadestype` (`Htype`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hadese_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS thavadestype;

CREATE TABLE `thavadestype` (
  `Htype` int(11) NOT NULL AUTO_INCREMENT,
  `Hname` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Htype`),
  KEY `FK_havadestype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_havadestype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO thavadestype VALUES("1","تصادف","1");
INSERT INTO thavadestype VALUES("2","سیل","1");
INSERT INTO thavadestype VALUES("3","آتش سوزی","1");
INSERT INTO thavadestype VALUES("4","سرقت","1");
INSERT INTO thavadestype VALUES("5","زلزله","1");
INSERT INTO thavadestype VALUES("6","صاعقه","1");
INSERT INTO thavadestype VALUES("7","سایر","1");



DROP TABLE IF EXISTS tinsu;

CREATE TABLE `tinsu` (
  `CodeI` int(11) NOT NULL AUTO_INCREMENT,
  `NameI` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `Teel` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `Address` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  `insuorgcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`CodeI`),
  KEY `FK_insu_vahed_idx` (`FK_VCode`),
  KEY `FK_insu_insuorg_idx` (`insuorgcode`),
  CONSTRAINT `FK_insu_insuorg` FOREIGN KEY (`insuorgcode`) REFERENCES `tinsuorg` (`insuorgcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_insu_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tinsu VALUES("1","شرکت بیمه ایران","08433333333","ایلام","1","1");
INSERT INTO tinsu VALUES("2","شرکت بیمه دانا","08433333333","ایلام","1","2");
INSERT INTO tinsu VALUES("3","شرکت بیمه پاسارگارد","08433333333","ایلام","1","5");



DROP TABLE IF EXISTS tinsuorg;

CREATE TABLE `tinsuorg` (
  `insuorgcode` int(11) NOT NULL AUTO_INCREMENT,
  `insuorgname` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`insuorgcode`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tinsuorg VALUES("1","بیمه آرمان");
INSERT INTO tinsuorg VALUES("2","بیمه آسماری");
INSERT INTO tinsuorg VALUES("3","بیمه تعاون");
INSERT INTO tinsuorg VALUES("4","بیمه سرمد");
INSERT INTO tinsuorg VALUES("5","بیمه ما");
INSERT INTO tinsuorg VALUES("6","پایگاه اطلاع رسانی بیمه مرکزی");
INSERT INTO tinsuorg VALUES("7","پژوهشکده بیمه");
INSERT INTO tinsuorg VALUES("8","شركت بيمه بهمن");
INSERT INTO tinsuorg VALUES("9","شركت بيمه كوثر");
INSERT INTO tinsuorg VALUES("10","شركت بیمه اتکایی ایرانیان");
INSERT INTO tinsuorg VALUES("11","شركت سرمايه‌گذاري ‌صنعت بيمه");
INSERT INTO tinsuorg VALUES("12","شرکت بيمه اتكايي امين");
INSERT INTO tinsuorg VALUES("13","شرکت بيمه البرز");
INSERT INTO tinsuorg VALUES("14","شرکت بيمه اميد");
INSERT INTO tinsuorg VALUES("15","شرکت بيمه ايران");
INSERT INTO tinsuorg VALUES("16","شرکت بيمه ايران معين");
INSERT INTO tinsuorg VALUES("17","شرکت بيمه آسيا");
INSERT INTO tinsuorg VALUES("18","شرکت بيمه پارسيان");
INSERT INTO tinsuorg VALUES("19","شرکت بيمه توسعه");
INSERT INTO tinsuorg VALUES("20","شرکت بيمه حافظ");
INSERT INTO tinsuorg VALUES("21","شرکت بيمه دانا");
INSERT INTO tinsuorg VALUES("22","شرکت بيمه دي");
INSERT INTO tinsuorg VALUES("23","شرکت بيمه رازي");
INSERT INTO tinsuorg VALUES("24","شرکت بيمه سامان");
INSERT INTO tinsuorg VALUES("25","شرکت بيمه سينا");
INSERT INTO tinsuorg VALUES("26","شرکت بيمه كارآفرين");
INSERT INTO tinsuorg VALUES("27","شرکت بيمه معلم");
INSERT INTO tinsuorg VALUES("28","شرکت بيمه ملت");
INSERT INTO tinsuorg VALUES("29","شرکت بيمه ميهن");
INSERT INTO tinsuorg VALUES("30","شرکت بيمه نوين");
INSERT INTO tinsuorg VALUES("31","شرکت بیمه پاسارگاد");
INSERT INTO tinsuorg VALUES("32","شرکت خدمات بیمه ای پیشگامان دانا");
INSERT INTO tinsuorg VALUES("33","شرکت خدمات بیمه رایان سایپا");
INSERT INTO tinsuorg VALUES("34","صندوق بيمه كشاورزي");
INSERT INTO tinsuorg VALUES("35","موسسه آموزش عالي بيمه(اكو)");



DROP TABLE IF EXISTS tinsuuse;

CREATE TABLE `tinsuuse` (
  `InsuUseID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_ShBimeNameh` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `FK_HadeseID` int(11) NOT NULL,
  `DarsadMogh` int(11) NOT NULL,
  `NameTaraf` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `TellTaraf` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `AddressTaraf` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `MablaghDariafti` int(11) DEFAULT NULL,
  `MablaghPardakhty` int(11) DEFAULT NULL,
  `Date_E` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`InsuUseID`),
  KEY `FK_insuuse_bimenameh_idx` (`FK_ShBimeNameh`),
  KEY `FK_insuuse_havades_idx` (`FK_HadeseID`),
  KEY `FK_insuuse_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_insuuse_bimenameh` FOREIGN KEY (`FK_ShBimeNameh`) REFERENCES `tbimenameh` (`ShomareBime`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_insuuse_havades` FOREIGN KEY (`FK_HadeseID`) REFERENCES `thavades` (`HadeseID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_insuuse_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tjarime;

CREATE TABLE `tjarime` (
  `JarimeID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_Codemeli` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `FK_CCode` int(11) NOT NULL,
  `JarimeType` tinyint(4) NOT NULL,
  `JarimePrice` int(11) NOT NULL,
  `JarimeDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `PardakhtDate` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `PardakhtPrice` int(11) DEFAULT NULL,
  `NumberFish` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `BankName` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `JarimeAddress` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`JarimeID`),
  KEY `FK_jarime_car_idx` (`FK_Pelak`),
  KEY `FK_jarime_employee_idx` (`FK_Codemeli`),
  KEY `FK_jarime_vahed_idx` (`FK_VCode`),
  KEY `FK_jarime_city_idx` (`FK_CCode`),
  CONSTRAINT `FK_jarime_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jarime_city` FOREIGN KEY (`FK_CCode`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jarime_employee` FOREIGN KEY (`FK_Codemeli`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jarime_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tjarime VALUES("1","9812ج637","4490271891","1","78","1","120000","1482311410","1","1482397810","120000","1","1","1","4490271891","1483600419","1","4490271891","4490271891");



DROP TABLE IF EXISTS tkarkerd;

CREATE TABLE `tkarkerd` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `DateKarkerd` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Shift` int(11) DEFAULT NULL,
  PRIMARY KEY (`FK_Pelak`,`DateKarkerd`),
  KEY `FK_karkerd_vahed_idx` (`FK_VCode`),
  KEY `FK_karkerd_shift_idx` (`FK_Shift`),
  CONSTRAINT `FK_karkerd_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_karkerd_shift` FOREIGN KEY (`FK_Shift`) REFERENCES `tshiftkari` (`TShiftKariID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_karkerd_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tkarkerd VALUES("9812ج637","1347437410","1","0","4490271891","1482069224","");
INSERT INTO tkarkerd VALUES("9822ب111","1479287410","1","0","4490271891","1479298394","");



DROP TABLE IF EXISTS tmaliat;

CREATE TABLE `tmaliat` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Mtype` tinyint(4) NOT NULL,
  `Date_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Date_F` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mab_Pay` int(11) DEFAULT NULL,
  `Dastgah` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_VCode` int(11) NOT NULL,
  `ShFish` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Bank` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `DateFish` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Comment` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`FK_Pelak`,`Mtype`,`Date_S`,`Date_F`),
  KEY `FK_maliat_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_maliat_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_maliat_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tmaliat VALUES("9812ج637","1","1482311410","1514279410","100000","1","1","1","1","1482311410","1","4490271891","1483599957","1","4490271891","4490271891");



DROP TABLE IF EXISTS tmamoriat;

CREATE TABLE `tmamoriat` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Mshomare` varchar(25) COLLATE utf8_persian_ci NOT NULL,
  `CodeRanande` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `FK_CityCodeS` int(11) NOT NULL,
  `FK_CityCodeD` int(11) NOT NULL,
  `DateS` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `DateF` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `KiloS` int(11) NOT NULL,
  `KiloF` int(11) NOT NULL,
  `Msubject` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `Mdate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Hamrahan` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `MType` tinyint(4) DEFAULT NULL,
  `AddressMabda` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `AddressMaghsad` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`Mshomare`),
  KEY `FK_mamoriat_employee_idx` (`CodeRanande`),
  KEY `FK_mamoriat_vahed_idx` (`FK_VCode`),
  KEY `FK_mamoriat_Ccodes_idx` (`FK_CityCodeS`),
  KEY `FK_mamoriat_Ccoded_idx` (`FK_CityCodeD`),
  KEY `FK_mamoriat_car_idx` (`FK_Pelak`),
  CONSTRAINT `FK_mamoriat_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mamoriat_Ccoded` FOREIGN KEY (`FK_CityCodeD`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mamoriat_Ccodes` FOREIGN KEY (`FK_CityCodeS`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mamoriat_employee` FOREIGN KEY (`CodeRanande`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mamoriat_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tnecessarycar;

CREATE TABLE `tnecessarycar` (
  `TNecessaryCarID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_NecessaryType` int(11) NOT NULL,
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Date` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `DriverStatus` tinyint(4) DEFAULT NULL,
  `FK_TahvilID` int(11) DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`TNecessaryCarID`),
  KEY `FK_necessary_car_idx` (`FK_Pelak`),
  KEY `FK_necessary_necessaryType_idx` (`FK_NecessaryType`),
  KEY `FK_necessary_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_necessary_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_necessary_necessaryType` FOREIGN KEY (`FK_NecessaryType`) REFERENCES `tnecessarytype` (`TNecessaryTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_necessary_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tnecessarytype;

CREATE TABLE `tnecessarytype` (
  `TNecessaryTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `TNecessaryTypeName` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`TNecessaryTypeID`),
  KEY `FK_necessarytype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_necessarytype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tnecessarytype VALUES("1","آچار","1");
INSERT INTO tnecessarytype VALUES("2","انبردست","1");
INSERT INTO tnecessarytype VALUES("3","پیچ گوشتی","1");



DROP TABLE IF EXISTS tostan;

CREATE TABLE `tostan` (
  `Ocode` int(11) NOT NULL AUTO_INCREMENT,
  `Oname` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`Ocode`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tostan VALUES("1","آذربایجان شرقی");
INSERT INTO tostan VALUES("2","آذربایجان غربی");
INSERT INTO tostan VALUES("3","اردبیل");
INSERT INTO tostan VALUES("4","اصفهان");
INSERT INTO tostan VALUES("5","البرز");
INSERT INTO tostan VALUES("6","ایلام");
INSERT INTO tostan VALUES("7","بوشهر");
INSERT INTO tostan VALUES("8","تهران");
INSERT INTO tostan VALUES("9","خراسان جنوبی");
INSERT INTO tostan VALUES("10","خراسان رضوی");
INSERT INTO tostan VALUES("11","خراسان شمالی");
INSERT INTO tostan VALUES("12","خوزستان");
INSERT INTO tostan VALUES("13","زنجان");
INSERT INTO tostan VALUES("14","سمنان");
INSERT INTO tostan VALUES("15","سیستان و بلوچستان");
INSERT INTO tostan VALUES("16","فارس");
INSERT INTO tostan VALUES("17","قزوین");
INSERT INTO tostan VALUES("18","قم");
INSERT INTO tostan VALUES("19","لرستان");
INSERT INTO tostan VALUES("20","مازندران");
INSERT INTO tostan VALUES("21","مرکزی");
INSERT INTO tostan VALUES("22","هرمزگان");
INSERT INTO tostan VALUES("23","همدان");
INSERT INTO tostan VALUES("24","چهارمحال و بختیاری");
INSERT INTO tostan VALUES("25","کردستان");
INSERT INTO tostan VALUES("26","کرمان");
INSERT INTO tostan VALUES("27","کرمانشاه");
INSERT INTO tostan VALUES("28","کهگیلویه و بویراحمد");
INSERT INTO tostan VALUES("29","گلستان");
INSERT INTO tostan VALUES("30","گیلان");
INSERT INTO tostan VALUES("31","یزد");



DROP TABLE IF EXISTS treminders;

CREATE TABLE `treminders` (
  `RemindersID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_RemID` int(11) NOT NULL,
  `Date_A` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `Date_E` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_AdminCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_CommitCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_TahvilCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `SMS` tinyint(4) DEFAULT NULL,
  `Notification` tinyint(4) DEFAULT NULL,
  `Email` tinyint(4) DEFAULT NULL,
  `Login` tinyint(4) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `FK_BazID` int(11) DEFAULT NULL,
  `KilometerRem` int(11) DEFAULT NULL,
  `OkDate` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `Comment` text COLLATE utf8_persian_ci,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`RemindersID`),
  KEY `FK_reminders_car_idx` (`FK_Pelak`),
  KEY `FK_reminders_remType_idx` (`FK_RemID`),
  KEY `FK_reminderd_adminEmp_idx` (`FK_AdminCode`),
  KEY `FK_reminders_commitEmp_idx` (`FK_CommitCode`),
  KEY `FK_reminders_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_reminderd_adminEmp` FOREIGN KEY (`FK_AdminCode`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reminders_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reminders_commitEmp` FOREIGN KEY (`FK_CommitCode`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reminders_remType` FOREIGN KEY (`FK_RemID`) REFERENCES `tremindertype` (`RemID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reminders_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO treminders VALUES("1","9822ب111","6","1479633010","1479546610","4490271891","4490271891","4490271891","1","4490271891","1479298394","1","1","1","0","1","","","","","1");
INSERT INTO treminders VALUES("2","9812ج637","5","1504253410","1504167010","4490271891","4490271891","4490239068","1","4490271891","1482069224","0","0","0","0","1","","","","","1");
INSERT INTO treminders VALUES("3","9812ج637","6","1409731810","1409645410","4490271891","4490271891","4490239068","1","4490271891","1482069224","1","1","1","0","1","","","","","1");
INSERT INTO treminders VALUES("4","9822ب111","8","1484030410","1483944010","4490271891","4490271891","4490271891","1","4490271891","1482070724","1","1","1","0","1","","149999","","قطعه رينگ چرخ (زاپاس) - در تاریخ 1395/9/28 - و کیلومتر 13000 تعویض شده است","1");
INSERT INTO treminders VALUES("5","9812ج637","14","1514279410","1514193010","4490271891","4490271891","4490239068","1","4490271891","1483599943","0","0","0","0","1","","","","","1");



DROP TABLE IF EXISTS tremindertype;

CREATE TABLE `tremindertype` (
  `RemID` int(11) NOT NULL AUTO_INCREMENT,
  `RemName` varchar(300) COLLATE utf8_persian_ci NOT NULL,
  `RemType` tinyint(4) NOT NULL,
  `RemDate` varchar(10) COLLATE utf8_persian_ci DEFAULT NULL,
  `RemKilo` int(11) DEFAULT NULL,
  `SMS` tinyint(4) DEFAULT NULL,
  `Notification` tinyint(4) DEFAULT NULL,
  `Email` tinyint(4) DEFAULT NULL,
  `Login` tinyint(4) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Text` varchar(250) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`RemID`),
  KEY `FK_reminderstype_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_reminderstype_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tremindertype VALUES("1","انتقال (خروج)","1","1","1","1","1","1","1","1","مهلت پایان انتقال نزدیک است.","1");
INSERT INTO tremindertype VALUES("2","امانت (خروج)","1","1","1","1","1","1","1","1","مهلت پایان امانت نزدیک است.","1");
INSERT INTO tremindertype VALUES("3","تاریخ اعتبار گواهینامه","1","1","1","1","1","1","1","1","تاریخ اعتبار گواهینامه رو به اتمام است.","1");
INSERT INTO tremindertype VALUES("4","اجاره (ورود)","1","1","1","1","1","1","1","1","مهلت پایان اجاره نزدیک است.","1");
INSERT INTO tremindertype VALUES("5","امانت (ورود)","1","1","1","1","1","1","1","1","مهلت پایان امانت نزدیک است.","1");
INSERT INTO tremindertype VALUES("6","پایان گارانتی","1","1","1","1","1","1","1","1","مهلت پایان گارانتی نزدیک است.","1");
INSERT INTO tremindertype VALUES("7","حادثه","1","1","1","1","1","1","1","1","مهلت پیگیری حادثه نزدیک است.","1");
INSERT INTO tremindertype VALUES("8","پیگیری قطعه","1","1","1","1","1","1","1","1","قطعه با مشخصات زیر پیگیری شود:","1");
INSERT INTO tremindertype VALUES("9","بیمه نامه شخص ثالث","1","1","1","1","1","1","1","1","مهلت پایان بیمه نامه شخص ثالث نزدیک است.","1");
INSERT INTO tremindertype VALUES("10","بیمه نامه بدنه","1","1","1","1","1","1","1","1","مهلت پایان بیمه نامه بدنه نزدیک است.","1");
INSERT INTO tremindertype VALUES("11","بیمه نامه آتش سوزی","1","1","1","1","1","1","1","1","مهلت پایان بیمه نامه آتش سوزی نزدیک است.","1");
INSERT INTO tremindertype VALUES("12","بیمه نامه سرقت","1","1","1","1","1","1","1","1","مهلت پایان بیمه نامه سرقت نزدیک است.","1");
INSERT INTO tremindertype VALUES("13","تحویل خودرو","1","1","1","1","1","1","1","1","تحویل خودرو رو به اتمام است","1");
INSERT INTO tremindertype VALUES("14","مالیات","1","1","1","1","1","1","1","1","مهلت پرداخت مالیات نزدیک است.","1");
INSERT INTO tremindertype VALUES("15","آچارکشی پیچ و و مهره های بدنه و شاسی اتومبیل","1","1","1","1","1","1","1","1","آچارکشی پیچ و و مهره های بدنه و شاسی اتومبیل بررسی شود","1");
INSERT INTO tremindertype VALUES("16","بازدید اتصالات، لوله ها و شیلنگهای ترمز","1","1","1","1","1","1","1","1","اتصالات، لوله ها و شیلنگهای ترمز بررسی شود","1");
INSERT INTO tremindertype VALUES("17","بازدید پدال ترمز","1","1","1","1","1","1","1","1","پدال ترمز بررسی شود","1");
INSERT INTO tremindertype VALUES("18","بازدید پدال و کلاچ و رگلاژ سیم کلاچ","1","1","1","1","1","1","1","1","پدال و کلاچ و رگلاژ سیم کلاچ بررسی شود","1");
INSERT INTO tremindertype VALUES("19","بازدید دیسک ترمز و کاسه چرخ و لنت های جلو و کفشکهای عقب","1","1","1","1","1","1","1","1","دیسک ترمز و کاسه چرخ و لنت های جلو و کفشکهای عقب بررسی شود","1");
INSERT INTO tremindertype VALUES("20","بازدید روغن ترمز","1","1","1","1","1","1","1","1","روغن ترمز بررسی شود","1");
INSERT INTO tremindertype VALUES("21","بازدید روغن گیربکس اتوماتیک و در صورت نیاز تنظیم مقدار و یا تعویض آن","1","1","1","1","1","1","1","1","بازدید روغن گیربکس اتوماتیک و در صورت نیاز تنظیم مقدار و یا تعویض آن","1");
INSERT INTO tremindertype VALUES("22","بازدید زیر اتومبیل","1","1","1","1","1","1","1","1","بازدید زیر اتومبیل","1");
INSERT INTO tremindertype VALUES("23","بازدید سفتی مهره های چرخ","1","1","1","1","1","1","1","1","بازدید سفتی مهره های چرخ","1");
INSERT INTO tremindertype VALUES("24","بازدید سیستم خنک کننده شامل تنظیم مقدار مایع خنک کننده","1","1","1","1","1","1","1","1","بازدید سیستم خنک کننده شامل تنظیم مقدار مایع خنک کننده","1");
INSERT INTO tremindertype VALUES("25","بازدید سیستم فرمان و تعلیق جلو","1","1","1","1","1","1","1","1","بازدید سیستم فرمان و تعلیق جلو","1");
INSERT INTO tremindertype VALUES("26","بازدید عملکرد کمربندهای ایمنی","1","1","1","1","1","1","1","1","بازدید عملکرد کمربندهای ایمنی","1");
INSERT INTO tremindertype VALUES("27","بازدید فشار اویل پمپ","1","1","1","1","1","1","1","1","بازدید فشار اویل پمپ","1");
INSERT INTO tremindertype VALUES("28","بازدید کلیه گردگیر پلوسها","1","1","1","1","1","1","1","1","بازدید کلیه گردگیر پلوسها","1");
INSERT INTO tremindertype VALUES("29","بازدید لاستیکها شامل زاپاس و تنظیم میزان فشار باد","1","1","1","1","1","1","1","1","بازدید لاستیکها شامل زاپاس و تنظیم میزان فشار باد","1");
INSERT INTO tremindertype VALUES("30","بازدید لوله ها و شیلنگهای سوخت","1","1","1","1","1","1","1","1","بازدید لوله ها و شیلنگهای سوخت","1");
INSERT INTO tremindertype VALUES("31","بازدید مدارات الکتریکی شامل چراغها، برف پاک کن ها، بوق و ...","1","1","1","1","1","1","1","1","بازدید مدارات الکتریکی شامل چراغها، برف پاک کن ها، بوق و ...","1");
INSERT INTO tremindertype VALUES("32","بازدید و اسکازین گیربکس و در صورت نیاز تنظیم مقدار آن","1","1","1","1","1","1","1","1","بازدید و اسکازین گیربکس و در صورت نیاز تنظیم مقدار آن","1");
INSERT INTO tremindertype VALUES("33","بازدید و تنظیم شمع ها","1","1","1","1","1","1","1","1","بازدید و تنظیم شمع ها","1");
INSERT INTO tremindertype VALUES("34","بازدید وضعیت بوستر ترمز و شیلنگ ها","1","1","1","1","1","1","1","1","بازدید وضعیت بوستر ترمز و شیلنگ ها","1");
INSERT INTO tremindertype VALUES("35","تست جاده","1","1","1","1","1","1","1","1","تست جاده","1");
INSERT INTO tremindertype VALUES("36","تعویض روغن ترمز","1","1","1","1","1","1","1","1","تعویض روغن ترمز","1");
INSERT INTO tremindertype VALUES("38","تعویض فیلتر روغن","1","1","1","1","1","1","1","1","تعویض فیلتر روغن","1");
INSERT INTO tremindertype VALUES("39","تعویض فیلتر سوخت","1","1","1","1","1","1","1","1","تعویض فیلتر سوخت","1");
INSERT INTO tremindertype VALUES("40","تعویض و اسکازین گیربکس","1","1","1","1","1","1","1","1","تعویض و اسکازین گیربکس","1");
INSERT INTO tremindertype VALUES("41","تنظمی میزان آب باطری و غلظت آن","1","1","1","1","1","1","1","1","تنظمی میزان آب باطری و غلظت آن","1");
INSERT INTO tremindertype VALUES("42","تنظيم موتور مناسب با شرایط محیطی","1","1","1","1","1","1","1","1","تنظيم موتور مناسب با شرایط محیطی","1");
INSERT INTO tremindertype VALUES("43","تنظیم تایمینگ دلکو","1","1","1","1","1","1","1","1","تنظیم تایمینگ دلکو","1");
INSERT INTO tremindertype VALUES("44","تنظیم ترمز دستی","1","1","1","1","1","1","1","1","تنظیم ترمز دستی","1");
INSERT INTO tremindertype VALUES("45","تنظیم تسمه ها","1","1","1","1","1","1","1","1","تنظیم تسمه ها","1");
INSERT INTO tremindertype VALUES("46","تنظیم سوپاپها (فیلر گیری)","1","1","1","1","1","1","1","1","تنظیم سوپاپها (فیلر گیری)","1");
INSERT INTO tremindertype VALUES("47","تنظیم نور و چراغها جلو","1","1","1","1","1","1","1","1","تنظیم نور و چراغها جلو","1");
INSERT INTO tremindertype VALUES("48","رگلاژ درها","1","1","1","1","1","1","1","1","رگلاژ درها بررسی شود","1");
INSERT INTO tremindertype VALUES("49","سفت کردن پیچ و مهره های منیفولد","1","1","1","1","1","1","1","1","سفت کردن پیچ و مهره های منیفولد","1");
INSERT INTO tremindertype VALUES("50","شستشوی خودرو","1","1","1","1","1","1","1","1","شستشوی خودرو","1");
INSERT INTO tremindertype VALUES("51","سرویس اولیه","1","1","1","1","1","1","1","1","سرویس اولیه","1");
INSERT INTO tremindertype VALUES("52","سرویس 3000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 3000 کیلومتر","1");
INSERT INTO tremindertype VALUES("53","سرویس 5000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 5000 کیلومتر","1");
INSERT INTO tremindertype VALUES("54","سرویس 10000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 10000 کیلومتر","1");
INSERT INTO tremindertype VALUES("55","سرویس 15000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 15000 کیلومتر","1");
INSERT INTO tremindertype VALUES("56","سرویس 20000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 20000 کیلومتر","1");
INSERT INTO tremindertype VALUES("57","سرویس 30000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 30000 کیلومتر","1");
INSERT INTO tremindertype VALUES("58","سرویس 40000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 40000 کیلومتر","1");
INSERT INTO tremindertype VALUES("59","سرویس 50000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 50000 کیلومتر","1");
INSERT INTO tremindertype VALUES("60","سرویس 60000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 60000 کیلومتر","1");
INSERT INTO tremindertype VALUES("61","سرویس 70000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 70000 کیلومتر","1");
INSERT INTO tremindertype VALUES("62","سرویس 80000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 80000 کیلومتر","1");
INSERT INTO tremindertype VALUES("63","سرویس 90000 کیلومتر","1","1","1","1","1","1","1","1","سرویس 90000 کیلومتر","1");
INSERT INTO tremindertype VALUES("64","سرویس 100000کیلومتر","1","1","1","1","1","1","1","1","سرویس 100000کیلومتر","1");
INSERT INTO tremindertype VALUES("65","بازدید باد چرخها","1","1","1","1","1","1","1","1","بازدید باد چرخها","1");



DROP TABLE IF EXISTS trizbazresi;

CREATE TABLE `trizbazresi` (
  `RizBazresiID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Bazresi` int(11) NOT NULL,
  `FK_Segment` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `Comment` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `StatusRaf` tinyint(4) NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`RizBazresiID`),
  KEY `FK_rizbazresi_bazresi_idx` (`FK_Bazresi`),
  KEY `FK_rizbazresi_segment_idx` (`FK_Segment`),
  KEY `FK_rizbazresi_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_rizbazresi_bazresi` FOREIGN KEY (`FK_Bazresi`) REFERENCES `tbazresi` (`BazresiID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rizbazresi_segment` FOREIGN KEY (`FK_Segment`) REFERENCES `tseg` (`CodeS`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rizbazresi_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tseg;

CREATE TABLE `tseg` (
  `CodeS` int(11) NOT NULL AUTO_INCREMENT,
  `NameS` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `Country` varchar(30) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) DEFAULT NULL,
  `SegType` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`CodeS`),
  KEY `FK_seg_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_seg_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3734 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tseg VALUES("2536","ابر داخل درب","1","4490271891","1478929023","","2");
INSERT INTO tseg VALUES("2537","ابر صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2538","اتاق کامل","1","000","000","","2");
INSERT INTO tseg VALUES("2539","اتوماتيک استارت","1","000","000","","2");
INSERT INTO tseg VALUES("2540","اجزاي داخل دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2541","ارينگ","1","000","000","","2");
INSERT INTO tseg VALUES("2542","استارت","1","000","000","","2");
INSERT INTO tseg VALUES("2543","استکان تايپيت - ۱","1","000","000","","2");
INSERT INTO tseg VALUES("2544","اهرم ايمني قفل درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2545","اهرم بازوي دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2546","اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2547","اهرم تعويض دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2548","اهرم دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2549","اهرم كنترل دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2550","اهرم ميل ماهک دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2551","اهرم ميل ماهک دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2552","اورينگ","1","000","000","","2");
INSERT INTO tseg VALUES("2553","اورينگ درب سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("2554","اورينگ رادياتور بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("2555","اورينگ شلنگ كولر","1","000","000","","2");
INSERT INTO tseg VALUES("2556","آبريزتعريق كولر","1","000","000","","2");
INSERT INTO tseg VALUES("2557","آبگير شيشه درب جلو چپ بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("2558","آبگير شيشه درب جلو راست بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("2559","آبگيرشيشه درب جلوچپ داخل","1","000","000","","2");
INSERT INTO tseg VALUES("2560","آبگيرشيشه درب جلوراست داخل","1","000","000","","2");
INSERT INTO tseg VALUES("2561","آبگيرشيشه درب عقب چپ داخل","1","000","000","","2");
INSERT INTO tseg VALUES("2562","آبگيرشيشه درب عقب راست بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("2563","آبگيرشيشه درب عقب راست داخل","1","000","000","","2");
INSERT INTO tseg VALUES("2564","آرم جلو ","1","000","000","","2");
INSERT INTO tseg VALUES("2565","آرم عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2566","آرم ليفان ۱,۶درب صندوق","1","000","000","","2");
INSERT INTO tseg VALUES("2567","آرمچر دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2568","آرميچر استارت","1","000","000","","2");
INSERT INTO tseg VALUES("2569","آفتابگير جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("2570","آفتابگير چپ - کرم","1","000","000","","2");
INSERT INTO tseg VALUES("2571","آفتابگير راست","1","000","000","","2");
INSERT INTO tseg VALUES("2572","آفتابگير راست","1","000","000","","2");
INSERT INTO tseg VALUES("2573","آفتابگيرچپ-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("2574","آفتابگيرراست-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("2575","آهنربا داخل گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("2576","آينه داخل","1","000","000","","2");
INSERT INTO tseg VALUES("2577","بازوي اكسل عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2578","بازوي اکسل عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2579","بازوي موتور برف پاك كن","1","000","000","","2");
INSERT INTO tseg VALUES("2580","باک بنزين كامل","1","000","000","","2");
INSERT INTO tseg VALUES("2581","بالشتک دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2582","بخاري و کولر کامل","1","000","000","","2");
INSERT INTO tseg VALUES("2583","بخاري و کولر کامل ","1","000","000","","2");
INSERT INTO tseg VALUES("2584","بدون نام","1","000","000","","2");
INSERT INTO tseg VALUES("2585","براکت سوپاپ کنيستر","1","000","000","","2");
INSERT INTO tseg VALUES("2586","براکت صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2587","برنجي دنده ۱","1","000","000","","2");
INSERT INTO tseg VALUES("2588","برنجي دنده ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2589","برنجي دنده ۳","1","000","000","","2");
INSERT INTO tseg VALUES("2590","برنجي دنده ۴","1","000","000","","2");
INSERT INTO tseg VALUES("2591","برنجي دنده ۵","1","000","000","","2");
INSERT INTO tseg VALUES("2592","برنجي دنده پنج","1","000","000","","2");
INSERT INTO tseg VALUES("2593","بست","1","000","000","","2");
INSERT INTO tseg VALUES("2594","بست باك بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2595","بست پلاستيكي","1","000","000","","2");
INSERT INTO tseg VALUES("2596","بست تسمه اي پلاستيكي","1","000","000","","2");
INSERT INTO tseg VALUES("2597","بست زه بالاي درب هاي چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2598","بست زه بالاي درب هاي راست","1","000","000","","2");
INSERT INTO tseg VALUES("2599","بست ستون جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2600","بست شلنگ","1","000","000","","2");
INSERT INTO tseg VALUES("2601","بست شلنگ ترمز جلوچپ","1","000","000","","2");
INSERT INTO tseg VALUES("2602","بست شلنگ ترمز جلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("2603","بست شلنگ کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2604","بست شيشه درب جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2605","بست فلزي","1","000","000","","2");
INSERT INTO tseg VALUES("2606","بست فلزي لوله اگزوز جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2607","بست فلزي مخزن شيشه شور","1","000","000","","2");
INSERT INTO tseg VALUES("2608","بست قاب زيرشيشه جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2609","بست قفلي درب عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2610","بست کمربند ايمني عقب-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2611","بست گردگيرجعبه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2612","بست لوله بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2613","بلبرينگ ته دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2614","بلبرينگ سر دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2615","بلبرينگ سرشفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2616","بلبرينگ سركمك فنرجلو","1","000","000","","2");
INSERT INTO tseg VALUES("2617","بلبرينگ سفت کن کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2618","بلبرينگ سوزني دنده ۱ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2619","بلبرينگ سوزني دنده ۱شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2620","بلبرينگ سوزني دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("2621","بلبرينگ سوزني دنده ۵ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2622","بلبرينگ سوزني دنده پنج","1","000","000","","2");
INSERT INTO tseg VALUES("2623","بلبرينگ سوزني دنده سه و چهار","1","000","000","","2");
INSERT INTO tseg VALUES("2624","بلبرينگ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2625","بلبرينگ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2626","بلبرينگ كلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2627","بلندگوي جلو (باس)","1","000","000","","2");
INSERT INTO tseg VALUES("2628","بلندگوي جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2629","بوش","1","000","000","","2");
INSERT INTO tseg VALUES("2630","بوش بلبرينگ چرخ عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2631","بوش بين دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("2632","بوش پشت مثلثي آيينه","1","000","000","","2");
INSERT INTO tseg VALUES("2633","بوش پين پدال ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2634","بوش سيلندرترمزجلو","1","000","000","","2");
INSERT INTO tseg VALUES("2635","بوش شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2636","بوش شمع سرسرسيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("2637","بوش فلزي پايه تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2638","بوش فلزي پايه تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2639","بوش لاستيكي پايه مخزن پمپ ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2640","بوش لاستيک ميل تعادل جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2641","بوش مجموعه تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2642","بوش ميله درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2643","بوش هادي سيم ترمز دستي","1","000","000","","2");
INSERT INTO tseg VALUES("2644","بوق تن بالا","1","000","000","","2");
INSERT INTO tseg VALUES("2645","بوق تن پائين","1","000","000","","2");
INSERT INTO tseg VALUES("2646","پانل توي گلگير سمت چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2647","پايه اوابراتور","1","000","000","","2");
INSERT INTO tseg VALUES("2648","پايه بالائي رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("2649","پايه برگردان دسته راهنما","1","000","000","","2");
INSERT INTO tseg VALUES("2650","پايه بست منبع اگزوزعقب","1","000","000","","2");
INSERT INTO tseg VALUES("2651","پايه بوق","1","000","000","","2");
INSERT INTO tseg VALUES("2652","پايه پمپ ABS روي اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("2653","پايه پمپ روغن موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2654","پايه پمپ فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2655","پايه پوسته فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2656","پايه تنظيم دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2657","پايه جعبه فيوز","1","000","000","","2");
INSERT INTO tseg VALUES("2658","پايه چدني دود","1","000","000","","2");
INSERT INTO tseg VALUES("2659","پايه حسگردماوفشارهوا","1","000","000","","2");
INSERT INTO tseg VALUES("2660","پايه دسته","1","000","000","","2");
INSERT INTO tseg VALUES("2661","پايه دسته موتورراست","1","000","000","","2");
INSERT INTO tseg VALUES("2662","پايه سوپاپ کنترل بخارباک بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2663","پايه سوپاپ کنيستر","1","000","000","","2");
INSERT INTO tseg VALUES("2664","پايه سيلندرترمز جلوچپ","1","000","000","","2");
INSERT INTO tseg VALUES("2665","پايه سيلندرترمز جلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("2666","پايه سيم ترمزدستي چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2667","پايه سيم گاز","1","000","000","","2");
INSERT INTO tseg VALUES("2668","پايه سيم گاز","1","000","000","","2");
INSERT INTO tseg VALUES("2669","پايه شاسي زير موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2670","پايه شفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2671","پايه صندلي جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2672","پايه صندلي جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("2673","پايه ضربگيراکسل عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2674","پايه ضربگيراکسل عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2675","پايه فيلتر روغن","1","000","000","","2");
INSERT INTO tseg VALUES("2676","پايه قفل اهرم شفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2677","پايه كانال واسكازين","1","000","000","","2");
INSERT INTO tseg VALUES("2678","پايه كله قندي ضربگيرعقب","1","000","000","","2");
INSERT INTO tseg VALUES("2679","پايه كمپرسوركولر","1","000","000","","2");
INSERT INTO tseg VALUES("2680","پايه کناري باک","1","000","000","","2");
INSERT INTO tseg VALUES("2681","پايه کنسول وسط","1","000","000","","2");
INSERT INTO tseg VALUES("2682","پايه کوئيل","1","000","000","","2");
INSERT INTO tseg VALUES("2683","پايه گلگير جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2684","پايه لوله ترمزعقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2685","پايه لوله ترمزعقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2686","پايه منيفولد هوا","1","000","000","","2");
INSERT INTO tseg VALUES("2687","پايه نگه دارنده سيني بغل شاسي","1","000","000","","2");
INSERT INTO tseg VALUES("2688","پايه نگهدارنده دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2689","پايه و قفل درب باک","1","000","000","","2");
INSERT INTO tseg VALUES("2690","پبچ قلاب سرسيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("2691","پخش سي دي","1","000","000","","2");
INSERT INTO tseg VALUES("2692","پدال ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2693","پدال کلاچ کامل با پايه","1","000","000","","2");
INSERT INTO tseg VALUES("2694","پدال گاز باپايه","1","000","000","","2");
INSERT INTO tseg VALUES("2695","پروانه فن رادياتور آب","1","000","000","","2");
INSERT INTO tseg VALUES("2696","پروانه فن رادياتوركولر","1","000","000","","2");
INSERT INTO tseg VALUES("2697","پمپ بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2698","پمپ ترمز با مخزن روغن","1","000","000","","2");
INSERT INTO tseg VALUES("2699","پمپ روغن موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2700","پمپ شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("2701","پمپ کلاچ پائيني","1","000","000","","2");
INSERT INTO tseg VALUES("2702","پمپ هيدروليک فرمان - کامل","1","000","000","","2");
INSERT INTO tseg VALUES("2703","پنل کولر و بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("2704","پوسته انتهايي دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2705","پوسته بخاري وکولر","1","000","000","","2");
INSERT INTO tseg VALUES("2706","پوسته ترموستات","1","000","000","","2");
INSERT INTO tseg VALUES("2707","پوسته جاي کمک جلوچپ","1","000","000","","2");
INSERT INTO tseg VALUES("2708","پوسته داخل استارت","1","000","000","","2");
INSERT INTO tseg VALUES("2709","پوسته دنده گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("2710","پوسته ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("2711","پوسته دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2712","پوسته کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2713","پوشش پشت شاسي جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("2714","پوشش شاسي جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2715","پولک زيرفنرسوپاپ-۱۶عدد","1","000","000","","2");
INSERT INTO tseg VALUES("2716","پولک سر کمک جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2717","پولک سرفنرسوپاپ-۱۶عدد","1","000","000","","2");
INSERT INTO tseg VALUES("2718","پولي پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("2719","پولي ميل سوپاپ دود","1","000","000","","2");
INSERT INTO tseg VALUES("2720","پولي ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("2721","پيچ","1","000","000","","2");
INSERT INTO tseg VALUES("2722","پيچ اهرم بازوي دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2723","پيچ اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2724","پيچ آفتابگير","1","000","000","","2");
INSERT INTO tseg VALUES("2725","پيچ آلن","1","000","000","","2");
INSERT INTO tseg VALUES("2726","پيچ باواشرتخت قاب فن رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("2727","پيچ بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("2728","پيچ بست گلويي اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("2729","پيچ پايه دسته موتور جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2730","پيچ پايه دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2731","پيچ پايه رادياتوربالائي","1","000","000","","2");
INSERT INTO tseg VALUES("2732","پيچ پايه سگ دست","1","000","000","","2");
INSERT INTO tseg VALUES("2733","پيچ پايه سيم گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("2734","پيچ پايه ضربگير","1","000","000","","2");
INSERT INTO tseg VALUES("2735","پيچ پايه کانال واسکازين","1","000","000","","2");
INSERT INTO tseg VALUES("2736","پيچ پايه کمپرسور","1","000","000","","2");
INSERT INTO tseg VALUES("2737","پيچ پايه کمپرسور","1","000","000","","2");
INSERT INTO tseg VALUES("2738","پيچ پوسته گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("2739","پيچ پولي ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("2740","پيچ تخليه روغن گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("2741","پيچ تخليه کارتل","1","000","000","","2");
INSERT INTO tseg VALUES("2742","پيچ تخليه واسكازين","1","000","000","","2");
INSERT INTO tseg VALUES("2743","پيچ تکيه گاه دوشاخه کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2744","پيچ چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("2745","پيچ چهارشاخه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2746","پيچ چهارگوش","1","000","000","","2");
INSERT INTO tseg VALUES("2747","پيچ حسگروضعيت ميل سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("2748","پيچ خرچنگي گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("2749","پيچ خودکار","1","000","000","","2");
INSERT INTO tseg VALUES("2750","پيچ داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2751","پيچ درپوش تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2752","پيچ درپوش گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("2753","پيچ دسته موتور - راست","1","000","000","","2");
INSERT INTO tseg VALUES("2754","پيچ دو سر رزوه","1","000","000","","2");
INSERT INTO tseg VALUES("2755","پيچ دوسر رزوه درب سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("2756","پيچ دوسر سرسيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("2757","پيچ دوسررزوه چدني دود","1","000","000","","2");
INSERT INTO tseg VALUES("2758","پيچ دوسررزوه مقر ترموستات","1","000","000","","2");
INSERT INTO tseg VALUES("2759","پيچ ديسک چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("2760","پيچ ديسک چرخ جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2761","پيچ ديسک کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2762","پيچ رادياتور کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2763","پيچ رام زير موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2764","پيچ سگ دست","1","000","000","","2");
INSERT INTO tseg VALUES("2765","پيچ سگ دست به سيبك طبق","1","000","000","","2");
INSERT INTO tseg VALUES("2766","پيچ سوراخدار لوله روغن كلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2767","پيچ سيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("2768","پيچ سيلندرترمزعقب به سيني","1","000","000","","2");
INSERT INTO tseg VALUES("2769","پيچ سيم کشي","1","000","000","","2");
INSERT INTO tseg VALUES("2770","پيچ سيني جلو موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2771","پيچ شاسي زير موتور-بلند","1","000","000","","2");
INSERT INTO tseg VALUES("2772","پيچ شش گوش","1","000","000","","2");
INSERT INTO tseg VALUES("2773","پيچ شش گوش پايه کمربند ايمني","1","000","000","","2");
INSERT INTO tseg VALUES("2774","پيچ شش گوش سيستم تعليق جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2775","پيچ شش گوش لبه دار","1","000","000","","2");
INSERT INTO tseg VALUES("2776","پيچ شش گوش لبه دار دسته موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2777","پيچ شش گوش و نوار فنري واشر","1","000","000","","2");
INSERT INTO tseg VALUES("2778","پيچ شش گوش و واشر تخت","1","000","000","","2");
INSERT INTO tseg VALUES("2779","پيچ شش گوش و واشر فنري","1","000","000","","2");
INSERT INTO tseg VALUES("2780","پيچ شش گوش واشر فنري لبه دار","1","000","000","","2");
INSERT INTO tseg VALUES("2781","پيچ شش‌گوش","1","000","000","","2");
INSERT INTO tseg VALUES("2782","پيچ شفت دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2783","پيچ ضربگيراکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2784","پيچ فلاب کابل تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2785","پيچ فلايويل","1","000","000","","2");
INSERT INTO tseg VALUES("2786","پيچ قاب بالائي دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2787","پيچ قاب فن رادياتور کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2788","پيچ قربالک فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2789","پيچ قلاب روي گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("2790","پيچ كرانويل","1","000","000","","2");
INSERT INTO tseg VALUES("2791","پيچ کاسه چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("2792","پيچ کليد","1","000","000","","2");
INSERT INTO tseg VALUES("2793","پيچ کمپرسور کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2794","پيچ لبه دار شش گوش","1","000","000","","2");
INSERT INTO tseg VALUES("2795","پيچ لبه دار شش گوش","1","000","000","","2");
INSERT INTO tseg VALUES("2796","پيچ لولاي درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2797","پيچ لولاي درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2798","پيچ ماهک دنده ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2799","پيچ ماهک دنده ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2800","پيچ منبع اگزوز-جلوئي","1","000","000","","2");
INSERT INTO tseg VALUES("2801","پيچ نگهدارنده پمپ ABS","1","000","000","","2");
INSERT INTO tseg VALUES("2802","پيچ نگهدارنده دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2803","پيچ نگهدارنده سوپاپ گازكولر","1","000","000","","2");
INSERT INTO tseg VALUES("2804","پيچ هواگيري","1","000","000","","2");
INSERT INTO tseg VALUES("2805","پيچ هواگيري سيلندر ترمزچرخ جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2806","پيچ و واشرها","1","000","000","","2");
INSERT INTO tseg VALUES("2807","پيچ واشر دارپايه پوسته فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2808","پيستون ۰/۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("2809","پيستون استاندارد","1","000","000","","2");
INSERT INTO tseg VALUES("2810","پين ۵,۵ / ۲۶","1","000","000","","2");
INSERT INTO tseg VALUES("2811","پين اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2812","پين اهرم کنترل دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2813","پين پدال کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2814","پين ديسک کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2815","پين راهنماي نصب پمپ روغن","1","000","000","","2");
INSERT INTO tseg VALUES("2816","پين سيلندر ترمزجلو","1","000","000","","2");
INSERT INTO tseg VALUES("2817","پين شفت ديفرانسيال","1","000","000","","2");
INSERT INTO tseg VALUES("2818","پين شفت ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("2819","پين کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2820","پين لنت ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2821","پين ماهک ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2822","پين ماهک دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2823","پين ماهک دنده ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2824","پين مرکزي اهرم دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2825","پين هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("2826","تايپت","1","000","000","","2");
INSERT INTO tseg VALUES("2827","تايپيت موتور -۱۶عدد","1","000","000","","2");
INSERT INTO tseg VALUES("2828","ترموستات ايواپراتور","1","000","000","","2");
INSERT INTO tseg VALUES("2829","تسمه پمپ فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2830","تسمه تايم","1","000","000","","2");
INSERT INTO tseg VALUES("2831","تسمه دينام و پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("2832","تسمه کمپرسور کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2833","تسمه هيدروليک","1","000","000","","2");
INSERT INTO tseg VALUES("2834","تسمه هيدروليک فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("2835","تسمه وسطي باک بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2836","تعليقات شاسي","1","000","000","","2");
INSERT INTO tseg VALUES("2837","تقويت ترمز درب","1","000","000","","2");
INSERT INTO tseg VALUES("2838","تقويت درب صندوق سمت چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2839","تقويت گلگير جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2840","تقويت گلگير جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("2841","تنظيم کننده فن اوابراتور","1","000","000","","2");
INSERT INTO tseg VALUES("2842","توپي پلوس سمت چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("2843","توپي سر کمک جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2844","جاسيگاري عقب کنسول وسط","1","000","000","","2");
INSERT INTO tseg VALUES("2845","جاسيگاري كنسول جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2846","جاي فنر برگشت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2847","جاي فنر برگشت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2848","جعبه داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2849","جعبه داشبورد كامل-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("2850","جعبه داشبورد کامل - کرم","1","000","000","","2");
INSERT INTO tseg VALUES("2851","جعبه رله قفل مرکزي","1","000","000","","2");
INSERT INTO tseg VALUES("2852","جعبه فيوز پشت داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2853","جعبه فيوز داخل اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("2854","جعبه فيوززيرداشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2855","جک صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2856","جلو پنجره كامل با آرم","1","000","000","","2");
INSERT INTO tseg VALUES("2857","چراغ ترمز سوم - عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2858","چراغ ترمزسوم-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("2859","چراغ خطر عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2860","چراغ خطر عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2861","چراغ راهنماي روي آينه چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2862","چراغ راهنماي روي آينه راست","1","000","000","","2");
INSERT INTO tseg VALUES("2863","چراغ روي درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2864","چراغ روي درب جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("2865","چراغ روي درب صندوق چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2866","چراغ روي درب صندوق راست","1","000","000","","2");
INSERT INTO tseg VALUES("2867","چراغ روي درب عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2868","چراغ روي درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2869","چراغ سقف (جاعينکي)","1","000","000","","2");
INSERT INTO tseg VALUES("2870","چراغ سقف با جاعينکي","1","000","000","","2");
INSERT INTO tseg VALUES("2871","چراغ سقف جلو-کرم","1","000","000","","2");
INSERT INTO tseg VALUES("2872","چراغ صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2873","چراغ عقب روي صندوق چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2874","چراغ عقب روي صندوق راست","1","000","000","","2");
INSERT INTO tseg VALUES("2875","چراغ نمره عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2876","چرخ آلومينيومي","1","000","000","","2");
INSERT INTO tseg VALUES("2877","چشم الکترونيکي سپر عقب شماره ۱","1","000","000","","2");
INSERT INTO tseg VALUES("2878","چشم الکترونيکي سپر عقب شماره ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2879","چشم الکترونيکي سپر عقب شماره ۳","1","000","000","","2");
INSERT INTO tseg VALUES("2880","چشم الکترونيکي سپر عقب شماره ۴","1","000","000","","2");
INSERT INTO tseg VALUES("2881","چشم شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("2882","حرارتگير پائيني چدني دود","1","000","000","","2");
INSERT INTO tseg VALUES("2883","حرارتگيرچدني دود","1","000","000","","2");
INSERT INTO tseg VALUES("2884","حسگر فشار و دما","1","000","000","","2");
INSERT INTO tseg VALUES("2885","حسگر مخزن گاز کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2886","حسگر( فشار) روي مخزن گاز کولر","1","000","000","","2");
INSERT INTO tseg VALUES("2887","حسگردما ايواپراتور","1","000","000","","2");
INSERT INTO tseg VALUES("2888","خار","1","000","000","","2");
INSERT INTO tseg VALUES("2889","خار بالاي دنده کيلومتر","1","000","000","","2");
INSERT INTO tseg VALUES("2890","خار پلاستيکي","1","000","000","","2");
INSERT INTO tseg VALUES("2891","خار پلاستيکي اوابراتور","1","000","000","","2");
INSERT INTO tseg VALUES("2892","خار پلاستيکي عقب سقف- مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2893","خار پولي ميلنگ","1","000","000","","2");
INSERT INTO tseg VALUES("2894","خار جلو پنجره","1","000","000","","2");
INSERT INTO tseg VALUES("2895","خار خروجي شفت گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("2896","خار دنده ۵ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2897","خار دنده ۵ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2898","خار دو شاخه کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2899","خار دوپايه","1","000","000","","2");
INSERT INTO tseg VALUES("2900","خار رودري","1","000","000","","2");
INSERT INTO tseg VALUES("2901","خار رودري بلند","1","000","000","","2");
INSERT INTO tseg VALUES("2902","خار رينگي دنده ۴ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2903","خار سپر جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2904","خار سپر جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2905","خار سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("2906","خار شفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2907","خار صندلي","1","000","000","","2");
INSERT INTO tseg VALUES("2908","خار قفل درب کنسول وسط","1","000","000","","2");
INSERT INTO tseg VALUES("2909","خار لوله سيستم ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2910","خار موکت کف صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2911","خار ميل ماهک (۵ تا)","1","000","000","","2");
INSERT INTO tseg VALUES("2912","خار ميل ماهک (۵ تا)","1","000","000","","2");
INSERT INTO tseg VALUES("2913","خار ميله درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2914","خارآزاد کن پشتي صندلي عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2915","خارپلاستيكي عقب سقف خاكستري","1","000","000","","2");
INSERT INTO tseg VALUES("2916","خاردرب کنسول دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2917","خاردنده ميل سوپاپ دود","1","000","000","","2");
INSERT INTO tseg VALUES("2918","خاردنده ميل سوپاپ هوا","1","000","000","","2");
INSERT INTO tseg VALUES("2919","خاردنده۵شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2920","خاردوشاخه وبلبرينگ كلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("2921","خاررينکي بلبرينگ چرخ جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2922","خاررينگي دنده ۴ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2923","خاررينگي سگدست چرخ عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2924","خاررينگي كشوئي","1","000","000","","2");
INSERT INTO tseg VALUES("2925","خارزه داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2926","خارسيم دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2927","خارشفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("2928","خارشلنگ ترمز جلو و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2929","خارفلزي ستون وسط و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2930","خارفلزي سيستم بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("2931","خارلاستيك درب موتورعقبي","1","000","000","","2");
INSERT INTO tseg VALUES("2932","خارلاستيک دوردرب صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2933","خارلوله ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2934","خارلوله ترمز از جلو به عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2935","خارلوله ترمز دو لبه","1","000","000","","2");
INSERT INTO tseg VALUES("2936","خارموكت كف","1","000","000","","2");
INSERT INTO tseg VALUES("2937","خارميله درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2938","خارنمدزيردرب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2939","خرطومي فيلتر هوا","1","000","000","","2");
INSERT INTO tseg VALUES("2940","داشبورد کامل","1","000","000","","2");
INSERT INTO tseg VALUES("2941","درب باک- بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("2942","درب پيچي پمپ بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2943","درب پيچي شناور باک بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2944","درب پيچي شناورباک بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2945","درب جعبه فيوز داخل موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2946","درب جعبه فيوز زير داشبورد-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("2947","درب جعبه فيوز زير داشبورد-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2948","درب جلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("2949","درب رادياتور آب","1","000","000","","2");
INSERT INTO tseg VALUES("2952","درب روغن موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2953","درب صندوق عقب (مدل ۵۲۰I)","1","000","000","","2");
INSERT INTO tseg VALUES("2954","درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2955","درب عقب کنسول وسط","1","000","000","","2");
INSERT INTO tseg VALUES("2956","درب مخزن آب رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("2957","درب مخزن روغن ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("2958","درب مخزن روغن هيدروليک","1","000","000","","2");
INSERT INTO tseg VALUES("2959","درپوش","1","000","000","","2");
INSERT INTO tseg VALUES("2960","درپوش استارت","1","000","000","","2");
INSERT INTO tseg VALUES("2961","درپوش اکسل عقب-راست","1","000","000","","2");
INSERT INTO tseg VALUES("2962","درپوش انتهاي گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("2963","درپوش پايه کمربند عقب-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2964","درپوش پيچ قاب برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("2965","درپوش جاي كليدداشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2966","درپوش جاي كليدداشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("2967","درپوش دسته برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("2968","درپوش دينام","1","000","000","","2");
INSERT INTO tseg VALUES("2969","درپوش روي پمپ بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2970","درپوش روي شناور باک","1","000","000","","2");
INSERT INTO tseg VALUES("2971","درپوش طبق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2972","درجه باک بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("2973","دريچه ورودهوا به اتاق بافيلتر","1","000","000","","2");
INSERT INTO tseg VALUES("2974","دستگيره درب صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("2975","دستگيره درب عقب چپ بدون دکمه","1","000","000","","2");
INSERT INTO tseg VALUES("2976","دستگيره دربهاي چپ داخل-مشگي","1","000","000","","2");
INSERT INTO tseg VALUES("2977","دستگيره درهاي راست داخل - مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2978","دستگيره سقف","1","000","000","","2");
INSERT INTO tseg VALUES("2979","دسته برف پاک کن چپ","1","000","000","","2");
INSERT INTO tseg VALUES("2980","دسته برف پاک کن راست","1","000","000","","2");
INSERT INTO tseg VALUES("2981","دسته برف پاک کن و شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("2982","دسته برف پاک کن وشيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("2983","دسته دنده-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("2984","دسته دنده كامل","1","000","000","","2");
INSERT INTO tseg VALUES("2985","دسته راهنما و برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("2986","دسته راهنماوکليدچراغ جلو","1","000","000","","2");
INSERT INTO tseg VALUES("2987","دسته موتور","1","000","000","","2");
INSERT INTO tseg VALUES("2988","دسته موتور راست","1","000","000","","2");
INSERT INTO tseg VALUES("2989","دسته وتيغه برف پاک كن راست","1","000","000","","2");
INSERT INTO tseg VALUES("2990","دسته وتيغه برف پاک كن راست","1","000","000","","2");
INSERT INTO tseg VALUES("2991","دكمه قفل درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("2992","دکمه قفل کن درب ازداخل-مشگي","1","000","000","","2");
INSERT INTO tseg VALUES("2993","دنده ۱ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2994","دنده ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("2995","دنده ۳ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2996","دنده ۳ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2997","دنده ۴ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("2998","دنده ۴ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("2999","دنده ۴شفت خروجي ن ۹","1","000","000","","2");
INSERT INTO tseg VALUES("3000","دنده ۵","1","000","000","","2");
INSERT INTO tseg VALUES("3001","دنده ۵ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3002","دنده ۵ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3003","دنده ۵شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3004","دنده استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3005","دنده اکسل ديفرانسيل (ديشلي)","1","000","000","","2");
INSERT INTO tseg VALUES("3006","دنده اکسل ديفرانسيل (ديشلي)","1","000","000","","2");
INSERT INTO tseg VALUES("3007","دنده اي بي اس چرخ عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3008","دنده تنظيم تايم سرميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3009","دنده ديشلي","1","000","000","","2");
INSERT INTO tseg VALUES("3010","دنده سرعت استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3011","دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3012","دنده کيلومتر هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3013","دنده ميل سوپاپ دود-تايم","1","000","000","","2");
INSERT INTO tseg VALUES("3014","دنده ميل سوپاپ هواتنظيم دلکو","1","000","000","","2");
INSERT INTO tseg VALUES("3015","دنده هاي ديفرانسيل مجموعه","1","000","000","","2");
INSERT INTO tseg VALUES("3016","دنده هرزگرد","1","000","000","","2");
INSERT INTO tseg VALUES("3017","دنده واسطه عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3018","دو شاخه اتوماتيک استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3019","دو شاخه کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3020","دوبل داخل رکاب بغل چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3021","دوبل داخل رکاب بغل راست","1","000","000","","2");
INSERT INTO tseg VALUES("3022","دوبل ستون جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3023","دوبل ستون جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3024","دوبل سقف جلوي","1","000","000","","2");
INSERT INTO tseg VALUES("3025","دوبل سقف عقبي","1","000","000","","2");
INSERT INTO tseg VALUES("3026","دوبل شاسي جلو سمت راست","1","000","000","","2");
INSERT INTO tseg VALUES("3027","دوبل گلگيرجلوراست قسمت داخل","1","000","000","","2");
INSERT INTO tseg VALUES("3028","دياق سپر","1","000","000","","2");
INSERT INTO tseg VALUES("3029","دياق گلگير جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3030","دياق گلگير جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3031","ديسک کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3032","ديود دينام","1","000","000","","2");
INSERT INTO tseg VALUES("3033","ذغال استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3034","ذغال دينام","1","000","000","","2");
INSERT INTO tseg VALUES("3035","رادياتور آب","1","000","000","","2");
INSERT INTO tseg VALUES("3036","رادياتور بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("3037","رادياتور گاز کولر بدون مخزن","1","000","000","","2");
INSERT INTO tseg VALUES("3038","رادياتوربخاري","1","000","000","","2");
INSERT INTO tseg VALUES("3039","رادياتورگازکولربا مخزن گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3040","رام پشت سپر جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3041","رام زير رادياتور بالائي","1","000","000","","2");
INSERT INTO tseg VALUES("3042","رام زير رادياتور پائيني","1","000","000","","2");
INSERT INTO tseg VALUES("3043","راهنماي شيشه درب جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3044","رله","1","000","000","","2");
INSERT INTO tseg VALUES("3045","رله - ۴۰A","1","000","000","","2");
INSERT INTO tseg VALUES("3046","رله ۴۰ آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3047","رله برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("3048","رله بوق","1","000","000","","2");
INSERT INTO tseg VALUES("3049","رله چراغ چشمک زن","1","000","000","","2");
INSERT INTO tseg VALUES("3050","رله چراغ مه شکن","1","000","000","","2");
INSERT INTO tseg VALUES("3051","رله فن-دور بالا","1","000","000","","2");
INSERT INTO tseg VALUES("3052","رله فن-دور پايين","1","000","000","","2");
INSERT INTO tseg VALUES("3053","رله کمپرسورکولر","1","000","000","","2");
INSERT INTO tseg VALUES("3054","رله گرمکن شيشه","1","000","000","","2");
INSERT INTO tseg VALUES("3055","رله لامپها","1","000","000","","2");
INSERT INTO tseg VALUES("3056","رله مرکزي","1","000","000","","2");
INSERT INTO tseg VALUES("3057","رله نور بالا","1","000","000","","2");
INSERT INTO tseg VALUES("3058","رله نور پايين","1","000","000","","2");
INSERT INTO tseg VALUES("3059","رودري جلو چپ (مدل ۵۲۰I)","1","000","000","","2");
INSERT INTO tseg VALUES("3060","رودري جلو راست (مدل ۵۲۰I)","1","000","000","","2");
INSERT INTO tseg VALUES("3061","رودري درب عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3062","رودري درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3063","روکش اوابراتور","1","000","000","","2");
INSERT INTO tseg VALUES("3064","روکش کامپيوتر موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3065","رولبرينگ جلو هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3066","رولبرينگ ديفرانسيال-قديمي","1","000","000","","2");
INSERT INTO tseg VALUES("3067","رولبرينگ عقب هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3068","رولبرينگ هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3069","ريل سوخت پاش-سان تک با-اوبي دي","1","000","000","","2");
INSERT INTO tseg VALUES("3070","رينگ پيستون ۰,۲۵","1","000","000","","2");
INSERT INTO tseg VALUES("3071","رينگ پيستون ۰,۵۰- دست","1","000","000","","2");
INSERT INTO tseg VALUES("3072","رينگ پيستون ۰/۲۵-دست","1","000","000","","2");
INSERT INTO tseg VALUES("3073","رينگ پيستون ۰/۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("3074","رينگ پيستون استاندارد-دست","1","000","000","","2");
INSERT INTO tseg VALUES("3075","رينگ پيستون استاندارد-دست","1","000","000","","2");
INSERT INTO tseg VALUES("3076","رينگ چرخ (زاپاس)","1","000","000","","2");
INSERT INTO tseg VALUES("3077","زانويي ورودي بنزين به باک","1","000","000","","2");
INSERT INTO tseg VALUES("3078","زبانه قفل دربهاروي كلاف","1","000","000","","2");
INSERT INTO tseg VALUES("3079","زه بالاي دربهاي چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3080","زه داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3081","زه داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3082","زه درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3083","زه درب جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3084","زه روي درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3085","زه گلگير جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3086","زه گلگير جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3087","زه لاستيکي سقف-راست","1","000","000","","2");
INSERT INTO tseg VALUES("3088","ساچمه اهرم كنترل دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3089","ساچمه کنترل اهرم دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3090","ساچمه کنترل اهرم دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3091","سپر عقب (مدل ۵۲۰)","1","000","000","","2");
INSERT INTO tseg VALUES("3092","سپر عقب (مدل ۵۲۰i )","1","000","000","","2");
INSERT INTO tseg VALUES("3093","ستکان تايپيت - ۴۳","1","000","000","","2");
INSERT INTO tseg VALUES("3094","ستون بيروني جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3095","ستون بيروني جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3096","سر شاسي جلو-چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3097","سر شاسي جلو-راست","1","000","000","","2");
INSERT INTO tseg VALUES("3098","سردنده -مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3099","سگ دست جلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("3100","سنسور دريچه گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3101","سنسور دريچه گاز (ليفان ۵۲۰i)","1","000","000","","2");
INSERT INTO tseg VALUES("3102","سنسور وضعيت دريچه گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3103","سه راهي آب شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("3104","سوپاپ بخارروغن","1","000","000","","2");
INSERT INTO tseg VALUES("3105","سوپاپ دود ۸","1","000","000","","2");
INSERT INTO tseg VALUES("3106","سوپاپ لوله ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("3107","سوپاپ هوا ۸","1","000","000","","2");
INSERT INTO tseg VALUES("3108","سوپاپ هوا روي پوسته گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3109","سوئيچ فشار گاز کولر","1","000","000","","2");
INSERT INTO tseg VALUES("3110","سيبک زير طبق","1","000","000","","2");
INSERT INTO tseg VALUES("3111","سيلندرترمزجلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3112","سيلندرترمزجلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("3113","سيلندرترمزعقب","1","000","000","","2");
INSERT INTO tseg VALUES("3114","سيلندرموتور","1","000","000","","2");
INSERT INTO tseg VALUES("3115","سيم باز کن درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3116","سيم باز کن درب جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3117","سيم بازكن درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3118","سيم دسته دنده منيفولد پلاستيکي","1","000","000","","2");
INSERT INTO tseg VALUES("3119","سيم رابط قفل درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3120","سيم كشي جلوداشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3121","سيم كشي دينام","1","000","000","","2");
INSERT INTO tseg VALUES("3122","سيم کشي ايربگ","1","000","000","","2");
INSERT INTO tseg VALUES("3123","سيم کشي ايربگ (کيسه هوا)","1","000","000","","2");
INSERT INTO tseg VALUES("3124","سيم کشي آنتن بلند","1","000","000","","2");
INSERT INTO tseg VALUES("3125","سيم کشي آنتن کوتاه","1","000","000","","2");
INSERT INTO tseg VALUES("3126","سيم کشي بدنه چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3127","سيم کشي جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3128","سيم کشي چراغ","1","000","000","","2");
INSERT INTO tseg VALUES("3129","سيم کشي داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3130","سيم کشي درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3131","سيم کشي درب جلوچپ","1","000","000","","2");
INSERT INTO tseg VALUES("3132","سيم کشي دربهاي عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3133","سيم کشي سقف","1","000","000","","2");
INSERT INTO tseg VALUES("3134","سيم کشي سمت چپ بدنه","1","000","000","","2");
INSERT INTO tseg VALUES("3135","سيم کشي عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3136","سيم کشي عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3137","سيم کشي کف راست","1","000","000","","2");
INSERT INTO tseg VALUES("3138","سيم کشي لنت ترمز جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3139","سيم کشي موتور-دو سوکت","1","000","000","","2");
INSERT INTO tseg VALUES("3140","سيم کنترل دستي دکمه کولر -۱","1","000","000","","2");
INSERT INTO tseg VALUES("3141","سيم کنترل دستي دکمه کولر -۲","1","000","000","","2");
INSERT INTO tseg VALUES("3142","سيم کنترل دستي دکمه کولر -۳","1","000","000","","2");
INSERT INTO tseg VALUES("3143","سيم گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3144","سيم منفي باطري","1","000","000","","2");
INSERT INTO tseg VALUES("3145","سيم منفي گرمکن شيشه عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3146","سيني بغلي سر شاسي جلو-چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3147","سيني بغلي سر شاسي جلو-راست","1","000","000","","2");
INSERT INTO tseg VALUES("3148","سيني پشت پدال ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("3149","سيني پشت سپر عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3150","سيني پلاستيکي زير موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3151","سيني جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3152","سيني جلو اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("3153","سيني جلو اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("3154","سيني داخل درب سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("3155","سيني داخلي گلگير جلو - چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3156","سيني روي چرخ جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3157","سيني روي رادياتور چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3158","سيني روي رادياتور راست","1","000","000","","2");
INSERT INTO tseg VALUES("3159","سيني زير داشبورد-بالاي","1","000","000","","2");
INSERT INTO tseg VALUES("3160","سيني زير داشبورد-وسطي","1","000","000","","2");
INSERT INTO tseg VALUES("3161","سيني زير داشبورد -پاييني","1","000","000","","2");
INSERT INTO tseg VALUES("3162","سيني زير سپر عقب (پوسته رويي)","1","000","000","","2");
INSERT INTO tseg VALUES("3163","سيني شاسي جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3164","سيني شاسي جلو راست کامل","1","000","000","","2");
INSERT INTO tseg VALUES("3165","سيني عقب جاي زاپاس-کامل","1","000","000","","2");
INSERT INTO tseg VALUES("3166","سيني كفشكهاي ترمزعقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3167","سيني كفشكهاي ترمزعقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3168","شاتون - ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3169","شاسي جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3170","شاسي جلوچپ","1","000","000","","2");
INSERT INTO tseg VALUES("3171","شب نماي لاي درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3172","شب نماي لاي درب جلو راست-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3173","شب نماي لاي درب عقب جپ","1","000","000","","2");
INSERT INTO tseg VALUES("3174","شب نماي لاي درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3175","شبکه زير برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("3176","شفت استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3177","شفت اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3178","شفت اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3179","شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3180","شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3181","شفت خروجي گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3182","شفت دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3183","شفت دنده هرزگرد","1","000","000","","2");
INSERT INTO tseg VALUES("3184","شفت فرمان پائيني همراه قرقري","1","000","000","","2");
INSERT INTO tseg VALUES("3185","شفت ورود? گ?ربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3186","شفت ورودي گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3187","شلگير عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3188","شلنگ آب دريچه گاز خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3189","شلنگ آب دريچه گاز ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3190","شلنگ بخارازکنيستربه سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("3191","شلنگ بخاربنزين سوپاپ به موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3192","شلنگ بخارروغن به ورودي هواکش","1","000","000","","2");
INSERT INTO tseg VALUES("3193","شلنگ برگشت بنزين روي باک","1","000","000","","2");
INSERT INTO tseg VALUES("3194","شلنگ بنزين ازباک به صافي","1","000","000","","2");
INSERT INTO tseg VALUES("3195","شلنگ بنزين به صافي","1","000","000","","2");
INSERT INTO tseg VALUES("3196","شلنگ بنزين خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3197","شلنگ بنزين سرباك","1","000","000","","2");
INSERT INTO tseg VALUES("3198","شلنگ بوستر","1","000","000","","2");
INSERT INTO tseg VALUES("3199","شلنگ بين پمپ کلاج بالاوپائين","1","000","000","","2");
INSERT INTO tseg VALUES("3200","شلنگ تخليه سرريز بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("3201","شلنگ ترمز عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3202","شلنگ ترمزجلو","1","000","000","","2");
INSERT INTO tseg VALUES("3203","شلنگ تهويه گاز باك","1","000","000","","2");
INSERT INTO tseg VALUES("3204","شلنگ خرطومي هواکش","1","000","000","","2");
INSERT INTO tseg VALUES("3205","شلنگ خروجي مخزن گازکولر","1","000","000","","2");
INSERT INTO tseg VALUES("3206","شلنگ دوقلو گاز کولر","1","000","000","","2");
INSERT INTO tseg VALUES("3207","شلنگ روغن از مخزن به پمپ کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3208","شلنگ روغن كلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3209","شلنگ شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("3210","شلنگ ورودي پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("3211","شمع","1","000","000","","2");
INSERT INTO tseg VALUES("3212","شيشه آينه بغل چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3213","شيشه آينه بغل راست","1","000","000","","2");
INSERT INTO tseg VALUES("3214","شيشه بالابر جلو راست با موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3215","شيشه درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3216","صداگير بالايي داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3217","صداگير پاييني داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3218","صفحه اتصال دينام","1","000","000","","2");
INSERT INTO tseg VALUES("3219","صفحه بالايي متصل کننده","1","000","000","","2");
INSERT INTO tseg VALUES("3220","صفحه بالائي","1","000","000","","2");
INSERT INTO tseg VALUES("3221","صفحه بين گيربكس وموتور","1","000","000","","2");
INSERT INTO tseg VALUES("3222","صفحه پاييني متصل کننده","1","000","000","","2");
INSERT INTO tseg VALUES("3223","صفحه ترمز درب جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3224","صفحه چراغ عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3225","صفحه چراغ عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3226","صفحه خام فلزي کليد","1","000","000","","2");
INSERT INTO tseg VALUES("3227","صفحه رادياتور بخاري روي بدنه","1","000","000","","2");
INSERT INTO tseg VALUES("3228","صفحه روي فنر لول جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3229","صفحه فلزي زيردرب سوپاپ اولي","1","000","000","","2");
INSERT INTO tseg VALUES("3230","صفحه فلزي کلاف راست","1","000","000","","2");
INSERT INTO tseg VALUES("3231","صفحه کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3232","صفحه نگهدارنده زير باطري","1","000","000","","2");
INSERT INTO tseg VALUES("3233","ضامن اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3234","ضامن ايمني قفل درب عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3235","ضامن ايمني قفل درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3236","ضربگير بالاي کمک جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3237","ضربگير پشت سپر عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3238","ضربگير پشت سپر عقب اسفنجي","1","000","000","","2");
INSERT INTO tseg VALUES("3239","ضربگير سپر جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3240","ضربگير ميله درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3241","ضربگيرزيررادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("3242","ضربگيرسپرجلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3243","ضربگيرسپرجلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("3244","ضربگيرطبق عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3245","ضربگيرطبق عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3246","ضربگيرعقبي بازوي اكسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3247","طاقچه عقب فلزي","1","000","000","","2");
INSERT INTO tseg VALUES("3248","طبق جلو چپ بدون سيبک","1","000","000","","2");
INSERT INTO tseg VALUES("3249","طبق جلو راست بدون سيبک","1","000","000","","2");
INSERT INTO tseg VALUES("3250","عايق حرارتي انباره اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3251","عايق حرارتي مياني انباره اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3252","عايق سقف","1","000","000","","2");
INSERT INTO tseg VALUES("3253","عايق سيني داشبورد قسمت موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3254","عايق سيني زيرداشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3255","فرم پشت داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3256","فشنگي ترمز (استپ ترمز)","1","000","000","","2");
INSERT INTO tseg VALUES("3257","فشنگي ترمزدستي","1","000","000","","2");
INSERT INTO tseg VALUES("3258","فشنگي چراغ دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3259","فشنگي روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3260","فلايويل","1","000","000","","2");
INSERT INTO tseg VALUES("3261","فلايويل","1","000","000","","2");
INSERT INTO tseg VALUES("3262","فن رادياتور آب(۹ پره)","1","000","000","","2");
INSERT INTO tseg VALUES("3263","فن رادياتورآب کامل باقاب۵پره","1","000","000","","2");
INSERT INTO tseg VALUES("3264","فندک","1","000","000","","2");
INSERT INTO tseg VALUES("3265","فندک کامل","1","000","000","","2");
INSERT INTO tseg VALUES("3266","فنر","1","000","000","","2");
INSERT INTO tseg VALUES("3267","فنر اهرم دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3268","فنر اهرم دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3269","فنر اهرم کنترل دنده ۵ و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3270","فنر برگشت","1","000","000","","2");
INSERT INTO tseg VALUES("3271","فنر برگشت اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3272","فنر برگشت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3273","فنر برگشت مکانيزم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3274","فنر برگشت ميل ماهک","1","000","000","","2");
INSERT INTO tseg VALUES("3275","فنر برگشت ميل ماهک","1","000","000","","2");
INSERT INTO tseg VALUES("3276","فنر پشت لامپ","1","000","000","","2");
INSERT INTO tseg VALUES("3277","فنر رگلاژ ترمز عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3278","فنر رگلاژ ترمز عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3279","فنر لول جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3280","فنرپولي سفت کن تسمه تام","1","000","000","","2");
INSERT INTO tseg VALUES("3281","فنرسوپاپ-۱۶عدد","1","000","000","","2");
INSERT INTO tseg VALUES("3282","فولي دينام","1","000","000","","2");
INSERT INTO tseg VALUES("3283","فيبر جاي زاپاس","1","000","000","","2");
INSERT INTO tseg VALUES("3284","فيلتر بنزين","1","000","000","","2");
INSERT INTO tseg VALUES("3285","فيلتر روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3286","فيلتر هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3287","فيلتر هوا (۵۲۰ و ۵۲۰I)","1","000","000","","2");
INSERT INTO tseg VALUES("3288","فيلترهواي ورودي به اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("3289","فيوز","1","000","000","","2");
INSERT INTO tseg VALUES("3290","فيوز ۳۰ آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3291","فيوز روي کمپرسور کولر","1","000","000","","2");
INSERT INTO tseg VALUES("3292","فيوز۱۰آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3293","فيوز۱۵آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3294","فيوز۲۰آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3295","فيوز۲۵آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3296","فيوز۳۰آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3297","فيوز۵۰آمپر جديد تخت","1","000","000","","2");
INSERT INTO tseg VALUES("3298","فيوز۵۰آمپرقديمي","1","000","000","","2");
INSERT INTO tseg VALUES("3299","فيوز۵آمپر","1","000","000","","2");
INSERT INTO tseg VALUES("3300","فيوز۶۰آمپر جديد تخت","1","000","000","","2");
INSERT INTO tseg VALUES("3301","فيوز۶۰آمپر قديمي","1","000","000","","2");
INSERT INTO tseg VALUES("3302","قاب آمپرهابالائي مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3303","قاب آمپرهاپائيني تيره","1","000","000","","2");
INSERT INTO tseg VALUES("3304","قاب آمپرهاپائيني مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3305","قاب آينه بغل داخل چپ-تيره","1","000","000","","2");
INSERT INTO tseg VALUES("3306","قاب آينه بغل داخل راست-تيره","1","000","000","","2");
INSERT INTO tseg VALUES("3307","قاب آيينه بغل چپ داخل","1","000","000","","2");
INSERT INTO tseg VALUES("3308","قاب آيينه بغل راست داخل","1","000","000","","2");
INSERT INTO tseg VALUES("3309","قاب بالايي فيلتر هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3310","قاب بالائي پايه دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3311","قاب بالائي پايه دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3312","قاب بالائي پايه دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3313","قاب بالائي ستون عقب چپ-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3314","قاب بالائي ستون وسط چپ-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3315","قاب بالائي ستون وسط راست-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3316","قاب بوش ميل موج گيرجلو","1","000","000","","2");
INSERT INTO tseg VALUES("3317","قاب پلاک جلو ","1","000","000","","2");
INSERT INTO tseg VALUES("3318","قاب پلاک عقب ","1","000","000","","2");
INSERT INTO tseg VALUES("3319","قاب پيچ دسته برف پاك كن","1","000","000","","2");
INSERT INTO tseg VALUES("3320","قاب ترمزدستي-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3321","قاب تنظيم پشتي صندليهاي جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3322","قاب دكمه قفل كن دربها","1","000","000","","2");
INSERT INTO tseg VALUES("3323","قاب دوردکمه قفل کن درب از داخل","1","000","000","","2");
INSERT INTO tseg VALUES("3324","قاب راديو پخش","1","000","000","","2");
INSERT INTO tseg VALUES("3325","قاب روي راديو پخش","1","000","000","","2");
INSERT INTO tseg VALUES("3326","قاب روي راديوپخش كرم نقره اي","1","000","000","","2");
INSERT INTO tseg VALUES("3327","قاب روي كيسه هواراست-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3328","قاب روي كيسه هواراست-مشگي","1","000","000","","2");
INSERT INTO tseg VALUES("3329","قاب زير براکت سمت چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3330","قاب زير گلگير جلو چپ (شلگير)","1","000","000","","2");
INSERT INTO tseg VALUES("3331","قاب زير گلگير جلو راست (شلگير)","1","000","000","","2");
INSERT INTO tseg VALUES("3332","قاب زيري هواکش","1","000","000","","2");
INSERT INTO tseg VALUES("3333","قاب ستون جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3334","قاب ستون جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3335","قاب ستون جلوچپ-تيره","1","000","000","","2");
INSERT INTO tseg VALUES("3336","قاب ستون جلوچپ وسط","1","000","000","","2");
INSERT INTO tseg VALUES("3337","قاب ستون جلوراست-تيره","1","000","000","","2");
INSERT INTO tseg VALUES("3338","قاب ستون وسط بالائي چپ-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3339","قاب فرمان بالائي-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3340","قاب فرمان بالائي طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3341","قاب فرمان پائيني-مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3342","قاب فرمان پائيني طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3343","قاب فلزي طاقچه عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3344","قاب فن رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("3345","قاب كاسه نمدعقب ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3346","قاب کليد روي داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3347","قاب لولاي صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3348","قالپاق رينگ چرخ آلمينيومي","1","000","000","","2");
INSERT INTO tseg VALUES("3349","قالپاق گريس چرخ عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3350","قربالک فرمان - کرم","1","000","000","","2");
INSERT INTO tseg VALUES("3351","قربالک فرمان - مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3352","قسمت پايين سيني جلو اتاق","1","000","000","","2");
INSERT INTO tseg VALUES("3353","قسمت پلاستيکي بالا کليد","1","000","000","","2");
INSERT INTO tseg VALUES("3354","قسمت پلاستيکي پايين کليد","1","000","000","","2");
INSERT INTO tseg VALUES("3355","قفل درب عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3356","قفل درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3357","قفل صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3358","قفل كمربند سرنشين - مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3359","قفل كمربندراننده- كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3360","قفل كمربندراننده-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3361","قفل كمربندسرنشين-كرم","1","000","000","","2");
INSERT INTO tseg VALUES("3362","قفل کمربند راننده - مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3363","قفل کمربند عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3364","قفل کمربند عقب - وسط","1","000","000","","2");
INSERT INTO tseg VALUES("3365","قفل کمربند عقب - وسط","1","000","000","","2");
INSERT INTO tseg VALUES("3366","قلاب","1","000","000","","2");
INSERT INTO tseg VALUES("3367","قلاب سر سيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("3368","قلاب سرسيلندرچپ","1","000","000","","2");
INSERT INTO tseg VALUES("3369","قلاب قفل صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3370","قلاب کابل تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3371","قلاب کابل تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3372","كاسه ترمز عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3373","كاسه نمد بازوي دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3374","كانال روغن گيربكس","1","000","000","","2");
INSERT INTO tseg VALUES("3375","كانال مياني تهويه","1","000","000","","2");
INSERT INTO tseg VALUES("3376","كانال هواي قاب زيرشيشه جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3377","كليد لاي دربها - گرد","1","000","000","","2");
INSERT INTO tseg VALUES("3378","كليدبالابرشيشه روي درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3379","كمپرسوركولر","1","000","000","","2");
INSERT INTO tseg VALUES("3380","كيسه هواي راننده - کرم","1","000","000","","2");
INSERT INTO tseg VALUES("3381","کارتر روغن موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3382","کاسه نمد انتهاي ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3383","کاسه نمد بازوي دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3384","کاسه نمد پلوس","1","000","000","","2");
INSERT INTO tseg VALUES("3385","کاسه نمد پلوس ديفرانسيل سمت چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3386","کاسه نمد پلوس سمت چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3387","کاسه نمد دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3388","کاسه نمد ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3389","کاسه نمد ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3390","کاسه نمد سر ميل لنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3391","کاسه نمد شفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3392","کاسه نمد شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3393","کاسه نمد شفت ورودي گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3394","کاسه نمد ميل سوپاپ","1","000","000","","2");
INSERT INTO tseg VALUES("3395","کاسه نمدديفرانسيال-غيرچيني","1","000","000","","2");
INSERT INTO tseg VALUES("3396","کاسه نمدلوله اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3397","کامپيوتر کيسه هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3398","کانال هواي کولرزيرداشبورد-چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3399","کاور پنجره جلو - چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3400","کاور پنجره جلو - راست","1","000","000","","2");
INSERT INTO tseg VALUES("3401","کرانويل","1","000","000","","2");
INSERT INTO tseg VALUES("3402","کرانويل دنده خورشيدي","1","000","000","","2");
INSERT INTO tseg VALUES("3403","کرانويل دنده خورشيدي","1","000","000","","2");
INSERT INTO tseg VALUES("3404","کشو?? دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3405","کشويي دنده ۱ و ۲ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3406","کشويي دنده ۵","1","000","000","","2");
INSERT INTO tseg VALUES("3407","کشويي زير صندلي جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3408","کشوئي ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("3409","کشوئي دنده ۱ و ۲ شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3410","کشوئي دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3411","کشوئي دنده ۵","1","000","000","","2");
INSERT INTO tseg VALUES("3412","کشوئي دنده سه و چهار","1","000","000","","2");
INSERT INTO tseg VALUES("3413","کف اتاق قسمت جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3414","کلاف چپ با دوبل","1","000","000","","2");
INSERT INTO tseg VALUES("3415","کلاف راست با دوبل","1","000","000","","2");
INSERT INTO tseg VALUES("3416","کله قندي عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3417","کليد تنظيم آيينه بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("3418","کليد چراغ مه شکن عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3419","کليد چشمک زن","1","000","000","","2");
INSERT INTO tseg VALUES("3420","کليد خام","1","000","000","","2");
INSERT INTO tseg VALUES("3421","کليد شيشه بالابر جلو چپ کامل ","1","000","000","","2");
INSERT INTO tseg VALUES("3422","کليد شيشه بالابر عقب چپ ","1","000","000","","2");
INSERT INTO tseg VALUES("3423","کليد کولر و بخاري ","1","000","000","","2");
INSERT INTO tseg VALUES("3424","کليد گردان تهويه هوا ۱","1","000","000","","2");
INSERT INTO tseg VALUES("3425","کليد گردان تهويه هوا ۲","1","000","000","","2");
INSERT INTO tseg VALUES("3426","کليد گرمکن شيشه عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3427","کليدآينه برقي","1","000","000","","2");
INSERT INTO tseg VALUES("3428","کليدچراغ مه شکن جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3429","کليدچراغ مه شکن عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3430","کليدديمرو قفل مرکزي","1","000","000","","2");
INSERT INTO tseg VALUES("3431","کليدقطع بنزين-ايمني خودکار","1","000","000","","2");
INSERT INTO tseg VALUES("3432","کليدکنترل قفل مرکزي","1","000","000","","2");
INSERT INTO tseg VALUES("3433","کمربند ايمني عقب -مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3434","کمربند عقب - وسط","1","000","000","","2");
INSERT INTO tseg VALUES("3435","کيسه هواي راننده - مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3436","کيسه هواي سرنشين - کرم","1","000","000","","2");
INSERT INTO tseg VALUES("3437","گردگير پلوس","1","000","000","","2");
INSERT INTO tseg VALUES("3438","گردگير سر پلوس سمت چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("3439","گردگيربوش سيلندرترمزجلو","1","000","000","","2");
INSERT INTO tseg VALUES("3440","گردگيرپيچ هواگيري ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("3441","گردگيرجعبه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3442","گردگيردسته دنده-نقره اي بابست","1","000","000","","2");
INSERT INTO tseg VALUES("3443","گردگيردسته دنده - نقره اي","1","000","000","","2");
INSERT INTO tseg VALUES("3444","گردگيردسته دنده با قاب نقره اي","1","000","000","","2");
INSERT INTO tseg VALUES("3445","گردگيردوشاخه بلبرينگ كلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3446","گردگيرسر پلوس سمت ديفرانسيال","1","000","000","","2");
INSERT INTO tseg VALUES("3447","گردگيرسيبک تقويت کمک چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3448","گردگيرسيبک تقويت کمک راست","1","000","000","","2");
INSERT INTO tseg VALUES("3449","گردگيرکمک جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3450","گردگيرميل فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3451","گژن پين - ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3452","گل پخش كن عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3453","گلگير عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3454","گلگير عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3455","گلوئي ترموستات","1","000","000","","2");
INSERT INTO tseg VALUES("3456","گيج روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3457","گيربکس کامل (منيفولد فلزي)","1","000","000","","2");
INSERT INTO tseg VALUES("3458","گيره ميله رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("3459","لاستيك دوردرب جلو چپ مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3460","لاستيك دوردرب جلو راست مشکي","1","000","000","","2");
INSERT INTO tseg VALUES("3461","لاستيك دوردرب عقب چپ مشگي","1","000","000","","2");
INSERT INTO tseg VALUES("3462","لاستيك روي پدال ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("3463","لاستيك ساق سوپاپ هوا-۸عدد","1","000","000","","2");
INSERT INTO tseg VALUES("3464","لاستيك عقب درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3465","لاستيك لبه درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3466","لاستيک آبگيرسرکمک جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3467","لاستيک روي پدال گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3468","لاستيک شيشه لچكي ستون عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3469","لاستيک شيشه لچكي ستون عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3470","لامپ","1","000","000","","2");
INSERT INTO tseg VALUES("3471","لامپ H۱","1","000","000","","2");
INSERT INTO tseg VALUES("3472","لامپ چراغ جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3473","لامپ چراغ راهنما جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3474","لامپ چراغ راهنما۲۱وات","1","000","000","","2");
INSERT INTO tseg VALUES("3475","لامپ چراغ راهنماي جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3476","لامپ چراغ راهنماي عقب۲۱وات","1","000","000","","2");
INSERT INTO tseg VALUES("3477","لامپ چراغ سقف","1","000","000","","2");
INSERT INTO tseg VALUES("3478","لامپ چراغ سقف جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3479","لامپ چراغ سقف عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3480","لامپ چراغ صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3481","لامپ چراغ لاي درب","1","000","000","","2");
INSERT INTO tseg VALUES("3482","لامپ چراغ مه شكن جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3483","لامپ چراغ مه شكن عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3484","لامپ چراغ مه شکن","1","000","000","","2");
INSERT INTO tseg VALUES("3485","لامپ چراغ نمره عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3486","لامپ صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3487","لوازم تنظيم كفشكهاي ترمزعقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3488","لوگوي عقب ( ليفان)","1","000","000","","2");
INSERT INTO tseg VALUES("3489","لولاي درب جلو و عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3490","لولاي درب جلووعقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3491","لولاي درب جلووعقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3492","لولاي درب صندوق عقب چپ ","1","000","000","","2");
INSERT INTO tseg VALUES("3493","لولاي درب صندوق عقب راست ","1","000","000","","2");
INSERT INTO tseg VALUES("3494","لولاي درب موتور چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3495","لولاي درب موتور راست","1","000","000","","2");
INSERT INTO tseg VALUES("3496","لوله PVC","1","000","000","","2");
INSERT INTO tseg VALUES("3497","لوله بخار روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3498","لوله بخاربنزين به کنيستر","1","000","000","","2");
INSERT INTO tseg VALUES("3499","لوله بنزين ريل به سوخت پاش","1","000","000","","2");
INSERT INTO tseg VALUES("3500","لوله ترمز از پمپ به اي بي اس","1","000","000","","2");
INSERT INTO tseg VALUES("3501","لوله ترمز از پمپ به اي بي اس","1","000","000","","2");
INSERT INTO tseg VALUES("3502","لوله ترمز از پمپ به اي بي اس","1","000","000","","2");
INSERT INTO tseg VALUES("3503","لوله ترمز از جلو به عقب خودرو","1","000","000","","2");
INSERT INTO tseg VALUES("3504","لوله ترمز از جلو به عقب خودرو","1","000","000","","2");
INSERT INTO tseg VALUES("3505","لوله ترمز جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3506","لوله ترمز چرخ عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3507","لوله ترمز چرخ عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3508","لوله ترمزروي اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3509","لوله خروجي آب ازسرسيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("3510","لوله رابط فيلتر روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3511","لوله روغن هيدروليك چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3512","لوله روغن هيدروليك راست","1","000","000","","2");
INSERT INTO tseg VALUES("3513","لوله روغن هيدروليک جعبه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3514","لوله گاز کمپرسور - خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3515","لوله گاز کولر","1","000","000","","2");
INSERT INTO tseg VALUES("3516","لوله گازكولرازاواپراتور-دوقلو","1","000","000","","2");
INSERT INTO tseg VALUES("3517","لوله گلويي اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3518","لوله گيج روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3519","لوله ورودي پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("3520","لوله ورودي پمپ آب آلمينيومي","1","000","000","","2");
INSERT INTO tseg VALUES("3521","ليور دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3522","ليور دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3523","ماهك دنده يك و دو","1","000","000","","2");
INSERT INTO tseg VALUES("3524","ماهک دنده ۱ و ۲","1","000","000","","2");
INSERT INTO tseg VALUES("3525","ماهک دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3526","ماهک دنده ۵","1","000","000","","2");
INSERT INTO tseg VALUES("3527","ماهک شفت دنده ۳ و ۴","1","000","000","","2");
INSERT INTO tseg VALUES("3528","ماهوتي شيشه درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3529","مپ سنسور","1","000","000","","2");
INSERT INTO tseg VALUES("3530","متعلقات اتصال کف","1","000","000","","2");
INSERT INTO tseg VALUES("3531","متعلقات زير شاسي - چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3532","متعلقات سيم کشي","1","000","000","","2");
INSERT INTO tseg VALUES("3533","متعلقات سيني جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3534","متعلقات شاسي","1","000","000","","2");
INSERT INTO tseg VALUES("3535","متعلقات طاقچه عقب فلزي","1","000","000","","2");
INSERT INTO tseg VALUES("3536","متعلقات کنسول وسط","1","000","000","","2");
INSERT INTO tseg VALUES("3537","متعليقات زير شاسي - راست","1","000","000","","2");
INSERT INTO tseg VALUES("3538","مجموعه پدال ترمزباسيني","1","000","000","","2");
INSERT INTO tseg VALUES("3539","مجموعه پوسته استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3540","مجموعه ترمز کامل چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3541","مجموعه کامل مخزن روغن فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3542","مجموعه موتور اوابراتور","1","000","000","","2");
INSERT INTO tseg VALUES("3543","محافظ اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3544","محور دنده هرزگرد","1","000","000","","2");
INSERT INTO tseg VALUES("3545","مخزن آب شيشه شوي باپمپ","1","000","000","","2");
INSERT INTO tseg VALUES("3546","مخزن روغن ترمزبادرب","1","000","000","","2");
INSERT INTO tseg VALUES("3547","مخزن گاز كولر","1","000","000","","2");
INSERT INTO tseg VALUES("3548","مغزي فندک","1","000","000","","2");
INSERT INTO tseg VALUES("3549","مقاومت فن بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("3550","مقاومت فن رادياتورآب","1","000","000","","2");
INSERT INTO tseg VALUES("3551","منبع اگزوز جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3552","منبع اگزوز عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3553","منجيد اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3554","منجيد منبع عقبي اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3555","منيفولد اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3556","منيفولد پلاستيکي (بنزين)","1","000","000","","2");
INSERT INTO tseg VALUES("3557","منيفولد هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3558","منيفولد هوا-بدنه بالائي","1","000","000","","2");
INSERT INTO tseg VALUES("3559","مهره","1","000","000","","2");
INSERT INTO tseg VALUES("3560","مهره اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3561","مهره اهرم تعويض دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3562","مهره بست باطري","1","000","000","","2");
INSERT INTO tseg VALUES("3563","مهره بست پوسته فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3564","مهره پايه دينام-۱۰/۱,۲۵","1","000","000","","2");
INSERT INTO tseg VALUES("3565","مهره پلاستيکي قاب زيرشيشه","1","000","000","","2");
INSERT INTO tseg VALUES("3566","مهره پلاستيکي قاب ستون جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3567","مهره پلاستيکي کنسول دست دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3568","مهره پيچ دسته موتور عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3569","مهره پيچ سيبک طبق جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3570","مهره پيچ سيبک طبق جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3571","مهره چهارگوش جعبه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3572","مهره درب سوپاپ-۶مم","1","000","000","","2");
INSERT INTO tseg VALUES("3573","مهره دسته موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3574","مهره زاپاس بندچرخ آلومينيومي","1","000","000","","2");
INSERT INTO tseg VALUES("3575","مهره سر پلوس","1","000","000","","2");
INSERT INTO tseg VALUES("3576","مهره سگ دست چرخ عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3577","مهره شاسي زير موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3578","مهره شش گوش با واشر","1","000","000","","2");
INSERT INTO tseg VALUES("3579","مهره شش گوش لبه دار","1","000","000","","2");
INSERT INTO tseg VALUES("3580","مهره فنري","1","000","000","","2");
INSERT INTO tseg VALUES("3581","مهره قفلي","1","000","000","","2");
INSERT INTO tseg VALUES("3582","مهره قفلي ۲۲/۱,۵","1","000","000","","2");
INSERT INTO tseg VALUES("3583","موتور برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("3584","موتور تغيير وضعيت دريچه هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3585","موتور فن رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("3586","موتور کامل (منيفولد پلاستيکي)","1","000","000","","2");
INSERT INTO tseg VALUES("3587","موتور کامل (منيفولد فلزي)","1","000","000","","2");
INSERT INTO tseg VALUES("3588","موتورتنظيم دريچه کولر و بخاري","1","000","000","","2");
INSERT INTO tseg VALUES("3589","موتورفن رادياتورآب","1","000","000","","2");
INSERT INTO tseg VALUES("3590","موتورفن رادياتورکولر","1","000","000","","2");
INSERT INTO tseg VALUES("3591","موکت درب صندوق","1","000","000","","2");
INSERT INTO tseg VALUES("3592","موکت روي سيني چرخ عقب-راست","1","000","000","","2");
INSERT INTO tseg VALUES("3593","موکت روي سيني چرخ عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3594","موکت کف صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3595","ميل چرخشي جلوئي اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3596","ميل چرخشي عقبي اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3597","ميل سوپاپ دود","1","000","000","","2");
INSERT INTO tseg VALUES("3598","ميل سوپاپ هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3599","ميل فرمان (چپ و راست)","1","000","000","","2");
INSERT INTO tseg VALUES("3600","ميل قفل درب جلو راست-افقي","1","000","000","","2");
INSERT INTO tseg VALUES("3601","ميل لنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3602","ميل ماهك دنده يك و دو","1","000","000","","2");
INSERT INTO tseg VALUES("3603","ميل ماهک بالايي","1","000","000","","2");
INSERT INTO tseg VALUES("3604","ميل موجگير جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3605","ميل موجگيراکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3606","ميله اکسل عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3607","ميله درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3608","ميله دستگيره بيروني درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3609","ميله قفل ايمني داخل درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3610","نگهدارنده باطري","1","000","000","","2");
INSERT INTO tseg VALUES("3611","نگهدارنده پوسته گلگير جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3612","نگهدارنده پوسته گلگير جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3613","نگهدارنده دو شاخه کلاچ","1","000","000","","2");
INSERT INTO tseg VALUES("3614","نگهدارنده زاپاس","1","000","000","","2");
INSERT INTO tseg VALUES("3615","نگهدارنده شبکه صندوق - راست","1","000","000","","2");
INSERT INTO tseg VALUES("3616","نگهدارنده لوله ترمز","1","000","000","","2");
INSERT INTO tseg VALUES("3617","نگهدارنده لوله جعبه فرمان","1","000","000","","2");
INSERT INTO tseg VALUES("3618","نگهدارنده ميل ماهک","1","000","000","","2");
INSERT INTO tseg VALUES("3619","نگهدارنده ميل ماهک","1","000","000","","2");
INSERT INTO tseg VALUES("3620","نمد زير درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3621","نمد کف اتاق - جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3622","نمد کف اتاق - عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3623","نمد کف اطاق","1","000","000","","2");
INSERT INTO tseg VALUES("3624","نوار دور درب عقب چپ-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3625","نوار دور درب عقب راست-طوسي","1","000","000","","2");
INSERT INTO tseg VALUES("3626","نوار عمودي درب جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3627","نوار عمودي درب جلو راست","1","000","000","","2");
INSERT INTO tseg VALUES("3628","نوار عمودي درب عقب چپ-جلوي","1","000","000","","2");
INSERT INTO tseg VALUES("3629","نوار عمودي درب عقب چپ-عقبي","1","000","000","","2");
INSERT INTO tseg VALUES("3630","نوار عمودي درب عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3631","نوار عمودي درب عقب راست-عقبي","1","000","000","","2");
INSERT INTO tseg VALUES("3632","نوار لاستيکي عمودي ستون چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3633","نوار متصل کننده سقف","1","000","000","","2");
INSERT INTO tseg VALUES("3634","نواردور درب","1","000","000","","2");
INSERT INTO tseg VALUES("3635","نواردور درب","1","000","000","","2");
INSERT INTO tseg VALUES("3636","نوارقاب برف پاک کن","1","000","000","","2");
INSERT INTO tseg VALUES("3637","نوشته ۵۲۰I","1","000","000","","2");
INSERT INTO tseg VALUES("3638","هسته استارت","1","000","000","","2");
INSERT INTO tseg VALUES("3639","هواکش کامل","1","000","000","","2");
INSERT INTO tseg VALUES("3640","واشر","1","000","000","","2");
INSERT INTO tseg VALUES("3641","واشر اهرم دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3642","واشر آبندي داخل گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3643","واشر بالابر درب موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3644","واشر بزرگ","1","000","000","","2");
INSERT INTO tseg VALUES("3645","واشر پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("3646","واشر پمپ آب","1","000","000","","2");
INSERT INTO tseg VALUES("3647","واشر پمپ کلاچ بالايي","1","000","000","","2");
INSERT INTO tseg VALUES("3648","واشر پيچ ستون داشبورد","1","000","000","","2");
INSERT INTO tseg VALUES("3649","واشر تخت","1","000","000","","2");
INSERT INTO tseg VALUES("3650","واشر تخت","1","000","000","","2");
INSERT INTO tseg VALUES("3651","واشر تنظيم بلبرينگ گيربکس ۰,۱","1","000","000","","2");
INSERT INTO tseg VALUES("3652","واشر دنده ديشلي","1","000","000","","2");
INSERT INTO tseg VALUES("3653","واشر دنده عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3654","واشر دنده هرزگرد","1","000","000","","2");
INSERT INTO tseg VALUES("3655","واشر رام موتور","1","000","000","","2");
INSERT INTO tseg VALUES("3656","واشر سوپاپ تهويه گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3657","واشر سوپاپ خروج هوا","1","000","000","","2");
INSERT INTO tseg VALUES("3658","واشر شفت خروجي گيربکس ۱,۸","1","000","000","","2");
INSERT INTO tseg VALUES("3659","واشر شفت دسته دنده","1","000","000","","2");
INSERT INTO tseg VALUES("3660","واشر فلزي دنده ۴ شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3661","واشر فن رادياتور","1","000","000","","2");
INSERT INTO tseg VALUES("3662","واشر فنري","1","000","000","","2");
INSERT INTO tseg VALUES("3663","واشر قفل هوزينگ ديفرانسيل","1","000","000","","2");
INSERT INTO tseg VALUES("3664","واشر قفلي خورشيدي","1","000","000","","2");
INSERT INTO tseg VALUES("3665","واشر گيربکس","1","000","000","","2");
INSERT INTO tseg VALUES("3666","واشر لاستيکي","1","000","000","","2");
INSERT INTO tseg VALUES("3667","واشر لاستيکي چرخ","1","000","000","","2");
INSERT INTO tseg VALUES("3668","واشر مسي پيچ تخليه کارتل","1","000","000","","2");
INSERT INTO tseg VALUES("3669","واشر منبع اگزوز وسط و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3670","واشر منيفولد دود","1","000","000","","2");
INSERT INTO tseg VALUES("3671","واشر منيفولد ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3672","واشرپايه مخزن آب شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("3673","واشرپشت پولي ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3674","واشرپلاستيكي","1","000","000","","2");
INSERT INTO tseg VALUES("3675","واشرپمپ روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3676","واشرپمپ شيشه شوي","1","000","000","","2");
INSERT INTO tseg VALUES("3677","واشرپيچ سرسيلندر","1","000","000","","2");
INSERT INTO tseg VALUES("3678","واشرپيچ نگهدارنده باک","1","000","000","","2");
INSERT INTO tseg VALUES("3679","واشرتخت","1","000","000","","2");
INSERT INTO tseg VALUES("3680","واشرتخت","1","000","000","","2");
INSERT INTO tseg VALUES("3681","واشرتخت-۴گوش","1","000","000","","2");
INSERT INTO tseg VALUES("3682","واشرتنظيم ۰۱۰شفت خروجي","1","000","000","","2");
INSERT INTO tseg VALUES("3683","واشرجلوئي پولي ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3684","واشرچراغ مه شكن عقب چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3685","واشرچراغ مه شكن عقب راست","1","000","000","","2");
INSERT INTO tseg VALUES("3686","واشرچراغ مه شکن جلو چپ","1","000","000","","2");
INSERT INTO tseg VALUES("3687","واشرچراغ مه شکن جلوراست","1","000","000","","2");
INSERT INTO tseg VALUES("3688","واشرحسگر وضعيت ميللنگ","1","000","000","","2");
INSERT INTO tseg VALUES("3689","واشرخاري","1","000","000","","2");
INSERT INTO tseg VALUES("3690","واشردريچه گاز","1","000","000","","2");
INSERT INTO tseg VALUES("3691","واشردنده ديشلي","1","000","000","","2");
INSERT INTO tseg VALUES("3692","واشردنده ديشلي","1","000","000","","2");
INSERT INTO tseg VALUES("3693","واشرصافي پمپ روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3694","واشرفلزي دنده۴شفت ورودي","1","000","000","","2");
INSERT INTO tseg VALUES("3695","واشرفنري سيبک طبق جلو","1","000","000","","2");
INSERT INTO tseg VALUES("3696","واشرفنري ميل سوپاپ دود","1","000","000","","2");
INSERT INTO tseg VALUES("3697","واشرگلويي اگزوز","1","000","000","","2");
INSERT INTO tseg VALUES("3698","واشرگيج روغن","1","000","000","","2");
INSERT INTO tseg VALUES("3699","واشرمنبع اگزوز وسط و عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3700","واشرمهره سرکمک","1","000","000","","2");
INSERT INTO tseg VALUES("3701","والو باد (والف)","1","000","000","","2");
INSERT INTO tseg VALUES("3702","واير شمع کامل","1","000","000","","2");
INSERT INTO tseg VALUES("3703","ورشوئي درب صندوق عقب","1","000","000","","2");
INSERT INTO tseg VALUES("3704","ياتاقان بازوي اکسل عقب بيروني","1","000","000","","2");
INSERT INTO tseg VALUES("3705","ياتاقان بازوي اکسل عقب داخلي","1","000","000","","2");
INSERT INTO tseg VALUES("3706","ياتاقان ثابت بالائ?- سايز ۲#","1","000","000","","2");
INSERT INTO tseg VALUES("3707","ياتاقان ثابت بالائ?- سايز ۳#","1","000","000","","2");
INSERT INTO tseg VALUES("3708","ياتاقان ثابت بالائ?- سايز ۴#","1","000","000","","2");
INSERT INTO tseg VALUES("3709","ياتاقان ثابت بالائ?- سايز ۵#","1","000","000","","2");
INSERT INTO tseg VALUES("3710","ياتاقان ثابت بالائي- سايز ۱#","1","000","000","","2");
INSERT INTO tseg VALUES("3711","ياتاقان ثابت بالائي ۰,۲۵","1","000","000","","2");
INSERT INTO tseg VALUES("3712","ياتاقان ثابت بالائي ۰۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("3713","ياتاقان ثابت پاييني- سايز ۱#","1","000","000","","2");
INSERT INTO tseg VALUES("3714","ياتاقان ثابت پاييني- سايز ۲#","1","000","000","","2");
INSERT INTO tseg VALUES("3715","ياتاقان ثابت پاييني- سايز ۳#","1","000","000","","2");
INSERT INTO tseg VALUES("3716","ياتاقان ثابت پاييني- سايز ۴#","1","000","000","","2");
INSERT INTO tseg VALUES("3717","ياتاقان ثابت پاييني- سايز ۵#","1","000","000","","2");
INSERT INTO tseg VALUES("3718","ياتاقان ثابت پاييني سا?ز ۰,۲۵","1","000","000","","2");
INSERT INTO tseg VALUES("3719","ياتاقان ثابت پاييني سا?ز ۰,۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("3720","ياتاقان متحرک ۰,۲۵ - ۸عدد","1","000","000","","2");
INSERT INTO tseg VALUES("3721","ياتاقان متحرک ۰/۲۵ - ۸ عدد","1","000","000","","2");
INSERT INTO tseg VALUES("3722","ياتاقان متحرک ۰/۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("3723","ياتاقان متحرک ۰/۵۰ - ۸ عدد","1","000","000","","2");
INSERT INTO tseg VALUES("3724","ياتاقان متحرک سايز ۰,۲۵","1","000","000","","2");
INSERT INTO tseg VALUES("3725","ياتاقان متحرک سايز ۰,۵۰","1","000","000","","2");
INSERT INTO tseg VALUES("3726","حق الزحمه تعمیرکار","1","4490271891","1483458364","1","3");
INSERT INTO tseg VALUES("3727","لاستیک","1","admin","1483164084","1","1");
INSERT INTO tseg VALUES("3728","لنت ترمز","1","admin","1483164047","1","1");
INSERT INTO tseg VALUES("3729","روغن موتور","1","admin","1483164114","1","1");
INSERT INTO tseg VALUES("3730","روغن ترمز","1","admin","1483164134","1","1");
INSERT INTO tseg VALUES("3731","روغن گیربکس","1","admin","1483164149","1","1");
INSERT INTO tseg VALUES("3732","صافی بنزین","1","admin","1483164168","1","1");
INSERT INTO tseg VALUES("3733","باطری","1","admin","1483164074","1","1");



DROP TABLE IF EXISTS tserghat;

CREATE TABLE `tserghat` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `SerghatDateS` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `DatePayda` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_CityCode` int(11) DEFAULT NULL,
  `DateReminder` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`FK_Pelak`,`SerghatDateS`),
  KEY `FK_tserghat_vahed_idx` (`FK_VCode`),
  KEY `FK_tserghat_city_idx` (`FK_CityCode`),
  CONSTRAINT `FK_tserghat_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tserghat_city` FOREIGN KEY (`FK_CityCode`) REFERENCES `tcity` (`Ccode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tserghat_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS tservices;

CREATE TABLE `tservices` (
  `ServiceCode` int(11) NOT NULL AUTO_INCREMENT,
  `FK_ACode` int(11) NOT NULL,
  `S_Date` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ranande` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Maghsad` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `Mabda` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_CodeHamkar` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Hamrahan` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `S_Time` varchar(10) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`ServiceCode`),
  KEY `FK_services_ajans_idx` (`FK_ACode`),
  KEY `FK_services_employee_idx` (`FK_CodeHamkar`),
  KEY `FK_services_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_services_ajans` FOREIGN KEY (`FK_ACode`) REFERENCES `tajans` (`ACode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_services_employee` FOREIGN KEY (`FK_CodeHamkar`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_services_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tservices VALUES("1","1","1472631010","امین خورشیدسوار","9847ب632","استانداری","شهرداری","4490271891","ندارد","ندارد","4490271891","1474701313","1","10:20");



DROP TABLE IF EXISTS tshiftkari;

CREATE TABLE `tshiftkari` (
  `TShiftKariID` int(11) NOT NULL AUTO_INCREMENT,
  `TShiftName` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `DateS` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `DateF` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vcode` int(11) NOT NULL,
  PRIMARY KEY (`TShiftKariID`),
  KEY `FK_shiftkari_vahed_idx` (`FK_Vcode`),
  CONSTRAINT `FK_shiftkari_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tshiftkari VALUES("1","روزانه","8","17","4490271891","1474985037","1");
INSERT INTO tshiftkari VALUES("2","شبانه","20","6","4490271891","1474985009","1");



DROP TABLE IF EXISTS tsookht;

CREATE TABLE `tsookht` (
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `SookhtDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `SookhtType` tinyint(4) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `SookhtMizan` float NOT NULL,
  `Price` int(11) NOT NULL,
  `Tozihat` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_CityCode` int(11) DEFAULT NULL,
  `CodeJaigah` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`FK_Pelak`,`SookhtType`,`Kilometer`),
  KEY `FK_sookht_vahed_idx` (`FK_VCode`),
  KEY `FK_sookht_city_idx` (`FK_CityCode`),
  CONSTRAINT `FK_sookht_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_sookht_city` FOREIGN KEY (`FK_CityCode`) REFERENCES `tcity` (`Ccode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_sookht_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tsookht VALUES("9812ج637","1","1480151410","1","10000","30","300000","ندارد","4490271891","1482072578","78","1","1","4490271891","4490271891");
INSERT INTO tsookht VALUES("9812ج637","1","1482052210","1","11000","30","300000","ندارد","4490271891","1482072610","78","1","1","4490271891","4490271891");
INSERT INTO tsookht VALUES("9822ب111","1","1483434610","1","12","10","100000","1","4490271891","1483453550","78","1","1","4490271891","4490271891");
INSERT INTO tsookht VALUES("9822ب111","1","1483434610","1","30","1","10000","2","4490271891","1483453647","78","1","3","4490271891","4490271891");



DROP TABLE IF EXISTS ttahvil;

CREATE TABLE `ttahvil` (
  `FK_pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `Date_tahvil` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_codemeli_dahande` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_codemeli_girande` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_Vcode` int(11) NOT NULL,
  `FK_Dcode_dahande` int(11) DEFAULT NULL,
  `FK_Dcode_girande` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `TahvilID` int(11) NOT NULL AUTO_INCREMENT,
  `TahvilType` tinyint(4) DEFAULT NULL,
  `DatesTahvil` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`TahvilID`),
  KEY `FK_tahvilGIRANDE_employee_idx` (`FK_codemeli_girande`),
  KEY `FK_tahvil_vahed_idx` (`FK_Vcode`),
  KEY `FK_tahvilGIRANDE_dept_idx` (`FK_Dcode_girande`),
  KEY `FK_tahvil_car_idx` (`FK_pelak`),
  CONSTRAINT `FK_tahvilGIRANDE_dept` FOREIGN KEY (`FK_Dcode_girande`) REFERENCES `tdept` (`Dcode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tahvilGIRANDE_employee` FOREIGN KEY (`FK_codemeli_girande`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tahvil_car` FOREIGN KEY (`FK_pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tahvil_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttahvil VALUES("9822ب111","1479287410","","4490271891","1","","1","4490271891","1479298413","1","1","");
INSERT INTO ttahvil VALUES("9812ج637","1482052210","","4490239068","1","","2","4490271891","1482069258","2","1","");



DROP TABLE IF EXISTS ttamir;

CREATE TABLE `ttamir` (
  `TamirID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_TReqID` int(11) NOT NULL,
  `FactorSh` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `FactorDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FactorPrice` int(11) NOT NULL,
  `Kilometer` int(11) NOT NULL,
  `FK_CodeTamirgah` int(11) NOT NULL,
  `FactorStatus` tinyint(4) NOT NULL,
  `TamirType` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  PRIMARY KEY (`TamirID`),
  KEY `FK_tamir_car_idx` (`FK_Pelak`),
  KEY `FK_tamir_treq_idx` (`FK_TReqID`),
  KEY `FK_tamir_tamirgah_idx` (`FK_CodeTamirgah`),
  KEY `FK_tamir_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_tamir_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamir_tamirgah` FOREIGN KEY (`FK_CodeTamirgah`) REFERENCES `ttamirgah` (`CodeT`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamir_treq` FOREIGN KEY (`FK_TReqID`) REFERENCES `ttamirreq` (`TReqID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamir_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttamir VALUES("2","9822ب111","1","تعویض لاستیک","1482052210","150000","13000","1","1","1","4490271891","1482072636","1");
INSERT INTO ttamir VALUES("3","9812ج637","2","1","1483521010","120000","120","1","3","4","4490271891","1483517207","1");



DROP TABLE IF EXISTS ttamirgah;

CREATE TABLE `ttamirgah` (
  `CodeT` int(11) NOT NULL AUTO_INCREMENT,
  `NameT` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `TypeT` tinyint(4) NOT NULL,
  `FK_CityCode` int(11) NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Tell` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `Address` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`CodeT`),
  KEY `FK_tamirgah_city_idx` (`FK_CityCode`),
  KEY `FK_tamirgah_vahed_idx` (`FK_VCode`),
  CONSTRAINT `FK_tamirgah_city` FOREIGN KEY (`FK_CityCode`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamirgah_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttamirgah VALUES("1","تعمیرگاه ایران خودرو","2","78","1","08433333333","ایلام");
INSERT INTO ttamirgah VALUES("2","تعمیرگاه سایپا","2","78","1","08433333333","ایلام");
INSERT INTO ttamirgah VALUES("3","تعمیرگاه شهرداری","1","78","1","08433333333","ایلام");



DROP TABLE IF EXISTS ttamirreq;

CREATE TABLE `ttamirreq` (
  `TReqID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_CodeRequister` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_Pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `ReqSharh` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `ReqPrice` int(11) NOT NULL,
  `ReqDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeKarpardaz` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `TaiedSharh` varchar(80) COLLATE utf8_persian_ci DEFAULT NULL,
  `ReqStatus` tinyint(4) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `MaxDate` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_TamirType` int(11) DEFAULT NULL,
  `OkDate` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `StartDateTamir` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `EndDateTamir` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  `CarStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`TReqID`),
  KEY `FK_treq_employee_idx` (`FK_CodeRequister`),
  KEY `FK_treq_car_idx` (`FK_Pelak`),
  KEY `FK_treq_vahed_idx` (`FK_VCode`),
  KEY `FK_treq_tamirtype_idx` (`FK_TamirType`),
  CONSTRAINT `FK_treq_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_treq_employee` FOREIGN KEY (`FK_CodeRequister`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_treq_tamirtype` FOREIGN KEY (`FK_TamirType`) REFERENCES `ttamirtype` (`TtamirTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_treq_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttamirreq VALUES("1","4490271891","9822ب111","تعویض","120000","1480756210","4490271891","4490271891","تایید","4","4490271891","1484061889","1","1481015410","1","1484039410","1480756210","1480842610","1");
INSERT INTO ttamirreq VALUES("2","4490239068","9812ج637","ندارد","120000","1483521010","4490271891","4490271891","2","3","4490271891","1483600499","1","1484125810","1","1483607410","1483521010","1483607410","2");



DROP TABLE IF EXISTS ttamirseg;

CREATE TABLE `ttamirseg` (
  `TamirSegID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_TamirID` int(11) NOT NULL,
  `FK_CodeForoshgah` int(11) NOT NULL,
  `FK_CodeSeg` int(11) NOT NULL,
  `SegPrice` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `DatePigiri` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `KilometerPigiri` int(11) DEFAULT NULL,
  `SegCount` int(11) DEFAULT NULL,
  `SegStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`TamirSegID`),
  KEY `FK_tamirseg_tamir_idx` (`FK_TamirID`),
  KEY `FK_tamirseg_foroshgah_idx` (`FK_CodeForoshgah`),
  KEY `FK_tamirseg_segment_idx` (`FK_CodeSeg`),
  CONSTRAINT `FK_tamirseg_foroshgah` FOREIGN KEY (`FK_CodeForoshgah`) REFERENCES `tforoshgah` (`CodeF`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamirseg_segment` FOREIGN KEY (`FK_CodeSeg`) REFERENCES `tseg` (`CodeS`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tamirseg_tamir` FOREIGN KEY (`FK_TamirID`) REFERENCES `ttamir` (`TamirID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttamirseg VALUES("1","2","1","3076","120000","4490271891","1482070724","1484030410","150000","1","1");



DROP TABLE IF EXISTS ttamirtype;

CREATE TABLE `ttamirtype` (
  `TtamirTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `TtamirTypeName` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `PriorityTamir` tinyint(4) NOT NULL,
  `TimeTamir` int(11) NOT NULL,
  `FK_Vcode` int(11) NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`TtamirTypeID`),
  KEY `FK_tamirtype_vahed_idx` (`FK_Vcode`),
  CONSTRAINT `FK_tamirtype_vahed` FOREIGN KEY (`FK_Vcode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttamirtype VALUES("1","تعویض لاستیک","1","1","1","4490271891","1474984682");



DROP TABLE IF EXISTS ttrafic;

CREATE TABLE `ttrafic` (
  `FK_Pelak` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `FK_VCode` int(11) NOT NULL,
  `Date_S` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Date_F` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Date_P` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Mab_P` int(11) NOT NULL,
  `FK_Ccity` int(11) NOT NULL,
  `Comment` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `ShFish` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `Bank` varchar(45) COLLATE utf8_persian_ci NOT NULL,
  `UserID` varchar(12) COLLATE utf8_persian_ci NOT NULL,
  `ActionDate` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `Ok` tinyint(4) DEFAULT NULL,
  `CodeSabt` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `CodeTaied` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`FK_Pelak`,`Date_S`,`Date_F`),
  KEY `FK_trafic_vahed_idx` (`FK_VCode`),
  KEY `FK_trafic_city_idx` (`FK_Ccity`),
  CONSTRAINT `FK_trafic_car` FOREIGN KEY (`FK_Pelak`) REFERENCES `tcar` (`Pelak`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_trafic_city` FOREIGN KEY (`FK_Ccity`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_trafic_vahed` FOREIGN KEY (`FK_VCode`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO ttrafic VALUES("9812ج637","1","1482311410","1484817010","1482311410","100000","78","1","1","1","4490271891","1483600138","1","4490271891","4490271891");



DROP TABLE IF EXISTS tvahed;

CREATE TABLE `tvahed` (
  `Vcode` int(11) NOT NULL AUTO_INCREMENT,
  `Vname` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `FK_Ccode` int(11) NOT NULL,
  `Vtype` int(11) NOT NULL,
  `UpVahed` int(11) DEFAULT NULL,
  `FK_AdminCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_CommitCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `FK_KarpardazCode` varchar(12) COLLATE utf8_persian_ci DEFAULT NULL,
  `Tell` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE utf8_persian_ci DEFAULT NULL,
  `Address` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `Email` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`Vcode`),
  KEY `FK_vahed_city_idx` (`FK_Ccode`),
  KEY `FK_vahed_vahed_idx` (`UpVahed`),
  KEY `FK_vahed_adminEmp_idx` (`FK_AdminCode`),
  KEY `FK_vahed_commitEmp_idx` (`FK_CommitCode`),
  KEY `FK_vahed_KarpardazEmp_idx` (`FK_KarpardazCode`),
  CONSTRAINT `FK_vahed_adminEmp` FOREIGN KEY (`FK_AdminCode`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vahed_city` FOREIGN KEY (`FK_Ccode`) REFERENCES `tcity` (`Ccode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vahed_commitEmp` FOREIGN KEY (`FK_CommitCode`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vahed_KarpardazEmp` FOREIGN KEY (`FK_KarpardazCode`) REFERENCES `temployee` (`Codemeli`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vahed_vahed` FOREIGN KEY (`UpVahed`) REFERENCES `tvahed` (`Vcode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tvahed VALUES("1","شهرداری ایلام","78","1","","4490271891","4490271891","4490271891","","","","");
INSERT INTO tvahed VALUES("2","فرمانداری","78","1","1","","","","","","","");
INSERT INTO tvahed VALUES("3","آتش نشانی","78","1","2","","","","","","","");
INSERT INTO tvahed VALUES("4","شهرداری منطقه 1","78","1","3","","","","","","","");
INSERT INTO tvahed VALUES("5","شهرداری منطقه 2","78","1","","","","","","","","");



