<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $company_name
 * @property int $sap_number
 * @property string $city_and_country
 * @property string $sub_dealer
 * @property int $machines
 * @property string $active_alarms_warning
 * @property int $status
 * @property string $created
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name'], 'required'],
            [['sap_number', 'machines', 'status'], 'integer'],
            [['city_and_country'], 'string'],
            [['created'], 'safe'],
            [['company_name', 'sub_dealer'], 'string', 'max' => 255],
            [['active_alarms_warning'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Company Name',
            'sap_number' => 'Sap Number',
            'city_and_country' => 'City And Country',
            'sub_dealer' => 'Sub Dealer',
            'machines' => 'Machines',
            'active_alarms_warning' => 'Active Alarms Warning',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
}
