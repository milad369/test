<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $percent
 * @property string $title
 * @property int $searching
 * @property int $ordering
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['percent', 'searching', 'ordering'], 'integer'],
            [['name', 'type'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'percent' => 'Percent',
            'title' => 'Title',
            'searching' => 'Searching',
            'ordering' => 'Ordering',
        ];
    }
}
