<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class SiteController extends Controller
{

    public function beforeAction($action)
    {

        if ($action->id == 'error')
            $this->layout = 'empty';

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['out', 'home', 'updateuser'],
                'rules' => [
                    [
                        'actions' => ['out', 'home', 'updateuser'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['out'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /****************************************UserSite****************/


    public function actionDatatable()
    {
        //connect database and get information base inputs such as page,rows_select,orderby,search and save array
        $arr_information = [
            ["company_name" => 'Customer Company 1', "sap_number" => '2564748', "city_and_country" => 'city,Iran', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '12', "active_alarms/warning" => '1', "status" => '1'],
            ["company_name" => 'Customer Company 2', "sap_number" => '4133214', "city_and_country" => 'city,US', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '2', "status" => '1'],
            ["company_name" => 'Customer Company 3', "sap_number" => '1719856', "city_and_country" => 'city,UK', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '2', "status" => '1'],

        ];
        $arr_setting = [
            'company_name' => ["type" => "string", "percent" => "20", "title" => "Company Name"]
        ];
        $arr_search = [
            'company_name'
        ];
        $arr_order = [
            '0',
            '1',
        ];
        $arr_metadata = [
            'company_name'
        ];
        //*******************************************************************
        $this->layout = "main";
        return $this->render('ShowTable', [
            "arr_information" => $arr_information,
            "arr_setting" => $arr_setting,
            "arr_search" => $arr_search,
            "arr_order" => $arr_order,
            "arr_metadata" => $arr_metadata,
        ]);
    }

    public function actionIndex()
    {
        //connect database and get information and save array
        $arr_information = [
            ["company_name" => 'Customer Company 1', "sap_number" => '2564748', "city_and_country" => 'city,Iran', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '12', "active_alarms/warning" => '1-8', "status" => 'true'],
            ["company_name" => 'Customer Company 2', "sap_number" => '4133214', "city_and_country" => 'city,US', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '', "status" => 'true'],
            ["company_name" => 'Customer Company 3', "sap_number" => '1719856', "city_and_country" => 'city,UK', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '', "status" => 'true'],

        ];
        $arr_setting = [
            'company_name' => ["type" => "string", "percent" => "20", "title" => "Company Name"],
            'sap_number' => ["type" => "int", "percent" => "10", "title" => "SAP Number"],
            'city_and_country' => ["type" => "string", "percent" => "15", "title" => "City and Country"],
            'sub/dealer' => ["type" => "string", "percent" => "20", "title" => "Sub/dealer"],
            'machines' => ["type" => "int", "percent" => "10", "title" => "Machines"],
            'active_alarms/warning' => ["type" => "string", "percent" => "15", "title" => "Active alarms/warning"],
            'status' => ["type" => "bool", "percent" => "15", "title" => "Status"],
        ];
        $arr_search = [
            'company_name',
            'status'
        ];
        $arr_order = [
            'company_name',
            'machines',
        ];
        $metadata = 'style="font-size: 14px;color: grey"';
        //*****************************************************************************************
        $this->layout = "empty";
        //create view table
        $contentTable = $this->renderPartial('Table', [
            "arr_information" => $arr_information,
            "arr_setting" => $arr_setting,
            "arr_order" => $arr_order,
            "metadata" => $metadata,
            'total' => count($arr_information),
        ]);
        //create pagination
        $contentPage = $this->render('//site/pagination', [
            'page' => 1,
            'total' => count($arr_information),
            'number' => 10
        ]);
        $this->layout = "main";
        return $this->render('index', [
            "contentTable" => $contentTable,
            "contentPage" => $contentPage,
            "arr_search" => $arr_search,
            "arr_setting" => $arr_setting,
        ]);

    }
    public function actionIndexajax()
    {
        //this function for connect with ajax

        $page = Yii::$app->request->post('page',1);
        $rows_select = Yii::$app->request->post('rows_select',10);
        $ordervalue = Yii::$app->request->post('ordervalue','');
        $orderby = Yii::$app->request->post('orderby','');
        $search = Yii::$app->request->post('search','');

        //connect database and get information base inputs such as page,rows_select,orderby,search and save array
        $arr_information = [
            ["company_name" => 'Customer Company 3', "sap_number" => '1719856', "city_and_country" => 'city,UK', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '', "status" => 'true'],
            ["company_name" => 'Customer Company 2', "sap_number" => '4133214', "city_and_country" => 'city,US', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '10', "active_alarms/warning" => '', "status" => 'true'],
            ["company_name" => 'Customer Company 1', "sap_number" => '2564748', "city_and_country" => 'city,Iran', "sub/dealer" => 'sub comp..,dealer com..', "machines" => '12', "active_alarms/warning" => '1-8', "status" => 'true'],
        ];
        $arr_setting = [
            'company_name' => ["type" => "string", "percent" => "20", "title" => "Company Name"],
            'sap_number' => ["type" => "int", "percent" => "10", "title" => "SAP Number"],
            'city_and_country' => ["type" => "string", "percent" => "15", "title" => "City and Country"],
            'sub/dealer' => ["type" => "string", "percent" => "20", "title" => "Sub/dealer"],
            'machines' => ["type" => "int", "percent" => "10", "title" => "Machines"],
            'active_alarms/warning' => ["type" => "string", "percent" => "15", "title" => "Active alarms/warning"],
            'status' => ["type" => "bool", "percent" => "15", "title" => "Status"],
        ];
        $arr_order = [
            'company_name',
            'machines',
        ];
        $metadata = 'style="font-size: 14px;color: grey"';


        //*****************************************************************************************
        $this->layout = "empty";
        //create view table
        $contentTable = $this->renderPartial('Table', [
            "arr_information" => $arr_information,
            "arr_setting" => $arr_setting,
            "arr_order" => $arr_order,
            "metadata" => $metadata,
            'total' => count($arr_information),
        ]);
        //create pagination
        $contentPage = $this->render('//site/pagination', [
            'page' => $page,
            'total' => count($arr_information),
            'number' => $rows_select
        ]);
        $dataRespose['result'] = "ok";
        $dataRespose['replymsg'] = array("contentTable" => $contentTable, "contentPage" => $contentPage);
        return json_encode($dataRespose);


    }


}


