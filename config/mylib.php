<?php 


class mylib{
    function __construct(){
        //include 'library/excel/PHPExcel/IOFactory.php';
    }
    
    
    function ReadExcelFile($upload_dir){
        
       
        $objPHPExcel = PHPExcel_IOFactory::load($upload_dir);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
         
         return $sheetData;
         
    }
    
    function test_input($data) {
        if($data==null || $data=="")
          return "";
	    if(substr($data,0,4)=="http")
			return $data;
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
    function ReadFieldOfExcelSheet($sheetData,$index,$stratrow){
        $rowarray=array();
        for($i=$stratrow;$i<=count($sheetData);$i++){
            $row=$sheetData[$i];
            $rowarray[$i-$stratrow]=$row[$index];
        }
        return $rowarray;
    }
    function test_input_array($inputarray){
       //for($i=0;$i<count($inputarray);$i++){
        foreach($inputarray as $key=>$value){
            
            $inputarray[$key]=$this->test_input($value);
        }
        return $inputarray;
        unset($inputarray);
    }
    function writetoExcellFile($valuesarray,$filename){
        $objPHPExcel = new PHPExcel(); 
        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0); 
// Initialise the Excel row number
      //  $rowCount = 1; 
// Iterate through each result from the SQL query in turn
// We fetch each database result row into $row in turn

        foreach($valuesarray as $row)
            foreach($row as $key=>$value){
                $objPHPExcel->getActiveSheet()->SetCellValue($key, $value); 
            //var_dump($row);
    // Set cell An to the "name" column from the database (assuming you have a column called name)
    //    where n is the Excel row number (ie cell A1 in the first row)
        
    // Set cell Bn to the "age" column from the database (assuming you have a column called age)
    //    where n is the Excel row number (ie cell A1 in the first row)
        
         
          // $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row[4]);  
    // Increment the Excel row counter
        //$rowCount++; 
    } 

    // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
            $objWriter->save($filename); 
            
    }
    
    public function CreateOptionTagSelect($values){
        $strhtml='';
       foreach($values as $value){
        $strhtml.='<option value="'.$value[0].'">'.$value[1].'</option>';
       }
       
       return $strhtml;
        
    }
    
    public function RegEx_password($password,$length=8){
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number= preg_match('@[0-9]@', $password);
        if(!$uppercase || !$lowercase || !$number || strlen($password) < $length) {
            return false;
         }
         return true;
    }
    
    function generateHash($password,$salt="") {
      if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
         if($salt==""){
              $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
         }
        
         return array(crypt($password, $salt),$salt);
    }
}

public function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
    public function sendSms($mobile, $msg) {
        $sms = new sms ();
        $sms->send ( [
            $mobile => $msg
        ] );
    }
  
}

?>