<?php

use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>bugloos</title>
    <link rel="icon" href="<?php echo Yii::$app->request->baseUrl;?>/assets/Image/index.png" sizes="32x32" />
    <?= Html::csrfMetaTags() ?>

    <!-- BEGIN  CSS-->
    <link href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/fonts/line-awesome/css/line-awesome.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/vendors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/plugins/forms/validation/form-validation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/assets/css/ka.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/plugins/extensions/toastr.css">

    <!-- END  CSS-->
    <script type="text/javascript">
        csrf = '<?php echo Yii::$app->request->csrfToken?>';
        urlMain = '<?php echo Yii::$app->request->baseUrl ?>';
    </script>
</head>

<body>
<?php $this->beginBody()?>


<div class="app-content content">
    <div class="content-wrapper">
        <div>
            <img class="width-200" src="<?php echo Yii::$app->request->baseUrl?>/assets/Image/Bugloos-Logo.svg">
            <h3 class="p-t-b-15">programming test-full stack</h3>
        </div>
        <div class="content-body">
            <?= $content ?>
        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Copyright &copy; <?php echo date('Y')?></span>
    </p>
</footer>


<!-- BEGIN JS-->
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js"
        type="text/javascript"></script>

<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/core/app.js" type="text/javascript"></script>

<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/scripts/extensions/toastr.js" type="text/javascript"></script>

<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/scripts/tables/datatables/datatable-basic.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/assets/js/mylib.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/assets/js/main.js" type="text/javascript"></script>

<!-- END JS-->



<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
