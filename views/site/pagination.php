<?php
$length = 5;
$start = 1;
$end = 1;
$allpage = ceil($total / $number);
if ($page - ($length / 2) < 0) {
	$start = 1;
} else {
	$start = $page - ceil($length / 2) + 1;
}
if ($length + $start > $allpage) {
	$end = $allpage;
} else {
	$end = $length + $start;
}

$back = $page - 1;
$next = $page + 1;
/*
$total=58; $number=10; $page=10;
$total=$total/$number;
$total=ceil($total);
$test = $page - ($number / 2);
if ($test < 0) {
	$start = 1;
	if ($number > $total) {
		$end = $total;
	} else {
		$end = $number;
	}
} else {
	$start = $test;

	$end = $test + $number;
	if ($end > $total) {
		$end = $total;
		$start = $total - $number;
	}
}
if ($start == 0) {
	$start = 1;
}
$back = $page - 1;
$next = $page + 1;
*/
?>
<div>
    <?php if($total==0){?>
        <div class="text-center">
            <h4>موردی برای نمایش وجود ندارد</h4>
        </div>
    <?php }?>
<p id="start" hidden=""><?php echo $start ?></p>
<p id="end" hidden=""><?php echo $end ?></p>


<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center pagination-separate pagination-curved pagination-flat">
        <li class="page-item <?php if ($page == 1) echo ' disabled';else echo'cursor-pointer' ?>" <?php if ($page != "1") echo 'onClick="changePage(' . $back . ')"' ?>>
            <a class="page-link" aria-label="Previous">
                <span aria-hidden="true">&laquo; Previous</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <?php for ($i = $start; $i <= $end; $i++) { ?>
            <li <?php if ($page != $i) echo 'onClick="changePage(' . $i . ')"' ?> class="page-item <?php if ($i == $page) echo 'active ';else echo'cursor-pointer' ?>"><a class="page-link" ><?php echo $i ?></a></li>

        <?php } ?>
        <li class="page-item <?php if ($page == $allpage || $allpage == "0") echo ' disabled';else echo'cursor-pointer' ?>" <?php if ($page != $allpage && $allpage != "0") echo ' onClick="changePage(' . $next . ')"' ?>>
            <a class="page-link" aria-label="Next">
                <span aria-hidden="true">Next &raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>
</nav>
</div>

