<table class="table table-striped table-bordered table-responsive-sm">
    <thead>
    <tr >
        <?php foreach ($arr_setting as $key=>$temp) { ?>
        <th <?php if(in_array($key,$arr_order)) echo 'onclick="changeorder(\''.$key.'\')"'?> class="width-<?= $temp['percent'] ?>-per cursor-pointer"><?php echo $temp['title']; if(in_array($key,$arr_order)) echo '<i id="'.$key.'up" class="ft-arrow-up"></i><i id="'.$key.'down" class="ft-arrow-down"></i>';?> </th>
        <?php }?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($arr_information as $temp) { ?>
        <tr <?= $metadata ?>>
            <?php foreach ($arr_setting as $key => $value) { ?>
                <td  class="width-<?= $value['percent'] ?>-per"><?php if($value['type']=='string' || $value['type']=='int'|| $value['type']=='float' ) echo $temp[$key];else if($value['type']=='bool' && $temp[$key]=='true') echo '<i class="ft-check-circle color-green"></i> active';else if($value['type']=='bool' && $temp[$key]=='false') echo '<i class="ft-stop-circle color-red1"></i> inactive' ?></td>
            <?php }?>
       </tr>
    <?php } ?>
    </tbody>
</table>
1 To 3 - <?php echo $total?> Total items


