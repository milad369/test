<section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <h3>show data with AJAX</h3>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <div class="row m-b-30">
                            <div class="col-md-3">
                                <!-- page entries-->
                                <div>Show <select id="rows_select" onchange="changePage(1)">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                    </select> entries
                                </div>
                            </div>
                            <?php foreach ($arr_search as $temp) {
                                if (count($arr_setting[$temp]) != 0 && $arr_setting[$temp]['type'] == 'bool') {
                                    ?>
                                    <!-- true or false type-->
                                    <div class="col-md-3"><label><?= $temp ?></label><select class="width-95-per"
                                                                                             onchange="changePage(1)"
                                                                                             id="<?php echo $temp . '_search' ?>">
                                            <option value="">all</option>
                                            <option value="true">active</option>
                                            <option value="false">inactive</option>
                                        </select></div>
                                <?php } else if (count($arr_setting[$temp]) != 0 && ($arr_setting[$temp]['type'] == 'string' || $arr_setting[$temp]['type'] == 'int')) { ?>
                                    <!-- string or int type-->
                                    <div class="col-md-3"><label><?= $temp ?></label><input onkeyup="changePage(1)"
                                                                                            id="<?php echo $temp . '_search' ?>"
                                                                                            class="width-95-per"
                                                                                            placeholder="search:"></div>
                                <?php } else if (count($arr_setting[$temp]) != 0 && $arr_setting[$temp]['type'] == 'date') { ?>
                                    <!-- date type-->
                                    <div class="col-md-3"><label><?= $temp ?></label><input onchange="changePage(1)"
                                                                                            id="<?php echo $temp . '_search' ?>"
                                                                                            class="width-95-per" type="date"
                                                                                            placeholder="search:"></div>
                                <?php }
                            } ?>


                        </div>
                        <!-- show Table-->
                        <div id="divTable">
                            <?php echo $contentTable; ?>
                        </div>
                        <!-- show pagination-->
                        <div id="divPage">
                            <?php echo $contentPage; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    orderby = '';
    ordervalue = '';
    arrsearch = [];
    //change array_search php to javascript
    <?php for ($i = 0; $i < count($arr_search); $i++) {
        echo 'arrsearch[' . $i . ']="' . $arr_search[$i] . '";';
    }?>

    function changePage(page) {
        var rows_select = $('#rows_select').val();
        var data_search = {};

        //create array search
        for (var i = 0; i < arrsearch.length; i++) {
            data_search[arrsearch[i]] = $("#" + arrsearch[i] + '_search').val();
        }

        senddataCo = {
            'page': page,
            'rows_select': rows_select,
            'orderby': orderby,
            'ordervalue': ordervalue,
            'search': data_search
        };
        //send to server by ajax
        send_to_server(urlMain + "/TableAjax.html", senddataCo, resultTable, "divTable", 'POST');
    }

    function resultTable(data) {
        if (data['result'] == "ok") {
            toastr.success('Done Successfully', 'Attention!', {positionClass: 'toast-bottom-left'});
            $("#divTable").html(data['replymsg']['contentTable']);
            $("#divPage").html(data['replymsg']['contentPage']);
        }
    }

    function changeorder(key) {
        if (key != orderby) {
            ordervalue = 'SORT_DESC';
        } else if (ordervalue == 'SORT_DESC') {
            ordervalue = 'SORT_ASC';
        } else {
            ordervalue = 'SORT_DESC';
        }
        orderby = key;

        changePage(1);

    }
</script>

