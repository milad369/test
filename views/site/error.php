<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<head>
    <meta charset="utf-8">
    <title>test</title>
    <!-- Favicons -->

    <link href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/fonts/line-awesome/css/line-awesome.min.css"
          rel="stylesheet">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/custom-rtl.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/app-assets/css-rtl/pages/error.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/assets/css/style-rtl.css">
    <!-- END Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/assets/css/ka.css">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/assets/iransans/fontiran.css">

</head>
<body class="vertical-layout vertical-menu 1-column   menu-expanded blank-page blank-page"
      data-open="click" data-menu="vertical-menu" data-col="1-column">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 p-0">
                        <div class="card-header bg-transparent border-0">
                            <h2 class="error-code text-center mb-2">404</h2>
                            <h3 class="text-uppercase text-center">صفحه مورد نظر یافت نشد!</h3>
                        </div>
                        <div class="card-content">
                            <div align="center">
                                <div >
                                    <a href="<?php  echo Yii::$app->request->baseUrl.'/index.html'?>" class="btn btn-primary btn-block font-page"><i class="ft-home"></i>  صفحه اصلی  </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<!-- BEGIN VENDOR JS-->
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"
        type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/core/app.js" type="text/javascript"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo Yii::$app->request->baseUrl;?>/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
</body>
</html>