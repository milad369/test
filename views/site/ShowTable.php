<section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <h3>show data with datatable</h3>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <table class="table table-striped table-bordered zero-configuration" id="example">
                            <thead>
                            <tr>
                                <th id="company_name">Company Name</th>
                                <th>SAP Number</th>
                                <th>City and Country</th>
                                <th>Sub/dealer</th>
                                <th>Machines</th>
                                <th>Active alarms/warning</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($arr_information as $temp) { ?>
                                <tr>
                                    <td><?php echo $temp['company_name'] ?></td>
                                    <td><?php echo $temp['sap_number'] ?></td>
                                    <td><?php echo $temp['city_and_country'] ?></td>
                                    <td><?php echo $temp['sub/dealer'] ?></td>
                                    <td><?php echo $temp['machines'] ?></td>
                                    <td><?php echo $temp['active_alarms/warning'] ?></td>
                                    <td><?php echo $temp['status'] ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

