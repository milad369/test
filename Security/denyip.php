<?php
class iptest{
	private $listip=array();
	private $max=10;
	private $ip;
	private $timeout=10800;
    private $fileAddr="Security/listblock.txt";
	private $add=0;
   function __construct($addcheck){
        $this->listip=array();
		if($addcheck){
			$this->add=1;
		}
    }
private function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

private function writetofile(){
    $myfile = fopen($this->fileAddr, "w");
	
	foreach($this->listip as $value ){
       if(count($value)>1){
	       	$str=implode(",",$value)."\n";
		fwrite($myfile,$str);
	   }
	
	}
}	
private function readfile(){
	$myfile = fopen($this->fileAddr, "r");
    while(!feof($myfile)){
        $str=fgets($myfile);
		$temp=explode(",",$str);
        if(count($temp)>0)
		  array_push($this->listip,$temp);
	}
}

public function testuser(){
	$this->ip=$this->get_client_ip();
	return $this->isvalid();
}
private function isvalid(){
	$this->readfile();
	$existIP=false;
	$i=0;
	foreach($this->listip as $value ){
		if($value[0]==$this->ip) {
			$existIP=true;
			if($value[1]<$this->max){ 
		     	$this->listip[$i][1]+=$this->add;
                if($this->listip[$i][1]==$this->max){
                    	$this->listip[$i][2]=time();
                }
				$this->writetofile();
				return true;
			}
			else{
			   if($value[2]<time()-$this->timeout){
					     $this->listip[$i][2]=0;
                         $this->listip[$i][1]=1;
                         $this->writetofile();
                        
						return true;
			    }
                else{
                   
                    return false;
                }
			}
		}
		
        $i++;		
	}
	if(!$existIP){
	 
		$temp=array($this->ip,1,0);
		array_push($this->listip,$temp);
        $this->writetofile();
        return true;
	}
}


function remove(){
	$this->ip=$this->get_client_ip();
    $this->readfile();
    $i=0;
    
    foreach($this->listip as $value ){
        if($value[0]==$this->ip) {
            unset($this->listip[$i]);
            break;
        }
        $i++;
    }
    $this->writetofile();
}

}

?>