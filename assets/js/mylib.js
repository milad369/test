// JavaScript Document
function send_to_server(addr, senddata, callback, idoverlay, method,id) {
    if (idoverlay != null) {
        // var overlay = new ItpOverlay(idoverlay);
        // overlay.show();
        OverLayMainOpen(idoverlay);
    }
senddata['_csrf']= csrf;
    $.ajax({
        url: addr,
        type: 'post',
        data: senddata,
        dataType: 'json',
    })
            .done(function (data) {
                if (idoverlay != null) {
                    // overlay.hide();
                    OverLayMainClose(idoverlay);
                }
                callback(data,method,id);

            })
            .fail(function (jqXHR, textStatus) {
                // toastr.error('مشکلی بوجود آمده است لطفا به مدیر سایت اطلاع دهید.', 'توجه!', {positionClass: 'toast-bottom-left'});
                // alert(jqXHR.responseText);
                if (idoverlay != null) {
                    // overlay.hide();
                    OverLayMainClose(idoverlay);
                }
            });

}


function sendData(data, url) {
  var name,

      form = document.createElement("form"),
      node = document.createElement("input");
var iframe = document.createElement("iframe");
iframe.name = "myTarget";

// Next, attach the iFrame to the main document
window.addEventListener("load", function () {
  iframe.style.display = "none";
  document.body.appendChild(iframe);
});
   //Define what should happen when the response is loaded
  iframe.addEventListener("load", function () {
   // alert("Yeah! Data sent.");
  });

  form.action = url;
  form.target = iframe.name;
  form.method = 'POST';
   for(name in data) {
    node.name  = name;
    node.value = data[name].toString();
    form.appendChild(node.cloneNode());
  }

  // To be sent, the form needs to be attached to the main document.
  form.style.display = "none";
  document.body.appendChild(form);

  form.submit();

  // But once the form is sent, it's useless to keep it.
  document.body.removeChild(form);
}

function soapFunction(url, method, sendData, callback, idoverlay) {
    var overlay = new ItpOverlay(idoverlay);
    overlay.show();
    $.soap({
        url: url,
        method: method,
        data: sendData,
        success: function (soapResponse) {
            overlay.hide();
            callback(soapResponse);
            //$(".greeting-id").text(soapResponse.toString());
            // do stuff with soapResponse
            // if you want to have the response as JSON use soapResponse.toJSON();
            // or soapResponse.toString() to get XML string
            // or soapResponse.toXML() to get XML DOM
        },
        error: function (SOAPResponse) {
            overlay.hide();
        }
    });
}
function testpass(str, state) {


    switch (state) {
        case "1":
            //should contain at least 8 characters, 1 number, 1 upper and 1 lowercase
            // return str.match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/");
            return str.match("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$");

            break;
        default :
            return false;
    }

}

function hasSpecialChar(str){
   if( str.match("[^|a-z0-9آ ا ب پ ت ث ج چ ح خ د ذ ر ز ژ س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی]")){
       return false;
   }
   return true;
}
function isUndefined(variable){
    if (typeof variable === 'undefined' || !variable) {
        return true;
    }
    return false;
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function chMaxChar(textboxID, idError, maxCount) {
    objError = $('#' + idError);
    objTxt = $('#' + textboxID);
    countChar = objTxt.val().length;

    if (maxCount == -1) {
        return true
    }
    else {
        if (countChar < maxCount)
            return true;
        else
            return false;
    }

}

function PlaySound(soundObj) {
    var buzzer = $('#' + soundObj)[0];
    // var sound = document.getElementById(soundObj)[0];
    buzzer.Play();
}

function chMinChar(textboxID, idError, minCount) {
    objError = $('#' + idError);
    objTxt = $('#' + textboxID);
    countChar = objTxt.val().length;

    if (minCount == -1) {
        return true
    }
    else {
        if (countChar <= minCount)
            return true;
        else
            return false;
    }

}


function validEmail(email) {
    var re = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return re.test(email);
}
function validEng(tem) {
    var re = new RegExp(/^[a-zA-Z0-9,-_\.@ ]+$/);
    return re.test(tem);
}

function validMobile(mobile) {
    regStr = new RegExp("09(9[0-9]|1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}");
    return regStr.test(mobile);

}
function checkEmpty(obj, border) {
    if (obj.val() == "") {
        if (border) {
            obj.css("border-color", "#F00");
        }

        return true;
    }
    else {
        return false;
    }
}



function uploadfile(formId, fileId, url, callback, idoverlay, data) {
    if (idoverlay != null) {
        var overlay = new ItpOverlay(idoverlay);
        overlay.show();
    }



    var formElement = document.querySelector(formId);
    var formData = new FormData(formElement);

    if(data != null){
        for  (var key in data) {
            formData[key] = data[key];
        }
    }

    //console.log(formData);
    var xhr = $.ajax({
        url: url, //Server script to process data
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json'
    }).done(function (data) {
                if (idoverlay != null) {
                    overlay.hide();
                }
                callback(data);

            })
            .fail(function (jqXHR, textStatus) {
              //  alert("خطایی بوجود آمده است. دوباره تلاش کنید" + jqXHR.responseText);
                if (idoverlay != null) {
                    overlay.hide();
                }
            });

    return xhr;

}

function addOption(obj, id, selected, disable) {
    str = "";
    strslct = "";
    /*if (selected == "-1")
     strslct = "selected=selected";
     str = '<option value="-1" ' + strslct + '>-</option>';*/
    //Object.keys(obj).forEach(function (key) {
    for (key in obj) {
        strslct = " ";
        if (key == selected)
            strslct = "selected=selected";
        str = str + '<option value="' + key + '" ' + strslct + '>' + obj[key] + '</option>';

    }
    $(id).find('option').remove();
    $(id).append(str);
    if (disable == "1") {
        (id).css("disabled", true);
    }
}
var englishNumbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
var persianNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
//English to Farsi
function convertNum(str) {
    str= str.toString();
    var chars = str.split('');
    for (var i = 0; i < chars.length; i++) {
        if (/\d/.test(chars[i])) {
            chars[i] = persianNumbers[chars[i]];
        }
    }
    return chars.join('');
}
//Farsi to English
function convertNum2(str) {
    str= str.toString();
    var chars = str.split('');
    for (var i = 0; i < chars.length; i++) {
        if (/\d/.test(chars[i])) {
            chars[i] = englishNumbers[chars[i]];
        }
    }
    return chars.join('');
}


function showSnackbar(text, time) {
    var x = document.getElementById("snackbar");
    x.innerHTML = text;
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, time);
}

function animateScroll(id, offset) {
   // offset = 20; //Offset of 20px

    $('html, body').animate({
        scrollTop: $(id).offset().top + offset
    }, 2000);
}

function PrintElem(elem)
    {
      var mywindow = window.open('', 'PRINT', 'height=400,width=600');


        mywindow.document.write('<html><head><title>' + document.title  + '</title>');

        mywindow.document.write('</head><body >');
      mywindow.document.write('<h1>' + document.title  + '</h1>');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();

        return true;

        }

$('.has-spinner').attr("disabled", false);
function buttonLoader(action, obj) {
    var self = obj;
    if (action == 'start') {
        if ($(self).attr("disabled") == "disabled") {
            return false;
        }
        $('.has-spinner').attr("disabled", true);
        $(self).attr('data-btn-text', $(self).text());
        var text = 'Loading';
        console.log($(self).attr('data-load-text'));
        if ($(self).attr('data-load-text') != undefined && $(self).attr('data-load-text') != "") {
            var text = $(self).attr('data-load-text');
        }
        $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin" title="button-loader"></i></span> ');
        $(self).addClass('active');
    }
    if (action == 'stop') {
        $(self).html($(self).attr('data-btn-text'));
        $(self).removeClass('active');
        $('.has-spinner').attr("disabled", false);
    }
}



function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
