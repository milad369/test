function viewUser(){
    $('#myModalViewUser').modal('show');
}
function viewUserHamkar(){
    $('#myModalViewUser').modal('show');
}
function viewUserSite(){
    $('#myModalViewUser').modal('show');
}
function editviewSite() {

    var pass = $('#passKarbarmain').val();
    var rpepass = $('#rpepassKarbarmain').val();

    if (pass !== "" || rpepass !== "") {
        if (pass != rpepass) {
            toastr.error('رمز عبور یکسان نیست.', 'توجه!', {positionClass: 'toast-bottom-left'});
            return;
        }
    }

    senddataCo ={
        password : pass
    };
    send_to_server (urlMain + "/updatepassuserSite.html", senddataCo,resulteditviewSite, "myModalViewUser", 'POST');

}
function resulteditviewSite(data){
    if (data['result'] == "ok") {
        $('#myModalViewUser').modal('hide');
        toastr.success('با موفقیت انجام شد', 'توجه!', {positionClass: 'toast-bottom-left'});
    }
}
function editviewHamkar() {

    var pass = $('#passKarbarmain').val();
    var rpepass = $('#rpepassKarbarmain').val();

    if (pass !== "" || rpepass !== "") {
        if (pass != rpepass) {
            toastr.error('رمز عبور یکسان نیست.', 'توجه!', {positionClass: 'toast-bottom-left'});
            return;
        }
    }

    senddataCo ={
        password : pass
    };
    send_to_server (urlMain + "/updatepasshamkar.html", senddataCo,resulteditviewHamkar, "myModalViewUser", 'POST');

}
function resulteditviewHamkar(data){
    if (data['result'] == "ok") {
        $('#myModalViewUser').modal('hide');
        toastr.success('با موفقیت انجام شد', 'توجه!', {positionClass: 'toast-bottom-left'});
    }
}
function editviewKarbar() {

    var pass = $('#passKarbarmain').val();
    var rpepass = $('#rpepassKarbarmain').val();

    if (pass !== "" || rpepass !== "") {
        if (pass != rpepass) {
            toastr.error('رمز عبور یکسان نیست.', 'توجه!', {positionClass: 'toast-bottom-left'});
            return;
        }
    }

    senddataCo ={
        password : pass
    };
    send_to_server (urlMain + "/updateuser.html", senddataCo,resulteditviewKarbar, "myModalViewUser", 'POST');

}
function resulteditviewKarbar(data){
    if (data['result'] == "ok") {
        $('#myModalViewUser').modal('hide');
        toastr.success('با موفقیت انجام شد', 'توجه!', {positionClass: 'toast-bottom-left'});
    }
}
function editviewKarbarOrgan() {

    var pass = $('#passKarbarmain').val();
    var rpepass = $('#rpepassKarbarmain').val();

    if (pass !== "" || rpepass !== "") {
        if (pass != rpepass) {
            toastr.error('رمز عبور یکسان نیست.', 'توجه!', {positionClass: 'toast-bottom-left'});
            return;
        }
    }

    senddataCo ={
        password : pass
    };
    send_to_server (urlMain + "/updateuserOrgan.html", senddataCo,resulteditviewKarbar, "myModalViewUser", 'POST');

}
function resulteditviewKarbar(data){
    if (data['result'] == "ok") {
        $('#myModalViewUser').modal('hide');
        toastr.success('با موفقیت انجام شد', 'توجه!', {positionClass: 'toast-bottom-left'});

    }
}
function updateUser(){
    $('#myModalViewUser').modal('show');
}
function UserUpdateSave() {

    var nameKarbarmain = $('#nameKarbarmain').val();

    senddataCo ={
        name:nameKarbarmain
    };
    send_to_server (urlMain + "/updateCustomer.html", senddataCo,resultUserUpdateSave, "myModalViewUser", 'POST');

}
function resultUserUpdateSave(data){
    if (data['result'] == "ok") {
        $('#myModalViewUser').modal('hide');
        $('#ModalResultShow').html('با موفقیت انجام شد');
        $('#ModalResult').modal('show');

    }
}

// function doWork() {
//     Change();
//     repeater = setTimeout(doWork, 120000);
// }
// function Change(){
//     var audioElement = document.createElement('audio');
//     audioElement.setAttribute('src', urlMain+'/assets/ring/ring.mp3');
//
//     $.ajax({
//         url: urlMain + "/GetOrderAlert.html",
//         type: 'get',
//         data: {
//
//         }
//     })
//         .done(
//             function (result) {
//                 var obj = JSON.parse(result);
//                 if (obj.result == "ok") {
//                     var count = obj.replymsg;
//                    if(count!=0){
//                        $('#alert1').text(count);
//                        $('#alert2').text(count);
//                        audioElement.play();
//
//                        var pathname = window.location.pathname;
//                        var temp=pathname.split('/');
//                        if(temp[(temp.length-1)]=='orderTel.html'){
//                            changePage(1);
//                        }
//
//                    }
//                 } else if (obj.result == "error") {
//                 }
//             })
//         .fail(function (jqXHR, textStatus) {
//             // alert("خطایی بوجود آمده است. دوباره تلاش کنید.");
//         });
// }
// doWork();
function overleyShow(id) {
    jQuery('#' + id).css('position', 'relative');
    var overlay = new ItpOverlay(id);
    overlay.show();
}
function overleyShowSelect(id) {
    $("#" + id).show();
    jQuery('#' + id).css('position', 'relative');
    var overlay = new ItpOverlay(id);
    overlay.show();
}
function overleyShowModal(id) {
    //jQuery('#'+id).css('position', 'relative');
    var overlay = new ItpOverlay(id);
    overlay.show();
}
function overleyHide(id) {
    //$("#"+id).hide();
    jQuery('#' + id).css('position', '');
    var overlay = new ItpOverlay(id);
    overlay.hide();
}
function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    setCookie(name,"",-1);
}
function sendFavForm() {
    $("#arrfavform").val(arrFav);
    $("#formFav").submit();
}
function sendCardForm() {
    $("#cookieidform").val(cookieID);
    $("#formCard").submit();
}
function OverLayMainOpen(id) {
    var block_ele = $('#'+id).closest('#'+id);
    $(block_ele).block({
        message: '<div class="semibold"><span class="ft-refresh-cw icon-spin text-left"></span>&nbsp; Please Wait</div>',
        fadeIn: 1000,
        fadeOut: 1000,
        // timeout: 2000, //unblock after 2 seconds
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            width: 'auto',
            backgroundColor: '#333'
        }
    });
}
function OverLayMainClose(id) {
    var block_ele = $('#'+id).closest('#'+id);
    $(block_ele).block({
        timeout: 2, //unblock after 2 seconds
    });
}